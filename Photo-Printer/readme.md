# Readme

## Maintenance

New maintence cartridge: Canon MC-20

Changed 2022-11-16

 - Empty weight of new cartridge: 275g
 - Full weight of old cartridge: 570g
 - Approx ink weight: 300g


New cartridges 2022-12-28: CO, GY and PBK
 - Empty weight of old cartridge: GY: 32.7g, PBK 32.7g
 - Full weight of new cartridge: GY: 117.1g, PBK 116.2g
 - Approx usable ink weight: 84g
