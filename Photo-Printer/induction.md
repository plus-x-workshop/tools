# Canon Pro-1000 Photo Printer Induction

Last reviewed 2022-10-17, AS


## Paying for usage

We charge per print, including failed prints (if you only want to pay for successful prints, we recommend using a bureau). Cost is £1.50 + VAT per multiple of A4, e.g. 

 - 1 x A4 print: £1.50 + VAT
 - 1 x A3/A3+ print: £3 + VAT
 - 1 x A2 print: £3 + VAT
 - 5 x A4 print: £7.50 + VAT
  
You do not have to pay for nozzle check prints.

Please let us know how many prints you have made, and we will verify this against the built-in accounting software. We will tally up print costs and invoice you quarterly.

## Online manual

https://ij.manual.canon/ij/webmanual/Manual/All/PRO-1000%20series/EN/CNT/Top.html


## Materials

### Ink

We provide genuine Canon inks and manage all restocking of inks in the printer. We cover the costs of this ink (approx £500 for a complete set) by charging per print.

### Paper

We recommend the use of papers from Canon, Hahnemuhle and Fotospeed, all of which have generic ICC profiles for this printer. [Photopaper.co.uk](https://www.photopaper.co.uk) is a good, well-priced stockist.

#### Paper Weight / Thickness
You can use paper in the following weight/thickness ranges.

**Do not use paper heavier or thicker than this, as it could jam in the printer.**

Top feed
- Plain paper: From 17 to 28 lb (64 to 105 g /m2)
- Specialty paper: Up to 80 lb (**300 g /m2**) / from 4 to 11.8 mil (0.1 to 0.3 mm)

Manual feed tray
- Specialty paper: Up to 107 lb (**400 g /m2**) / from 4 to 27.6 mil (0.1 to 0.7 mm)

#### Page sizes

You can load the page sizes as follows.

- top feed: A5, A4, A3, A3+, A2, B5, B4, B3, Letter, Legal, 4" x 6" (10 x 15 cm), 5" x 7" (13 x 18 cm), 8" x 10" (20 x 25 cm), 10" x 12" (25 x 30 cm), 11" x 17" (28 x 43 cm), 14" x 17" (36 x 43 cm), 17" x 22" (43 x 56 cm), 8.27 x 23.39 inches (210 x 594 mm)

- manual feed tray: A4, A3, A3+, A2, B4, B3, Letter, Legal, 8" x 10" (20 x 25 cm), 10" x 12" (25 x 30 cm), 11" x 17" (28 x 43 cm), 14" x 17" (36 x 43 cm), 17" x 22" (43 x 56 cm), 8.27 x 23.39 inches (210 x 594 mm)

(from https://ij.manual.canon/ij/webmanual/Manual/All/PRO-1000%20series/EN/BG/bg-006.html)


## Borderless printing

**We do not allow borderless printing**. This is because it risks leaving ink on the rollers, which then spoils the next print. If you want to print a full bleed image, please print on overszr paper and trim down to size (you can use the Logan Team system blade and straight edge to do this in the Fablab)

You can buy oversized papers especially for this, eg. A3+ size (329mm x 483mm) <https://www.photopaper.co.uk/shop/Fotospeed-A3-Paper-c34321021>



## Image Prep

Save your images as:

* JPEG (**not** RAW, PNG, PDF, GIF etc)
* RGB (**not** CMYK, Greyscale, Bitmap, etc.)
* Use Adobe RGB for wider colour space (otherwise sRGB is fine) <https://ij.manual.canon/ij/webmanual/PrintStudioPro/W/1.3/EN/DPPG/dppg_2-01.html>
* Aim for around 300dpi, but experiment for best results


## Basic workflow

Update: please **do not switch the printer off after use**; it will go into energy saving mode automatically. This saves printer ink.

- Wake printer up by pressing power button
- Open input and output trays
- Connect to the printer using the provided USB cable
- If required, perform the nozzle test
- Load paper (printing face up, top edge loads first)
- Register paper type on printer interface
- In the printer settings on your computer, make sure you have selected the correct paper size, type (matching the paper you've registered on the printer) and colour matching profile.
- Print

You can print from the PC in the fablab closest to the printer (there is a USB port on the left side of the screen). You can also print from your own laptop, if you have a USB-A port/dongle.

Full instructions using standard system print dialog: 
https://ij.manual.canon/ij/webmanual/Manual/All/PRO-1000%20series/EN/BG/bg-002.html

Full instructions using  Professional Print & Layout: 
https://ij.manual.canon/ij/webmanual/ProfessionalPrintLayout/M/1.3/EN/PPL/Top.html



## Software

### Print from any software using the standard printing controls

- Download drivers here: <https://www.canon.co.uk/support/consumer_products/products/printers/inkjet/other_series/imageprograf_pro-1000.html?type=drivers>

### Or use Canon's Professional Print & Layout software. 

 - This is installed on the PC in the fablab closest to the printer, or you can download it to your computer here: <https://www.canon.co.uk/printers/inkjet/pixma/software/professional-print-layout-software/>

Canon's Professional Print & Layout software has settings designed for this printer. If you are using the native system print dialog, you will need to change the following settings (Screenshts are from MacOS Ventura, equivalent setttings are availble on Windows)

### Paper size, orientation, scaling, etc.

![](assets/mac-print-dialog-1.png)

### Printer Options > Colour Matching

Select ColorSync, and slect your ICC prole (See below for more on ICC profiles)

![](assets/mac-print-dialog-2.png)

### Printer Options > Quality and Media

Under Media type, select the paper that most closely matches your media. The options given are Canon brand names. Choose the one that is closest to the brand name for your paper.

Also choose paper source and print quality settings.

![](assets/mac-print-dialog-3.png)

## ICC Profiles

If printing from your own laptop, you can use any ICC profiles you have installed. On the fablab PC, we have profiles for Canon, Hahnemuhle and Fotospeed papers, which can all be accessed from the Professional Print & Layout software settings.

More information on ICC Profiles: https://www.canon.co.uk/pro/stories/perfect-prints-with-icc-profiles/

This page gives useful information on which paper type to select for many common brands of paper, and which ICC profile to use.

You can download generic profiles for your own computer here:

- Hahnemühle https://www.hahnemuehle.com/en/digital-fineart/icc-profile/download-center.html
- Fotospeed https://fotospeed.com/profiles/printer/ink/ink/40/
- Canson Infinity https://www.canson-infinity.com/en/icc-profiles


## Basic troubleshooting

See the guide here:
https://ij.manual.canon/ij/webmanual/ErrorCode/PRO-1000%20series/EN/ERR/err_top.html?ref=err_top

**Do not attempt any maintenance of the printer**, with the exception of printing the nozzle check pattern, and then running the print head cleaning operation if required. 

We recommend running this test if you believe the printer has not been used for a long time.

Instructions: https://ij.manual.canon/ij/webmanual/Manual/All/PRO-1000%20series/EN/BG/bg-041.html

**If you believe any other maintenance is required, please ask a member of the workshop team for help.**




## Member Induction sign-off form 

Please review and complete this form: [Plus X Workshop Induction — Photo Printer](https://forms.gle/CngAqurBd9EFXa858)

**This must be completed before any further use of the machine**



