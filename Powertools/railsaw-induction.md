last reviewed 2022-07-22 by AS

# Festool Railsaw/ Plunge-cut Saw TS 55 REBQ 230V Induction

## Documentation

- [Manufacturer website](https://www.festool.co.uk/products/sawing)
- [Manual (PDF)](https://gitlab.com/plus-x-workshop/tools/-/raw/master/Powertools/assets/TS55REBQ-manual.pdf)

## Materials 
*  ALLOWED: MDF, plywood, timber boards
* ALLOWED WITH SPECIAL BLADE: Aluminium, Plastics
*  NOT SUITABLE: recycled wood, small piece (use the bandsaw for these)

## Safety

* No lone working is permitted with this tool. Please ensure there is someone else in the space with you or nearby in the workshop in case of accident (updated policy: August 2024)


* Verify that the Rail, Saw and Blade are in condition before starting work. If in doubt, ask a member of staff for help.
* Do not place limbs, body or legs parts in line with the blade – always be to the side.**
* PPE must be worn while using this tool (steel toe caps, goggles). Check all loose clothing is out of the way and hair is tied back.
* Kick back – this is when too much pressure is put on the blade either by the material coming together behind the cut or you are not cutting on a straight line.
* Clamp your work down to prevent movement of material.
* Blade – be aware of where the blade is, you can’t see it when cutting 
* Blade – check the blade is spinning correctly, listen out for any grinding on the blade – ask if unsure. 
* Check the workbench table is stable and wheels are locked in position.
* Always leave the tool in a safe place i.e. not on the edge of the table or rail – place it in the middle of the table if possible, with the cables out of the way to not be a hazard to others.

## Setup:

### Blades

* Check that you have the correct blade in for your material – ask if you are unsure

### Rail and clamping

* Place rail on top of material and line it up
* Think about placement of your piece on the workbench – will you be able to guide the saw along the whole cut in one go, if not change where you are setting up.
* Choose the correct rail length - the saw should be able to sit both sides of your material without being on top of it.
* Clamp down your rail with the festool clamps as well as your work, be sure not to over tighten as this can course a curve in the rail.
* Use extra clamps on your piece to prevent it from moving – this can also cause kickback if material is moved. – **DO NOT USE YOUR HANDS!**

### Saw setup

* You can change the blade depth via the green bar with FS on it (see image on next page). The FS is the blade depth with the rail and the plain is without the rail.
* The blade should be going through the whole material eg. 18mm board the blade should be at 19mm you don’t need to set the depth way over.
* Also check the blade is at 0 on the front of the tool
* Place the saw on the rail – wobble the saw and see if it moves, if it does there are some black knobs on the bottom which you can tighten and this helps secure the saw onto the rail.

### Extraction

* Always use extraction
* For larger jobs, you should also use the room extraction to remove dust that the potable extractor cannot capture
* Attach the correct extractor (they are labelled for metal & wood, or just wood) make sure you twist the nozzle onto the attachment, so it is locked. Check the extractor is on ‘auto’
- If the extractor is beeping please get a member of staff for help and stop using it

![Saw on Rail](assets/saw_on_rail.png)

![Clamp](assets/clamp.png)

![Depth Gauge](assets/depth_gauge.png)

## Cutting 

* If you need help cutting large pieces ASK FOR HELP!
* Plug the power lead into the hoover and into the tool checking you have twisted it to lock it in place. (the tool is now on be careful!)
* Before you cut place the power cable and hoover tubing over your shoulder, so they don’t get cut or court when cutting.
* There are steps to using this tool which will really help to get used to it.
* Do a dry run first just to see if there are any obstacles eg. People, hoses, cables, not enough space hoover in wrong place
    1. Push up on the green button on the top of the handle (removes the stopper)
        + move the blade down and check you have set the depth correct and that the riving knife is coming out (this helps prevent kick back*)
    2. Pull the green button under the handle (on button)
    3. Now WAIT till the blade is completely running (check is sounds correct if unsure ask a member of staff)
    4. Check the hoover is turing on.
    5. Put hand on the horizontal handle to help push down (NOT BEHIND THE BLADE)
    6. Glide along your material if anything feels wrong turn the tool off and step back and check
* When not using unplug the power cable and leave in the middle of the table to prevent any danger to others and yourself. 
* Not on the edge of the table or rail!

![Buttons](assets/buttons.png)

## Cleaning

* Return saw back to the original position, if moved to a different angle
* Put any waste in the bins provided.
* Once finished place all items back where they belong: rail, saw, extractor, power cable, clamps and if the blade was changed from wood ask a member of staff to change it back to wood.
* clean up any mess 

## Member Induction sign-off form 

Complete this form to finish your induction: [Plus X Workshop Induction — Rail Saw](https://forms.gle/Rzy3Y7Pi4nyUxM7K6)

#### *this must be completed before any further use of the machine*
