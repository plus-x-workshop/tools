last reviewed 2022-08-26 by KI

## CTM 26 E GB 240V and CTM 48 E LE EC B22 240V Extractor

### Which to use for what

| **Extractor**                    | **Metal Shards**             | **Metal Sanding** | **Wood/ Plastic Sanding** | **Wood/ Plastic Parts** |
| ---------------------------------| ---------------------------- | ----------------- | ------------------------- | ----------------------- |
| CTM 26 E GB 240V (small)         |  no                          | no                | yes                       | yes                     |
| CTM 48 E LE EC B22 240V (large)) |  yes (make sure cooled down) | yes               | yes                       | yes                     |


### After using the festool extractors please leave them as shown below: 

- with the cables to the powertool lightly rapped around the handle one ontop of the other (not taggled together). 

![festool_extractor03](assets/festool_extractor03.jpeg)
![festool_extractor02](assets/festool_extractor02.jpeg)

- leave the nozzle tube from the extraction ontop of the coiled power tool cables
- it should come in from the side (pink circle) and coil in being held down by dark blue plastic top.

![festool_extractor01](assets/festool_extractor01.jpeg)

- the cables and noozle tube should never be in knots 

![festool_extractor04](assets/festool_extractor04.jpeg)