last reviewed 2022-08-25 by KI

# Power Tools (Festools, drills and other handheld power tools) maintenance

* [Domino Operation User Manual PDF](assets/festool-domino-operation-manual.pdf)
* [Domino Safety User Manual PDF](festool-domino-safety-manual.pdf)
* [Jig Saw User Manual PDF](assets/jig-saw-manual.pdf)
* [Kapex User Manual PDF](assets/kapex-ks60-manual.pdf)
* [Mitre Saw User Manual PDF](assets/mitre-saw-manual.pdf)
* [Router User Manual PDF](assets/OF1010EBQ-manual.pdf)

### Firmware Updates

n/a

### General Maintenance

* Maintenance and care

    * Always remove the power supply plug from the socket before carrying out any work on the machine.
    * All maintenance and repair work which requires the motor casing to be opened may only be carried out by an authorised service centre.
    * Customer service and repair. Only through manufacturer or service work- shops: Please find the nearest address at: www.festool.com/Service
    * Use only original Festool spare parts! Order No. at: www.festool.com/Service

### Replacement parts

#### Repair issues:
[Repair website](https://www.festool.co.uk/myfestool/repair-order)

buying extraction bags:

* [CT-26 wood extractor](festool.co.uk/accessory/496187---sc-fis-ct-265#Overview)
* [CT-48 wood extractor](https://www.festool.co.uk/accessory/497539---sc-fis-ct-485#Overview)

#### Damaged parts

Replace any sawn-off or damaged limit stops. Damaged limit stops may be ejected when you work with the saw. Any persons standing in the vicinity of the saw may be injured.