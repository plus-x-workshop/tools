last reviewed 2021-10-09 by KI

# Festool Domino DF 500 Q-Plus 240V Induction

## Documentation

- [Manufacturer website](https://www.festool.co.uk/products/domino-jointing-system/domino-joining-machines/574327---df-500-q-plus-gb-240v#Overview)
- [Manual, safety (PDF)](assets/festool-domino-safety-manual.pdf)
- [Manual, operation (PDF)](assets/festool-domino-operation-manual.pdf)

## Materials 
+  OK: flat sheet and block materials: soft and hard wood, chip
board, plywood and fibre boards
+  Not suitable: metals, very small pieces and pieces thinner than the joining biscuits

## Safety

- If unsure at any point, ask a member of staff for help
- If the extractor is beeping please ask a member of staff for help

### Setup:

- PPE must be worn: Steel toe caps, goggles
- Use clamps or another practical way to secure and support the workpiece to a stable platform
- Always use the integrated Festool dust extraction
- Always remove the power cable before making adjustments and changes to the tool

### In use:

- Only handle the tool by the rear portion of the body and the front handle
- Never support the workpiece by placing your hand behind it, as the cutter can extend all the way through the workpiece


### After use:

- Unplug from the extraction and clean up any mess. 
- Make sure the area is clean and that the tool is back in its box

## Use

### Changing the cutter

![domino image 4](assets/domino-image-4.png)
![domino image 5](assets/domino-image-5.png)

- To lift the unlocking lever (4-2) until it audibly engages, the 8 mm spanner (4-1) can be used for this
- Pull the guide (4-4) from the body of the tool (4-5)
- Press and hold the spindle lock (5-1) while using the spanner to unthread the cutter (5-2)
- Thread in the desired cutter by hand **note -** the cutter the will self-tighten with use
- Refit the guide to the body by lining up the rails of the guide to the holes in the body **note -** having the tool in vertical position and using a very slight twisting motion can aid with this step. 
- **note -** The cutter will extend through the guide when refitting and will hit and damage whatever is being used to support the guide


### Setting up the workpiece

![domino image 10](assets/domino-image-10.png)

- Always secure the workpiece with clamps to the work bench
- If working on a workbench and cutting horizontally, possition the workpiece very slightly over the edge of the workbench so that you can be sure the tool is sat against the edge of the workpiece and not the workbench. This will ensure correct alignment.
- Possition the two pieces as you wish to join them and use a pencil to mark across both so that the lines can be used to align the cutter and join the workpieces as intended (10-2)


### Setting up the tool

![domino image 1](assets/domino-image-1.png)
![domino image 2](assets/domino-image-2.png)

- Adjust the angle of the guide, most commonly to 0 or 90 degrees, by loosening the side lever (1-5) and using the gauge on the side
- Adjust the guide height for the correct material thinkness by loosening the black lever on the right of the tool (6-1) and moving the numbered slide (1-6) to the desired figure and resecuring the lever **note -** the number displayed is the total material thickness and the cut will be made at exactly half way
- Adjust the cutting depth by moving the notched green lever on the left of the tool (1-7) while simultaneously pressing the locking lever (1-8). The selection should be approximately half of the depth of the joining dowel being used. **note -** For the jointer bit with a diameter of 5 mm, only jointing depths of 12 mm, 15 mm and 20 mm are permitted due to its short shank length
- Adjust the cut width to match that of the joining dowel by turning the green switch on top of the tool (1-2)


### Cutting horizontally

- Be sure to have the workpiece clamped with the edge to be cut hanging over the edge of the work bench by roughly 5 mm. This helps to be sure that the Domino is square to the workpiece
- Place the top of the guide onto the workpiece with the edge of the guide against the edge to be cut. Use the arrows on the guide to the alignment line that was drawn previously
- The best way to handle the tool is with one hand on the handle (1-4) keeping the tool aligned and the other supporting the tool body
- The large green on/off switch (1-1) starts the drill **note -** the switch can be locked on if pushed forward and down
- To make the cut turn on the tool and with a smooth movement push the body of the tool into the workpiece until it reaches the cut depth limit 


### Cutting vertically

Note: in the vertical position, the tool must be running before making contact with the workpiece. Otherwise it will skip about and damage the workpiece. 

- Position the guide to 0 degrees
- Using the sprung-loaded guide tabs (2-4), the etched markers on the base of the Domino and the ruler guide can aid with accuracy and consistency
- Attach the additional support guide to the base of the Domino, or use another fence to aid in alignment
- The best way to handle the tool is with one hand on the handle (1-4) keeping the tool aligned and the other supporting the tool body
- The large green on/off switch (1-1) starts the drill **note -** the switch can be locked on if pushed forward and down
- To make the cut turn on the tool and with a smooth movement push the body of the tool into the workpiece until it reaches the cut depth limit 


### Cutting a mitre

- Same as cutting horizontally, be sure to have the workpiece clamped with the edge to be cut hanging over the edge of the work bench by roughly 5 mm. This helps to be sure that the Domino is square to the workpiece
- Place the top of the guide onto the workpiece with the edge of the guide against the edge to be cut. Use the arrows on the guide to the alignment line that was drawn previously
- The best way to handle the tool is with one hand on the handle (1-4) keeping the tool aligned and the other supporting the tool body
- The large green on/off switch (1-1) starts the drill **note -** the switch can be locked on if pushed forward and down
- To make the cut turn on the tool and with a smooth movement push the body of the tool into the workpiece until it reaches the cut depth limit 

## Member Induction sign-off form 

PLease review and sign the induction form: [Plus X Workshop Induction — Domino](https://forms.gle/r8MhNuHxVYwr3SDNA)

#### *this must be completed before any further use of the machine*