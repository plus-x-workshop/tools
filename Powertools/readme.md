last reviewed 2022-08-23 by KI

# Power Tools (Festools, drills and other handheld power tools) readme

serial number: [festool machines](https://plusxteam.sharepoint.com/:x:/r/sites/WorkShop/_layouts/15/Doc.aspx?sourcedoc=%7B57330AB3-8F04-496D-979A-5137FDA7BED7%7D&file=Workshop%20information.xlsx&action=default&mobileredirect=true&cid=43894818-0f4f-418f-8783-54019f443943)

## Our docs:

* [Festool Railsaw (Circular Saw) Induction](railsaw-induction.md)
* [Festool Router Induction](router-induction.md)
* [Festool Mitre Saw Induction](mitre-saw-induction.md)
* [Festool Jig Saw Induction](jig-saw-induction.md)
* [Festool Domino Saw Induction](domino-induction.md)

* [Extractor Information](extractors.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

## External docs

* [Festool Rail Saw manual](PDFassets/TS55REBQ-manual.pdf)
* [Festool Jig Saw User manual (PDF)](assets/jig-saw-manual.pdf)
* [Domino User manual (PDF)](assets/festool-domino-operation-manual.pdf)
* [Festool Router User manual (PDF)](assets/OF1010EBQ-manual.pdf)
* [Festool Railsaw (Circular Saw) User manual (PDF)](assets/jig-saw-manual.pdf)
* [Festool Mitre Saw User manual (PDF)](assets/mitre-saw-manual.pdf)
    * [Mitre saw extending table user manual (PDF)](assets/kapex-ks60-manual.pdf)

## Contact information

* Allen Steenkamp
    * Email: Allan.Steenkamp@festool.com
    * Phone: +44 (0)1284 760 791
    * Mobile: +44 (0)7795 505 242

### Repairs

* [website](https://plusxteam.sharepoint.com/:x:/r/sites/WorkShop/_layouts/15/Doc.aspx?sourcedoc=%7B57330AB3-8F04-496D-979A-5137FDA7BED7%7D&file=Workshop%20information.xlsx&action=default&mobileredirect=true&cid=43894818-0f4f-418f-8783-54019f443943)

* To get a machine repaired you first need to the [Festool website](https://www.festool.co.uk/) and then log in using [workshop information sheet](https://plusxteam.sharepoint.com/:x:/r/sites/WorkShop/_layouts/15/Doc.aspx?sourcedoc=%7B57330AB3-8F04-496D-979A-5137FDA7BED7%7D&file=Workshop%20information.xlsx&action=default&mobileredirect=true&cid=43894818-0f4f-418f-8783-54019f443943). You can then go to [request a repair](https://www.festool.co.uk/myfestool/repair-order/create) and put down all the information needed for each machine which you can find in the workshop information sheet under [festool machines](https://plusxteam.sharepoint.com/:x:/r/sites/WorkShop/_layouts/15/Doc.aspx?sourcedoc=%7B57330AB3-8F04-496D-979A-5137FDA7BED7%7D&file=Workshop%20information.xlsx&action=default&mobileredirect=true&cid=43894818-0f4f-418f-8783-54019f443943). 

## Issues

* [Issues tagged with Powertools](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=Powertools&first_page_size=20)

### Sanders 

* if the extractor is beeping when the sander are being used check the bag and if it is still beaping it is because the sucking dial needs to be turned down to 21. All others can be on 36 but the sanders have smaller extractor end so needs less air pulled through to let it work. 

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|
