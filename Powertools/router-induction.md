last reviewed 2021-10-09 by KI

# Festool Router OF 1010 EBQ 230V Induction

## Documentation

- Manufacturer website: https://www.festool.co.uk/products/routing/routers/577166---of-1010-rebq-set-gb-240v
- Summary manual (PDF): [OF1010EBQ-manual.pdf](assets/OF1010EBQ-manual.pdf)
- Manual (USA) (PDF): [OF1010-manual-USA.pdf](assets/OF1010-manual-USA.pdf)

## Materials 
+  OK: flat sheets materials: MDF, plywood, timber boards, aluminium 
+  Not suitable: hard metals, small pieces (that the router cannot safely rest on)


## Safety

- If the **Festool extractor** is beeping please get a member of staff for help and stop using it

### Setup:

- Use clamps or another practical way to secure and support the workpiece to a stable platform.
- Cracked or distorted cutters must not be used.
- Wear suitable protection: ear protection, safety goggles, steel-toe shoes. Do not use gloves when performing the routing operation (though you may need to when handling materials)
- Always use the integrated Festool dust extraction
- Always remove the power cable before changing the cutter


### In use:

- Hold tool by insulated gripping surfaces, because the cutter may contact its own cord
- Always switch the router on first before bringing the tool into contact with the workpiece
- Always advance the router in the same direction as the cutting direction of the cutter (counter-routing)
- ![](assets/router-cutting-direction.png)
- Do not force the tool. If the cutter seems blunt, or the material is difficult to cut, lower your plunge depth, slow your feeedrate or contact a member of staff for assistance.
- Do not overreach. Keep proper footing and balance at all times. Be aware of the location of the power cord and extraction hose

### After use:

- The router can be locked in the down position, in which case the cutter may be exposed. Always raise the router head after a cut has finished, and observe the bit stopping before laying the tool down 

### If routing aluminium:
  - use the metal-compatible dust extractor
  - Wear protective goggles
  - Clean the tool of accumulated metal dust


# How to use


## Changing the cutting bit

![](assets/router-change-cutting-bit.png)

Warning: Do not exceed the maximum speed specified on the tool and/or keep to the speed range. Cracked or distorted cutters must not be used.

Milling cutters with diameters over 30 mm should not be used with this machine.
Always remove the power cable before changing the cutter

Inserting the tool

- Insert the router (6.3) into the open clamping col- let as far as possible, but at least up to the mark
on the router shank.
- Turn the spindle until the spindle stop (6.1) catches when pressed and the spindle is locked in place.
- Tighten the collet nut (6.2) with a 19 mm open-end spanner.

Removing the tool

- Turn the spindle until the spindle stop (6.1) catches when pressed and the spindle is locked in place.
- Loosen the collet nut (6.2) using a 19 mm open- ended spanner until a resistance is felt. Overcome this resistance by turning the open-ended span- ner even further.
- Remove the cutter.

Clamping collet changing

- Fully unscrew the collet nut (6.2) and remove from spindle together with the clamping collet.
- Only insert a new clamping collet in the spindle when the nut is attached and engaged, then tighten the nut slightly. Do not tighten the collet nut until a milling cutter has been fitted!

## Setting the milling depth

![router milling depth](assets/router-milling-depth.png)

The milling depth is adjusted in three stages:

a) Setting the zero point

- Open the clamping lever (7.4) so that the stop cylinder (7.5) can be moved freely.
- Place the router with router table (7.7) onto a smooth surface. Open the rotary knob (7.8) and press the machine down until the milling cutter rests on the base. Clamp the machine tight in this position with the rotary knob (7.5).
- Press the stop cylinder against one of the three sensing stops of the pivoted turret stop (7.6).
- The individual height of each sensing stop can be adjusted with a screwdriver.
- Push the pointer (7.1) down so that it shows 0 mm on the scale (7.3).

b) Setting the milling depth

The desired milling depth can be set either with the quick depth adjustment or with the fine depth adjustment.
- Quick depth adjustment: Pull the stop cylinder (7.5) up until the pointer shows the desired mill- ing depth. Clamp the stop cylinder in this position with the clamping lever (7.4).
- Fine depth adjustment: Lock the stop cylinder with the clamping lever (7.4). Set the desired mill- ing depth by turning the adjusting wheel (7.2) in. Turn the adjusting wheel to the next mark on the scale to adjust the milling depth by 0.1 mm. One full turn adjusts the milling depth by 1 mm. The maximum adjustment range with the adjusting wheel is 8 mm.
 
c) Increasing the milling depth

Open the rotary knob (7.8) and press the tool down until the stop cylinder touches the sensing stops.

## While using

- Clamp your workpiece
- Plan and observe your cutting direction

This video gives a useful guide: <https://www.youtube.com/watch?v=mrE4HyXiwqs>

Note:
 - the proper direction to move the router changes depending on whether it's an iside or outside cut
 - if you're widening an existing cut, be aware of which edge of the router is cutting (e.g. if you're wideining a cut by moving the router back, you'll be using the back edge of the cutter. The back edge is cutting from left to right, so on an interior cut, you should move the router from left to right)
 - if you're using both edges of the cutter (eg to make an initial groove, forces are balnced, so cutting direction matter less)

External cuts

![](assets/router-external-direction.png)

Internal cuts 

![](assets/router-internal-direction.png)


### Routing with the FS guide system

![router rail](assets/router-rail.png)

The guide system (accessory) facilitates routing straight grooves.

- Fasten the guide stop (10.1) to the platen with the guide rails (10.3) of the parallel guide.
- Fasten the guide rail (10.5) with FSZ screw clamps (10.4) to the workpiece. Make sure that the safety distance X of 5 mm between the front edge of the guide rail and cutter or groove is observed.
- Place the guide stop onto the guide rail as shown in Fig. 10. To ensure a backlash-free guidance of the router stop you can adjust two guide cheeks with a screwdriver through the side openings (10.2).
- Screw the height-adjustable support (10.7) of the router table’s threaded bore in such a way that

### Routing with the parallel guide

![router parallel guide](assets/router-parallel-guide.png)

The parallel guide (9.8) (accessory) can be used for routing parallel to the edge of the workpiece.

- Clamp the two guide rods (9.6) to the parallel guide with the rotary knobs (9.3).
- Push the guide rods into the grooves (9.2) of the router table until the desired distance between the router and workpiece edge is reached. Clamp the guide rods tight with the rotary knobs (9.1).
This distance can be adjusted faster and more precisely with the fine precision adjustment (9.7) (accessory).
- Turn the adjusting screw (9.4) in the plastic part of the guide.
- Clamp the guide rods with the rotating knobs (9.5) in the precision adjustment.
- Loosen the rotating knobs (9.3) of the parallel guide, set the desired distance with the adjusting screw and retighten the rotating knobs.

## After use

- Clean the router of dust before returning to the box
- If you damage a cutting bit, or suspect any damage to the bit or the router itself, you must inform a member of staff. Do not return a possibly damaged router to the box.

## Suggested tasks

- Use a bearing bit to chamfer or roundover the edge of a workpiece
- Use the rail guide to route a [dado](https://en.wikipedia.org/wiki/Dado_(joinery)) in a straight line



## Member Induction sign-off form 

This is the form please follow this link [Plus X Workshop Induction — Router](https://forms.gle/YsBCHkk8W1kuuvU87)

#### *this must be completed before any further use of the machine*
