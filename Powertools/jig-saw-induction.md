last reviewed 2021-10-09 by AS

# Festool Jig Saw PSB 420 EBQ-Plus 240V Induction

## Documentation

- Manufacturer website: 
  - [Saw: CARVEX PSB 420](https://www.festool.co.uk/products/sawing/jigsaws/576189---psb-420-ebq-plus-gb-240v)

- Manuals (PDF): 
  - [jig-saw-manual.pdf](assets/jig-saw-manual.pdf)


## General Safety

**The key hazards come from your body parts being in the path of the blade or the tool kicking back material unexpectedly due to poor handling**

**This is a very dangerous tool if used when tired, or in a rush. Be careful, and do not skip precautions.**



- **You**: No loose clothing or hair. Wear steel toes and safety glasses. 
- **Extraction**: Always use the Festool extractor.
- **Materials**: Wood, plastic, aluminium only. Securely clamped to prevent the saw 'kicking up' when being used.
- **Damage to the tool**: do not use the saw if it appears damaged. If you damage the tool, tell a member of staff, so they can make it safe for the next person.
- **Workspace**: Secure placement. Nothing underneath workpiece.
- **Hands**: keep them away from the blade. Use clamps for small pieces.
- **Securing the workpiece**: Should be flat and secure against a workbench
- **Using the saw**: Let the blade reach full speed before con­tacting the workpiece. Remove it from the workpiece before releasing the trigger. The saw is exposed on the underside of your workpiece – **you can't see it – always check where it will come out**. 
- **Unpredictable movement** is often due to the condition of the material being cut or inadequate control of the tool. This is normally either side to side or violetly up and down if the saw blade catches. The best way to avoid this is to use guide rails (see below) and to be sure you can have firm grip throughout the cut.
- **Changing the saw blade** speak to a member of staff when wishing to change or replace the saw blade.


## Step-by-step safety and instructions

### Appropriate jobs

- For straight cuts use the band saw, mitre saw or rail saw
- For very detailed curved cuts, use the scroll saw (or a hand coping saw)
- The jigsaw should only be used when the pice of material hanging off the side of the workbench is small enough to be supported by hand

### Check tool

- Check blade is secure and undamaged (blade should face forward and cut on the up stroke)
- Check there is no damage to power cables 
- Report any damage or uncertainty to workshop staff


### PPE

- Tie back long hair.
- Do not wear loose fitting clothes that could become caught in the saw blade.
- Wear steel-toed shoes (if working with heavy materials). Your workpiece wil likely fall ont the floor/your feet, when you finish or near the end of the cut. 
- Safety glasses


### Extraction

- Always use the Festool extractor (in normal use, no dust mask or room extraction is necessary)
- Use the Festool extractor as the power supply
- If the **Festool Extractor is beeping** please get a member of staff for help and stop using it
- If you are cutting painted materials that may release hazardous dust, position an overhead chip collector close as possible, and switch on the room extraction (and wear ear defenders)

### Work area

- Be sure your work area is clear of additional hazards and has adequate space to properly support and seure your workpiece. See below for securing workpieces. 
- Be sure there is addequate clerance below the workpiece for the duration of the cut. 


### Securing workpieces

- Use clamps to support the workpiece whenever possible. If supporting the workpiece by hand, you must always keep your hand at least 100 mm from either side of the saw blade. Do not use this saw to cut pieces that are too small to be se­curely clamped or held by hand. 

### Adjusting sawblade stroke

- The speed of the saw blade stroke can be adjusted by turning the numbered green dial in the back of the tool. 1 - 5 are set speeds between 1500 - 3800 rpm. 'A' is an automatic setting that is load dependant, it speeds up as it detects load on the saw.
 A guide to speeds for different materials:

| Material | Speed |
| ------ | ------ |
| Hard and soft woods | A |
| Hardboard | A - 4 |
| Plastic | A - 3 |
| Ceramic and aluminium | 5 - 3 |
| Steel | 4 - 2 |

- The pendulum (back and forth movement) of the sawblade can be adjusted to process differnt materials. Is is adjusted using the green switch at the front left of the tool, between 0 and 3. 0 being off, 3 being maximum. 
 A guide to pendulum settings for different materials:
 
| Material | Speed |
| ------ | ------ |
| Hard and soft woods, chipboard and wood fibre board | 1 - 3 |
| Ply woods and plastics | 1 - 2 |
| Ceramic  | 0 |
| Steel and aluminium | 0 - 1 |

### Working with the tool

- When working, hold the electric power tool by the handle and guide it along the desired cutting line. For precise cuts and smooth running, use two hands to guide the electric power tool.
- To follow a predetermined line, the triangular pointer on the splinterguard indicates the cutting line of the saw blade. 
- To operate the tool, use the green variable speed trigger under the handle while pressing in the switch lock. Alternatively, for constant power, use either of the green power switches on the side of the tool. 
- Work slowly and do not force the tool too quickly through cuts or around tight corners
- Practice first, the jigsaw needs to be eased around corners, and getting a feel for how to do this takes time

### Cut line illumination

- A continuous light or stroboscope is installed for illuminating the cutting line: Up to approx. 2100 rpm: continuous light. From approx. 2100 rpm: stroboscope light
- **warning** The stroboscope light may make it difficult to de-termine the saw blade position, work in a well lit area.
- To change the light setting:
 -	Plug in the electric power tool.
 -	Press both buttons constant power buttons located on the side of the tool simultaneously and hold for approx. 10 s until a beep sounds.
 -	Release both buttons.
 -	Press the left button (on the pendulum stroke side) to select the stroke speed and consequently the desired operating.

| Mode | Indication during configuration | Behaviour during operation |
| ------ | ------ | ------ |
| 1 | Light flashes | With stroboscope (standard) |
| 2 | Light on | Permanent light without strobo-scope |
| 3 | Light off | Light switched off |

 - Press the right button to save the setting.


## Member Induction sign-off form 

This is the form please follow this link [Plus X Workshop Induction — Jig Saw](https://docs.google.com/forms/d/e/1FAIpQLSecDdbRGcaleBnARO5bKUpBN27saEzE2aaKC6PhyfEFkrdrlg/viewform?usp=sf_link)

#### *this must be completed before any further use of the machine*
