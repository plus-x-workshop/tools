last reviewed 2023-06-09 by AS

# Festool Mitre Saw KS 120 REB GB 240V Induction

## Documentation

Manufacturer website: 

- [Saw: KAPEX KS 120 REB](https://www.festool.co.uk/products/sawing/sliding-compound-mitre-saws/575304---ks-120-reb-gb-240v)
- [Underframe:  UG-KAPEX KS 120](https://www.festool.co.uk/accessory/497351---ug-kapex#Overview)
- [Trimming attachment: KA-UG-KS 120-R/L](https://www.festool.co.uk/accessory/497514---ka-ug-rl)

Manuals (PDF): 

- [mitre-saw-manual.pdf](assets/mitre-saw-manual.pdf)
- [Frame and extensions](https://gitlab.com/plus-x-workshop/tools/-/raw/master/Powertools/kapex-ks60-manual.pdf?inline=false)

## General Safety

**The key hazards come from your body parts being in the path of the blade, or materials catching in the blade and flying off the saw at high speed.**

**This is a dangerous tool if used when tired, or in a rush. Be careful, and do not skip precautions.**

- **You**: No loose clothing or hair. Wear steel toes and safety glasses.
- **Extraction**: Always use the Festool extractor.
- **Materials**: Wood, plastic, aluminium only.
- **Damage to the tool**: do not use the saw if it appears damaged. If you damage the tool, tell a member of staff, so they can make it safe for the next person.
- **Workspace**: Secure placement. Nothing behind machine. No loose tools or worpieces on the machine.
- **Hands**: keep them away from the blade. Use clamps for small pieces. Do not reach into the tool while it is running
- **Securing the workpiece**: Should be flat and secure against the table and rear fence, supported along its length.
- **Using the saw**: Let the blade reach full speed before con­tacting the workpiece. Remove it from the workpiece before releasing the trigger. Push, don't pull.


### Understanding the direction of force

- The saw blade turns down the front towards the back of the tool. 
- When cutting, the workpiece is pushed down and towards the back fence of the saw.
- Cut materials can be pushed out of the back of the machine at vey high speeds. Make sure no people or objects are behind the machine.
- Debris and offcuts can also bounce back and hit people or objects in front of the machine.
- Workpieces that are poorly secured or wedged against stop blocks can become caught in the blade, especially towards the end of the cut. These may fly in any direction at very high speed, or bind in the blade with unpredictable results. 


### Safe materials

- The mitre saw is for sawing blocks of wood, plastic, aluminium pro­files and similar materials. Do not use it to process other materials, in particular steel, concrete and mineral materials.
- No abrasive cutoff wheels to be used.
- Inspect your workpiece before cutting. There should be no nails or foreign objects in the workpiece.

When sawing aluminium, the following measures must be taken:

- Connect the saw to the metal-specific dust extractor (larger unit, with a black body: model `CTM48`).
- Regularly clean dust deposits from the mo­tor housing on the tool.
- Use an aluminium saw blade.

## Step-by-step safety and instructions

### PPE

- Tie back long hair.
- Do not wear loose fitting clothes that could become caught in the saw blade.
- Wear steel-toed shoes and safety glasses.

### Work area

- Position saw against clear wall or empty workbench.
- Make sure no people or objects are behind saw.
- Ensure the saw is on a level, firm work surface.
- Keep all power cables out of your way.
- Do not use the saw until the table is clear of all tools, wood scraps, etc., except for the workpiece. Small debris or loose pieces of wood or other objects that contact the revolving blade can be thrown with high speed.
- Cut only one workpiece at a time. Stacked multiple workpieces cannot be adequately clamped or braced and may bind on the blade or shift during cutting.

### Extraction

![mitresaw extraction](assets/mitresaw-extraction.png)

- Connect the Festool extractor to the connector on the back of the saw [9.1] (you must use the metal extractor if cutting aluminium (larger unit, with a black body: model CTM48)
- Connect the power cable of the saw to the 3 pin socket on the extractor, and set the extractor to auto, so it will switch on automatically when the saw starts.
- If the extractor is **beeping**, stop and ask for help to clear the blockage or change extractor bags.
- If you are cutting painted materials that may release hazardous dust, position the saw by an overhead chip collector, and switch on the room extraction (and wear ear defenders).

### Checking the tool

- Ask a member of staff if you would like to change the saw blade.
- Always use the correct saw blade for the material
- Check the blade before use, and alert a member of staff if you suspect damage to the blade (Deformed or cracked saw blades and saw blades with blunt or broken cutting edges must not be used.)
- Make sure that the rotational directions of the saw blade [7.6] and the ma­chine [7.1] correspond to each other. If you need to change the blade, please ask a member of staff.

![mitresaw directions](assets/mitresaw-directions.png)


– Before each use, check that the pendulum guard is working correctly. Only use this power tool when it is in perfect working or­der.
– Never reach into the chip ejector with your hands. Rotating parts may injure your hands.


### Securing workpieces

The workpiece must be stationary and clamped or held against both the fence and the table. 

- Do not feed the workpiece into the blade or cut “freehand” in any way. 
- Use clamps to support the workpiece whenever possible. If supporting the workpiece by hand, you must always **keep your hand at least 100 mm from either side of the saw blade**. Do not use this saw to cut pieces that are too small to be se­curely clamped or held by hand. 
     - Use the workpiece clamps [8.1] to hold down at least one end of the workpiece. 
- Provide adequate support such as table extensions, saw horses, etc. for a work­ piece that is wider or longer than the table top. 
- Always clamp the workpiece securely, especially long, heavy sections. After the machine has cut all the way through the workpiece, the centre of gravity may shift suddenly and cause the base frame to tip over.
- You can use quick clamps to secure the workpiece against the back fence.
- Do not use another person as a substitute for a table extension.
- The cut-off piece must not be jammed or pressed by any means against the spin­ning saw blade. If confined, i.e. using length stops, the cut-off piece could get wedged against the blade and thrown vio­lently.
- Always use a clamp or a fixture designed to properly support round material such as rods or tubing. Rods have a tendency to roll while being cut, causing the blade to “bite” and pull the work with your hand into the blade.
- If the workpiece is bowed or warped, clamp it with the outside bowed face toward the fence. Always make certain that there is no gap between the workpiece, fence and table along the line of the cut. 


**Moving the rear fences**

These can be moved in or out to provide more support by releasing the green levers behind the fences. You may need to remove the top-down clamps first. These can be freely pulled out when they are turned fully backwards behind the fence.

**CAUTION** When cutting mitres the rear fences may interfere with the saw blade if they are pushed in to the centre position. Move these out to the widerist position beforte making a horizontal mitre cut.

**Fitting the table extensions ("Trimming attachment KA-UG-KS")**

![mitre saw extentions combined](assets/mitre-saw-extensions-combined.png) 

### Making a cut

- If the blade is stored in a locked-down position, pull out the lock [4.2] to release it
- Check front to back lock is on/off as appropriate
- Check the vertical angle dial on the rear of the machine is in the 0 &deg; position. There is a slight detent at this position. Check the blue cap over the green mitre angle know is down.
- Bring down the saw with the handle, pulling the trigger underneath the handle [1.2]
- Start the blade with the safety lock button on top [1.3]


### Safety when cutting

- Let the blade reach full speed before con­tacting the workpiece. This will reduce the risk of the workpiece being thrown.
- If the workpiece or blade becomes jam­med, turn the mitre saw off. Wait for all moving parts to stop and disconnect the plug from the power source and/or re­move the battery pack. Then work to free the jammed material. Continued sawing with a jammed workpiece could cause loss of control or damage to the mitre saw.
- Hold the handle firmly when making an in­complete cut or when releasing the switch before the saw head is completely in the down position. The braking action of the saw may cause the saw head to be sudden­ly pulled downward, causing a risk of in­jury.


### Do not use length stops

The cut-off piece must not be jammed against or pressured by any other means against the spinning saw blade. If confined, i.e. using length stops, if could get wedged against the blade and thrown violently.

If you need to make repeated identical cuts, use masking tape to mark a stop line for the loose piece of material.

### Keep your hands away from the saw blade

- Never cross your hand over the intended line of cutting either in front or behind the saw blade. 
- Supporting the workpiece – “cross handed” i.e. holding the workpiece to the right of the saw blade with your left hand or vice versa is very dangerous.
- Do not reach behind the fence with either hand closer than 100 mm from either side
of the saw blade, to remove wood scraps, or for any other reason while the blade is – spinning. The proximity of the spinning saw blade to your hand may not be obvious and you may be seriously injured.

### Cutting deeper workpieces

If the workpiece is deeper (front to back) than a few centimetres, you will need to use the forward-back axis on the saw.

If the saw is locked in its front-back travel, release the rotary knob [4.3]. You can clamp the saw in any position on these front-back rods by re-tightening this knob.

![mitresaw locks](assets/mitresaw-locks.png)

- **Push** the saw through the workpiece. **Do not pull** the saw through the workpiece. 
- To make a cut: 
  - raise the saw head and pull it out over the workpiece without cutting 
  - start the motor, 
  - press the saw head down – and push the saw through the workpiece, away from your body towards the back of the machine

Cutting on the pull stroke is likely to cause the saw blade to climb on top of the work­ piece and violently throw the blade assem­bly towards the operator.


#### Horizontal mitres


**CAUTION** When cutting mitres the rear fences may interfere with the saw blade if they are pushed in to the centre position. Move these out to the widerist position beforte making a horizontal mitre cut.

The horizontal mitre angle can be continuously adjusted between 50° (left side) and 60° (right side). In addition, there are snap-in positions for common mitre angles.

The arrow of the indicator [13.2] points to the set horizontal mitre angle. The two markings on the right and left of the indicator arrow enable precise setting of half-degree angles. For this to work, both of these markings must line up with the degree dashes on the scale.

The following mitre angles have snap-in posi­tions:

- Left: 0°, 15°, 22.5°, 30°, 45° 
- Right: 0°, 15°, 22.5°, 30°, 45, 60°

- Move the machine into the working position. 
- Pull the clamping lever [13.5] upwards.
- Press the notch lever [13.4] downwards.
- Rotate the saw table until you get to the mi­tre angle you want.
- Release the notch lever. You must feel the notch lever engage.
- Press the clamping lever downwards.

Optional horizontal mitre angles:
- Move the machine into the working position. 
- Pull the clamping lever [13.5] upwards.
- Press the notch lever [13.4] downwards.
- Rotate the saw table until you get to the mi­tre angle you want.
- Press the clamping lever downwards. 
- Release the notch lever.

![mitresaw horizontal](assets/mitresaw-horizontal-mitre.png)



### Laser guide

The machine has two lasers which mark the kerf to the left and right of the saw blade. You can use them to align the workpiece on both sides (left or right side of the saw blade/kerf).

- Press the button [2.1] to switch the laser on or off. If the machine is not in use for 30 mi­nutes, the laser will automatically switch it­ self off and must be switched on again.

![mitresaw back](assets/mitresaw-back.png)

- Never direct the laser beam at people. It may cause accidents as a result of the glare.
- Never look directly into the laser beam or its reflection. However, if you make direct contact with the laser beam, close your eyes immediately and move your head from the beam. Direct eye contact with the laser beam can cause damage to the eye.
- Do not make any modifications to the la­ser. 
  

### After use

- If you noticed any damage or unusual behaviour while cutting, please inform a member of staff, so we can ensure the machine is safe for the next member to use.
- The blade is stored in a locked-down position. Lower the blade and push in the lock [4.2] to lock it down.
- Clean all debris from the machine and working area
- Disconnect all extra supports and put away

## Blades

We have the following blades:

- [Saw blade WOOD UNIVERSAL HW 260x2,5x30 W60 (494604)](https://www.festool.co.uk/accessory/494604---260x2,5x30-w60)
- [Saw blade WOOD FINE CUT HW 260x2,5x30 W80 (494605)](https://www.festool.co.uk/accessory/494605---260x2,5x30-w80)
- [Saw blade ALUMINIUM/PLASTICS HW 260x2,4x30 TF68 (494607)](https://www.festool.co.uk/accessory/494607---260x2,4x30-tf68)

Always use the correct blade. If in doubt, ask.


## Advanced Operation


Please consult the manual (PDF linked above) if you want to:

- Transfer an angle using the bevel [8.7]
- Cut a vertical mitre [7.8]
- Change the stop area behind the blade [7.6]
- Limit the depth of the cut [7.10]

#### Blades 

**--- Must only be changed by a member of staff ---**

Only use Festool saw blades that are designed for use in this power tool. The saw blades must comply with the following data:

- Saw blade diameter 260 mm
- Cutting width 2.5 mm (corresponds to the tooth width)
- Locating bore 30 mm
- Standard blade thickness1.8 mm
- Saw blade in accordance with EN 847-1
- Saw blade with chip angle ≤ 0°

Festool saw blades for woodworking comply with EN 847-1.

Only saw materials for which the saw blade in question has been designed. 

| Colour | Material | | |
|--|--|--|--|
| Yellow | Red | Green | Blue |
| Wood | Laminated wooden pan­els | Eternit fibre cement panels | Aluminium, plastic|

## Induction task

- Cut a series of 90° cuts in a piece of scrap wood - on a mark
- Cut a series of 45° mitred cuts

## Member Induction sign-off form 

Please review and sign this form: [Plus X Workshop Induction — Mitre Saw](https://forms.gle/y4t9Vxmuapza4f8H6)

#### *this must be completed before any further use of the machine*

