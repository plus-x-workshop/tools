Last reviewed 2022-08-23 by KI

# Electronics Tools readme

serial number: 

* Mixed Signal Oscilloscope and PCBite: MS5A224606566

### Covers the core electronics tools in the fablab:

* Weller WE 1010 Soldering Stations (70W, 230V)
* Weller heat gun for soldering rework

* ESD-protected electronics benches

* Rigol MSO5204 200MHz BW, 4 Channel, 8 GSa/S Digital Oscilloscope
* PCBite kit with 2x SP200 200 Mhz hands-free oscilloscope probes

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintanence.md)

* [How to Solder](how-to-solder.md)

## External docs

* [Rigol MSO5204 Oscilloscope user manual (PDF)](rigol-mso5000-manual.pdf)
* [Rigol website](https://int.rigol.com)

* [Weller soldering iron user manual (PDF)](weller-guide)

* [PCbite information (PDF)](pcbite-info)
* [PCBite website](https://sensepeek.com/)

## Contact information 

## Issues

* [Issues tagged with Electronics](https://gitlab.com/plus-x-workshop/tools/-/issues?scope=all&utf8=✓&state=opened&label_name[]=Electronics)

## Services

<!-- All machine issues should be tagged with this machine name -->

<!-- Edit this link appropriately -->

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|