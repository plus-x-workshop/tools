# How to Solder Surface Mount Components

Guide: 
- [Adafruit Guide To Excellent Soldering](https://learn.adafruit.com/adafruit-guide-excellent-soldering/surface-mount)

Videos:  
- [SMD Soldering - the easy way - YouTube](https://www.youtube.com/watch?v=wEBrvfWwzuQ) (Arthur Guy)  
- [EEVblog #186 - Soldering Tutorial Part 3 - Surface Mount](https://www.youtube.com/watch?v=b9FC9fAlfQE) (enthusiastic Aussie, unavoidable)
- [Basic Soldering Lesson 1 - "Solder & Flux"](https://www.youtube.com/watch?v=vIT4ra6Mo0s&index=1&list=PL926EC0F1F93C1837) (excellent retro series, especially this video on materials: copper, solder and flux and how they work) 

## Remember:

* Clean your board to remove copper oxide and finger oils
* Keep soldering iron tip shiny and clean
  * Switch the iron off when you're not using it
  * Don't be tempted to use too high a temperature (that is not why your solder isn't melting)
* Melting solder wets the joints
* Heat both parts of the joint with the iron
* Feed in small amount of solder - helps conduct heat
* Wait – learn to see the signs of a good flow and joint forming
* Once parts are at the right temperature, more solder will flow up and down parts
* Wait, then remove iron
* Check your work – you want shiny, smooth joints


## Recognising a bad joint

[Common Soldering Problems - Adafruit Guide To Excellent Soldering](https://learn.adafruit.com/adafruit-guide-excellent-soldering/common-problems)

[Solder joints](assets/solder-joints.jpg)

## Techniques

### Holding the board

* Tape
* Vice

### Holding the part

* Tacking 
* Note orientation of chips, capacitors, diodes, etc.
* Order of stuffing - inside out

### Desoldering

* Braid - tin the braid first to help heat flow
* Pump
* Hot air gun - use directional tip (plus tweezers and gravity). Clean up with braid afterwards.

## Practice board

We can mill a simple practice board that will give you plenty of solder joints to try, at increasing levels of difficulty. 
Better still, complete an SRM-20 induction, and you can make the whole board yourself.

[Download soldering practice board designs](assets/hello-soldering/)

