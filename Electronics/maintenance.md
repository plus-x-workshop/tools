Last reviewed 2022-08-18 by KI

# Weller Soldering Iron/ Electronics Tools maintenance

### Manuals 

- Rigol MSO5204 oscilloscope user manual (PDF): [rigol-mso5000-manual.pdf](rigol-mso5000-manual.pdf)
- Weller soldering irons guide (PDF): [weller-guide.pdf](weller-guide.pdf)
- PCBite information (PDF): [pcbite-info.pdf](https://sensepeek.com/)

### Firmware Updates

n/a

### General Maintenance

- Verify that soldering iron tips are undamaged
- Verify that soldering iron power cords are undamaged
- Verify all test equipment tools and leads put away
- Check contents of members consumables box: solder, braid, standard milling bits and wrenches, solder sucker, etc.)
- Clean up electronics assembly areas

### Replacement parts

- Check with Andrew for replacement parts to be purchased or the budget for previous purchasing 

### Soldering irons

These irons take ET tips. For full details, see the [weller guide](weller-guide.pdf)

For SMD soldering, we recommend, according to personal preference: 

- ET HL Chisel tip (0.8 mm x 0.4 mm)
- ET SL Chisel tip (0.3 mm)
- ET P Round tip (0.8 mm)

### Solder

Offer lead-free and leaded solder in small diameters (0.5 mm), e.g. 

- Lead-free: <https://uk.rs-online.com/web/p/solder/0555272/>
- Leaded: <https://uk.rs-online.com/web/p/solder/1796666/>

### Solder braid

Narrow solder braid, such as <https://uk.rs-online.com/web/p/solder-wicks/3829810/>