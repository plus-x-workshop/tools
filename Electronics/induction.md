Last reviewed 2021-10-09 by AS

# Weller Soldering Iron/ Electronics Production Induction

This induction covers the use of:
- Soldering Irons
- Hot air guns for reworking
- Manual electronics assembly

## Safety

- Please review and make sure you understand the risks outlined below before using any of the tools. Eye protection should be work when soldering or using side-cutters to trim components.
- Do not carry out any modifications or repairs to the tools – alert a member of staff if you notice anything wrong.

## Soldering iron temperature settings

- Soldering irons should be kept at the minimum viable temperature, and always switched off when not in use, even for a short period.
- For 40/60 tin/lead solder, 230°C is hot enough to melt the solder (though you can raise the temperature to 300-350°C for faster working times). 

## Cleaning

- Ensure the tip is clean and free of oxidation (black colouring) before starting. Use a brass wool cleaner or damp sponge to clean the tip.
- If the soldering iron doesn’t seem to be hot enough, ensure the tip is clean – do not just turn up the temperature.
- Tin the tip before starting and frequently while working.

## Soldering Iron Tips

- We provide tips in 0.4 and 0.8 mm sizes for SMD soldering. If you need to use a different size tip, or would like to provide your own, please ask a member of staff first.
- Small tips are fragile and must be used with care. Users will be charged for damaged tips at cost. Please ensure the tip is clean and undamaged before use.

## Solder

- We provide small quantities of leaded and lead-free solder, if you prefer. For large jobs, please bring your own.
- Understand that lead is hazardous to your health, and if you choose to use it, you do so at your own risk. Always wash your hands before and after soldering, and before consuming any food. **No food or drink is allowed in the fablab.**

## Ventilation

- For quick jobs specialist ventilation is required beyond the standard AC ventilation. Soldering stations are intended for small production jobs and rework, not large production runs. Ensure that you do not lean over the work and breathe in the fumes, and take a break if you’re working for a longer period of time.
- Use the desktop extractor fan to draw fumes away from you if required.

## Rework

- You can use the hot air gun, vice and pliers to assist in any rework. Please follow all safety precautions when handling hot tools or components. Be careful in the use of the hot air gun; always know where you are diecting the heat.

## Testing

- We provide an oscilloscope and multimeter for testing. The oscilloscope comes with its own probes. 
- In addition, there is a PCBite testing station, with magnetic PCB mounts and it's own hands-free probes, that can also be used with our oscilloscope.


## ESD Protection

- ESD mats and wrist bands are provided. Note that wristbands are shared between all users of the workshop.

## Protecting the work surface

- The table surfaces and ESD mats are not sacrificial surfaces. Please take care to avoid damage to the work surfaces. Never rest a soldering iron or hot tool on a work surface.

## After you’ve finished

- Switch off the soldering iron.
- Tidy away any tools and put any waste in the correct bins.
- Leave the workstation tidier than you found it.


# How to solder

Soldering is easy! If this is your first time soldering, there are lots of guides online, and we can help too.

See **[How to solder](how-to-solder.md)**.

## Member Induction sign-off form 

This is the form please follow this link [Plus X Workshop Induction — Soldering](https://forms.gle/pnyotCmHKitnjGFm7)

#### *this must be completed before any further use of the machine*
