last reviewed 2022-10-06 by KI

# Singer Heavy Duty sewing machine Induction

## Setting up the machine  
* Plug the sewing machine into the wall and place the peddle onto the floor. 
* Make sure you have enough room to work with your materials.

## Changing thread/ bobbin
![winding-bobbin](assets/winding-bobbin.png)

![inserting-bobbin](assets/inserting-bobbin.png)
![threading-upper-thread](assets/threading-upper-thread.png)

## Sewing  
![auto-needle-threader](assets/auto-needle-threader.png)

## Cleaning  
 
* Turn off the machine and put it all back with the peddle and cover
* Remove all your scraps from the cutting area – place them in the bins provided  
* Place all used items back in the craft corner where found
* Make sure the area is clean before leaving the machine 
  
## Member Induction sign-off form 

This is the form please follow this link [Plus X Workshop Induction — Sewing Machine](https://forms.gle/AvAo5voNH5HUtVZT8)

#### *this must be completed before any further use of the machine*