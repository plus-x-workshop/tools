last updated 2022-08-18 by KI

# Singer Heavy Duty sewing machine maintenance

[User Manual PDF](assets/singer4432-manual.pdf)

### Firmware Updates

n/a

### General Maintenance

* Inserting & Changing Needle: *page 53*
* Troubleshooting Guide: *page 55*

- turn on and make sure the peddle is working

### Replacement parts

- Hobby Craft 

* [Maintenance guide pg 53, Trouble Shooting pg 54](https://johnlewis.scene7.com/is/content/JohnLewis/232868876mnl1pdf?)
