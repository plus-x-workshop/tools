last reviewed 2022-08-23 by KI

# Singer Heavy Duty sewing machine readme

serial number: n/a

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

## External docs

* [User manual (PDF)](assets/singer4432-manual.pdf)

## Contact information

## Issues

* [Issues tagged with sewing machine](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=sewing%20machine&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|