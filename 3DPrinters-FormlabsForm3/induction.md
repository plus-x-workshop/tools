Last reviewed 2024-11-01 by AS

# Formlabs Form3 SLA 3D Printer + Form Wash/Cure Induction 


## Safety 


|  **Hazard**                                                  | **Control**                                                  |
|--------------------------------------------------------------|--------------------------------------------------------------|
| **COSHH** Handling un-cured resin (or IPA)  <br>Skin irritant: Resin may cause skin irritation or an allergic skin reaction.<br><br>Resins and IPA may cause eye irritation | PPE: Wear gloves when handling liquid resin or resin-coated surfaces.  Wash skin with plenty of soap and water.<br><br>PPE: Eye protection to be worn   |
| **Manual Handling** <br>Cuts from sharp tools or fragments of cured resin | Orient sharp tools away from yourself, especially when cutting or scraping.<br><br>PPE: Eye protection to be worn   |
| **Fire** <br>IPA solvent used in washing                     | IPA to be kept in COSHH cabinet when not in use <br>All washing to be done away from hot works in wet area   |
| **Manual Handling** Flying debris while cleaning parts       | PPE: Eye protection to be worn                               |
| **Electricity**<br>Risk of injury due to faulty equipment, contact with live electrical components or improper use. | Machine subject to regular inspections and maintenance. Machine to be inspected before use. |
| **UV Radiation**<br>Risk of damage to skin or eyes from exposure to UV light | Do not circumvent door interlocks.                           |
| **COSHH** Spills while fitting new tanks or carrying tanks, or inspecting tray | PPE: Eye protection and gloves to be worn .<br><br>Tanks and cartidges to be changed by staff only. <br> |


## Materials

You can use our resin, and fill out your usage on the form nearby the printers. 
Our current pricing of available materials: 
- Formlabs Clear/Black/White - £2/10ml - good for aesthetic + optically clear projects
- Formlabs Rigid 10k - £4/10ml - expensive but amazing detail reproduction
- Third party resins are prohibited for safety reasons, please ask a member of staff if you wish to use a resin we don't have. 

## Specs
- Build volume: 145 × 145 × 185 mm
- Z resolution: 0.025 - 0.160 mm

<!-- **x/y resolution: SLA, not MSLA, so no pixels** -->

## Helpful videos 

- This video describes the areas to think about when designing and placing your piece onto the bed: [Why Resin 3D Prints Fail - Improve Your Prints - Tips on Understanding Overhangs and Supports](https://www.youtube.com/watch?v=pbYAhjASGFY&ab_channel=Nerdtronic)

- [From Design to 3D Print With the Form 3](https://www.youtube.com/watch?v=xf9oBcTzMFo&ab_channel=Formlabs)



## Software Workflow – Preform

***`On PreForm the print is set up UPSIDE DOWN on build plate`***

* Export file as an STL and open in PreForm
* Check for any software updates – ask us if you are unsure 
* Job Setup:
    * Check you are connected to the right printer
    * Check you have the correct cartridge in and that the tank matches
    * Check the resin is correct (colour and type)
    * On the cartridge they have ‘version’ codes double check you have selected the correct one for your cartridge
    * Print setting should be on ‘default’
    * Layer thickness should be in ‘fixed’
    * Choose the quality of your print.
    * Then press apply

![Job Setup](assets/job_setup.png)

* Centre your model so that it's in the middle of the build plate. Use the 4-way arrow to move it around and the top right arrow to enlarge or reduce your model.
* Using the orientation tool (or the freehand rotate tool seen in step 2) orient your model so that the side with the least detail is faced down to the build plate. (this will make it easier to clean up)

* Click this support icon to generate supports
* You can edit the existing supports or clicking anywhere on the part to add new ones – the size of the supports can also be adjusted
by tweaking the density and touchpoint size
* If an area goes red it needs more supports – all the thumbs up on the right side should be green 

![Supports](assets/supports.png)

* Once you are happy with the model, click this *orange icon* to upload it to your + Check the printer is correct

![Button](assets/button.png)

* The cartridge and the tank are the same resin and you have enough for your print
* ‘Job Name’ is correct
* Any incorrections you will be asked in orange and should be changed
* When all complete press upload job

![Print](assets/print.png)

![Tools](assets/tools.png)
* All parts must be cleaned with Isopropyl alcohol after removal from the printer.
* Wear gloves and eye protection when handling any uncured parts or resin.
    * UV Cure resin is extremely hazardous, and poses long-term health risks with repeated contact. 
    * [data sheet for liquid resin](https://support.formlabs.com/s/article/Resin-Care?language=en_US)
    * ***WASH HANDS WITH SOAP IMMEDIATELY IF YOU CONTACT UNCURED RESIN***
* wear eye protection when post-processing parts – support material can fly off at high speed when cut.
* Do not open the lid while the printer is running. 


## Post-processing

<!-- * Get both the form-wash and form-cure down from the shelf next to the sink
    * Find a flat area with access to 2 sockets to set them up, away from any heat / ignition source.  -->
* Ask a member of staff if you require more IPA
* On the form-wash, scroll and click "open"
* Place the build platform, with your model still attached, on the holder. 
* Scroll and select your duration, and press start.
    * Duration can vary depending on material and part complexity. 20 minutes is a good start.
* Once your part is washed, remove it from the form-wash, and remove your part from the plate. 
    * Removal can be tricky, formlabs provided a number of tools to aid removal, these live on the back of the form wash. 
    * Be careful when using sharp tools to remove parts, never apply excessive force, always orient the tool away from your body.
* Place your removed part in the form-cure, and start a curing cycle appropriate for your material. 
    * Timings and Temperatures for different materials can be found [here](https://support.formlabs.com/s/article/Form-Cure-Time-and-Temperature-Settings?language=en_US)



## Member Induction sign-off form

[Plus X Workshop Induction — Formlabs](https://forms.gle/Pqa44v7pdkrxC8bYA)

#### *this must be completed before any further use of the machine*




