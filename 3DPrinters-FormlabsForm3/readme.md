Last reviewed 2022-08-24 by KI

# Formlabs Form3 SLA 3D Printer readme

serial number: 

* HandyAmoeba
* HappyMinotaur

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

## External docs

* [User manual (PDF)](assets/formlabs-manual.pdf)

## Contact Information

* [Choose a Service Website](https://formlabs.com/uk/services/)

## Issues

* [Issues tagged with Form3](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=Form3&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)| 