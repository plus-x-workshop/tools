last reviewed 18-08-2022 by KI

# Formlabs Form3 SLA 3D Printer maintenance

[User Manual PDF](assets/formlabs-manual.pdf)

### Firmware Updates

- turn on the computer and the mahcines and open up formlabs software 'ProForm' and if an update is need the machine will automatically ask you to installe it.

### General Maintenance 

- check the computer and remove any files from members that have been saved

pages 20-25 in [User Manual PDF](assets/formlabs-manual.pdf)

* Inspecting the Product: *page 21*
* Inspection Tasks Between Prints: *page 22*
* Monthly Inspection and Maintenance Tasks: *page 22*
* Periodic Inspection and Maintenance Tasks: *page 22*
* Planned Maintenance Procedures: *page 24*

- [Formlabs Services](https://formlabs.com/services/)
- [Formlabs Support](https://support.formlabs.com/s/topic/0TO1Y000000IvrVWAS/form-3?language=en_US)
- [Maintaining the resin tank (Form 3/Form 3B)](https://support.formlabs.com/s/article/Maintaining-the-Resin-Tank-Form3?language=en_US)

### Replacement parts

- [Accessories](https://formlabs.com/store/accessories/)

