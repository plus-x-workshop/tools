Last reviewed 2021-10-09 by KI

# Trotec Q500 laser cutter (large) Induction

2021-08-24: TODO Just copied and pasted from [Word Doc](https://plusxteam.sharepoint.com/:w:/r/sites/WorkShop/_layouts/15/Doc.aspx?sourcedoc=%7B18F7FE30-51FF-4AA6-9D00-ADD4C7165B15%7D&file=Speedy%20300%20Induction.docx&action=default&mobileredirect=true)
 
### Trotec Q500 Laser Cutting Training 

**`This machine is for cutting if you are wanting to engrave use the Speedy 300 laser engraver – ask a member of staff if unsure`**

## Software workflow 

### We support Inkscape for vector editing and Ruby for sending files to the laser cutter. Jobcontrol is available for legacy usage. 
Inkscape  
* Open your file up in Inkscape  
* Go to File > Document properties - check bed size dimensions (it’s in mm) set this to bed size of the machine (1300 mm x 900 mm) 
* Select your artwork – shift+Ctrl+F 
* You need to change the colours of each part of your job to make sure they correlate to the right movements of the machine  
* Basic cutting 
	+ Go to the ‘fill’ tab and check it is none 
	+ Go to ‘stroke style’ tab and the width should be 0.010 mm  
* Basic etching 
	+ Go to the ‘fill’ tab and check it is RGB with green(G), red(R) and blue(B) all to 0 
    + Note any strokes will also be engraved. Set stroke to none if necessary. 
 

## Materials 

* Places to buy from: Kitronik and Hobarts

* Many materials cannot be cut on this machine without damaging the machine or you.

* Under no circumstances should you attempt to cut or engrave:      
    + Mirrored acrylic (with the mirror facing up) 
    + Shiny metal 
    + PVC  
    + Any material for which you cannot provide the correct material data sheet 
    + Any material of unknown origin (e.g. salvaged scrap) 

* In general, acceptable materials are 
    + Laser plywood 
    + Cardboard and paper 
    + Polypropylene 
    + Acrylic 
    + Felt 
    + Leather 
    + Anodised aluminium can be engraved as long as it has a dull finish 
    + Laser rubber 
    + 2-colour laminate 
    + MDF

If in any doubt about material safety, consult a member of staff first.  

## Machine – Set up 
 
* Turn machine on by logging into Fabman – the chiller is attached also to the Fabman and must be running at all times – it will make a very loud beeping noise to tell you its on
* Twist the key by the emergency stop button to on
* Check that the Chiller has turned on (this is the white box next to the laser cutter the screen will have red numbers on it – ask a member of staff if unsure)
* Leave it to initialise – the laser will move around and go to the top left-hand corner
* It will beep when ready 

## Focusing  
* On the track pad use the Y and X across to move the laser to the front of the bed (the bed doesn’t move)
* Place the focusing tool on the left side of the red laser head (focusing tool is located in the trolley, in the draw saying Q500, in a box – this should be placed back in the box in the correct draw!)
* Place your material on the bed
* Use the large silver knob on the top to move the whole red laser head up and down (check the brass knob under the large silver knob is loose)
* Move the red laser head down slowly until the focusing tool touches your material 
* Remove the focusing tool and place back in correct box and draw

## TROTEC RUBY

#### Everything written below is copied from [RubyHelp.com](https://www.rubyhelp.com/article-categories/guide/).

[Ruby Video](https://www.youtube.com/watch?v=ZHDKHiww4uY&ab_channel=TrotecLaser)

**You no longer have to do the correct colours or the correct line thickness in your vector file you can change these in ruby!**

* On the computer desktop click on the Ruby icon "Trotec Ruby" this will open up Ruby in the web browser. Make sure there is only one Ruby Tab open (check left side of browser). 
* If you need to log in this should be auto filled with 'workshop@plusx.space' and then password (please speak to a member of staff if you can't log in)
* Once into Ruby follow the steps below.

## Connecting to the machine

* Once scanned in with fabman, press the green power button on the e-stop box to turn the laser on

* Once open, the machine (if switched on) will automatically connect to **'Ruby'** 
* If it doesn't connect right click on the Ruby icon on the bottom right of the bar and hover over **'laser mode' (blue)** then make sure **'Ruby'(red)** is selected.

**` --- Notes: makesure job control is not open --- `**

![connecting+ruby](assets/connecting+ruby.jpeg)

## Manage Screen

[Ruby Help Manage Screen](https://www.rubyhelp.com/knowledge-base/management-screen/)

* The Manage Screen is the starting phase. Here you get a overview about your current Designs, Jobs and connected Laser.

* Drag your vector file onto the manage screen 

![manage_screen01](assets/manage_screen01.png)

This screen has 2 main areas:

- Management Area
- Lasers

### Management Area

![manage_screen02](assets/manage_screen02.png)
![manage_screen03](assets/manage_screen03.png)

### Lasers

![manage_screen04](assets/manage_screen04.png)

* Here you can see which laser is connected or available

## Design Screen

[Ruby Help Design Screen](https://www.rubyhelp.com/knowledge-base/design-screen/#shortcut-design)

* The Design Screen is for setting up your design before thinking about laser parameters, you can do rudimentary editing here. The toolbar has simple graphics tools. 

![design_screen01](assets/design_screen01.png)

* This screen has 3 main areas:

    * Design list
    * Toolbar
    * Right panel

### Design list

![design_screen02](assets/design_screen02.png)
![design_screen03](assets/design_screen03.png)

### Toolbar

![design_screen04](assets/design_screen04.png)
![design_screen05](assets/design_screen05.png)

### Right panel

![design_screen06](assets/design_screen06.png)

* Attributes
* Object list

### Attributes

![design_screen07](assets/design_screen07.png)
![design_screen08](assets/design_screen08.png)

### Object list

![design_screen09](assets/design_screen09.png)
![design_screen10](assets/design_screen10.png)

### Options to list your objects

![design_screen11](assets/design_screen11.png)
![design_screen12](assets/design_screen12.png)

## Prepare Screen

[Ruby Help Prepare Screen](https://www.rubyhelp.com/knowledge-base/prepare-screen/)

* The Prepare Screen is there to finalize your created job and push it to the laser. There are multiple settings to adjust like the Material or the Processing rules.

![Prepare_screen01](assets/Prepare_screen01.png)

This screen has 3 main areas:

* Job list
* Toolbar
* Right panel

### Job list

![Prepare_screen02](assets/Prepare_screen02.png)
![Prepare_screen03](assets/Prepare_screen03.png)

### Toolbar

![prepare_screen04](assets/prepare_screen04.png)
![prepare_screen05](assets/prepare_screen05.png)

### Right panel

![Prepare_screen06](assets/Prepare_screen06.png)

The Right panel is separated in 3 areas:

* Production
* Design
* Laser rules

### Production

![Prepare_screen07](assets/Prepare_screen07.png)
![Prepare_screen08](assets/Prepare_screen08.png)

### Design 

![Prepare_screen09](assets/Prepare_screen09.png)
![Prepare_screen10](assets/Prepare_screen10.png)

### Laser Rules 

![Prepare_screen11](assets/Prepare_screen11.png)
![Prepare_screen12](assets/Prepare_screen12.png)

## Produce Screen

[Ruby Help Produce Screen](https://www.rubyhelp.com/knowledge-base/produce-screen/)

* In the Production Phase you have a overview of the jobs currently queueing in your Laser. You see details about the job the laser is working on and can manage the queue.

![produce_screen01](assets/produce_screen01.png)
![produce_screen02](assets/produce_screen02.png)

## Printing in Job Control 
* Under the jobs list there is a USB cable head press this button  
* By doing this it connects the computer to the laser cutter  
* The USB cable button then turns into a play button  
* You should now see the laser head on the screen in corelation to where it is on the laser bed  
* Move the laser into the correct position on the material you are cutting by using the red arrows on the laser cutter  
* Now drag your job to the laser and it should snap into place 

**`Check you have turned on the room extraction, the 2 fans and the air compressor by turning on by the wall socket!`**

* You can now press the play button icon under the jobs list  
* Your job should now be printing  
* Watch your job by the emergency stop not by the computer! 

## Running the job 
Note – ensure the sperate machines are running before starting job (turn on by wall socket)
  1. Both extraction fans 	      
  2. Air compressor          
  3. Heavy workshop extraction

You can start this up first to ensure good extraction as soon as the cutting starts. 
Never leave the job unattended. You must supervise the job at all times to ensure there is no risk of fire. 
* Do not turn away from the machine to speak to someone else 
* Do not check your phone 
* Do not return to the PC to set up another job 
Members who are seen to not observe proper safety precautions with the laser cutter will not be allowed to use it. 

## Finishing the job 
Let the extractor fan run for a minute after the job has run to allow fumes to be extracted. 
If you are cutting a material that lets off gasses after cutting (e.g. acrylic) leave it in the machine, with the lid closed for at least a minute before removing. Cut parts that are still letting off gas should be placed in the Spray Booth or Heavy Workshop by an extractor hood. 
Save your work on the PC log out and put onto sleep.  

## In the event of fire 
If you see smouldering appearing as the laser cuts, press pause on the machine. 
Consult your settings and a member of staff; once you’re happy these are correct, try again. 
If the fire grows, and it is safe to remove the material, you can place it on the floor and use the CO2 extinguisher to put it out. 
If it is not safe to deal with the situation, follow the building fire procedure. 

#### RGB Colour Table on Ink Scape  

|             | Black (Etching) | Red (cutting) |
| ----------- | --------------- | ------------- |
| `Red (R)`   | 0               | 255           |
| `Green (G)` | 0               | 0             |
| `Blue (B)`  | 0               | 0             |

* Once ready, go to File > Print 
* Select ‘Trotec Engraver’  
* Press the JC (Job Control logo) and then press ‘Print’  
* Open the Job Control app 

## Job Control
* Job should now show up in Job Control on the righthand side under ‘jobs’ 
* Double click or drag onto bed  
* If you’d like to see your job press the eye icon on the top row next to the cog icon 
* Choose the material by double clicking on the bed  
* Material database should open with all the materials to choose from on the left and their properties on the right.  
* In the ‘process’ column if it says skip click on the arrow and choose the correct process for the colour  
* If engraving (black) this should always be first in the line-up and in the ‘direction’ of ‘bottom up’  
* Cutting (red) should be second and so on.  
* Once done press OK 

## Member Induction sign-off form 

This is the form please follow this link [Plus X Workshop Induction — Q500](https://forms.gle/SZgYQSqVjJGrDnhv8)

#### *this must be completed before any further use of the machine*
