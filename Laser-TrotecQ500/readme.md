last reviewed 2022-08-23 by KI

# Trotec Q500 laser cutter readme

serial number: Q51-0113

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

## External Docs

* [Brochure (PDF)](brochure-q-series-en.pdf)

* User manual (PDF)(assets/manual) - can;t find?

* Laser cutting services: [Cirrus Laser Ltd](https://cirruslaser.co.uk/) laser cutting service in Burgess Hill (recomment by another member) 

## Contact information for trotec

* Trotec Technical Support Number: 0191 418 8110 

* Andy is the best for material testing information: Campling Andrew <Andrew.Campling@troteclaser.com>
* Andy Mobile: mob +44 (0)797 056 2031
* Support: ts 1st level tluk <service-uk@troteclaser.com>

#### Add this to emails when you contacting Trotec

* Your Name: XXXX
* Your Company Name: Plus X Innovation Hub
* Your Phone Number: XXXX
* Serial Number of your machine: Q51-0113

## Issues

* [Issues tagged with Q500](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=Q500&first_page_size=20)

## Services 

| Date       | Link to  Reports                                                      |
| -----------| --------------------------------------------------------------------- |
| 2021-07-20 | ![Q500 Trocare Certificate](assets/q500_trocare_certificate.pdf)      |


