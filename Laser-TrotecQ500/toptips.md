last reviewed 2022-08-23 by KI

# Q500 TopTips

## Top Tips

* When focusing on thicker pieces of material:
     * doing two passes on a thicker piece of material bring the laser down on the second pass, this will help it focus to the second pass.
     * must be done seperatly from the first pass 

## Best Practices 