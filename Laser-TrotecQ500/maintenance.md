Last reviewed 2022-08-20 by KI

# Trotec Q500 laser cutter maintenance

Laser Movement – X and Z Axis (Left and Right + To and Fro)

Job Control is the software used to control the laser cutter – Serial Number: Q51-0113

### Firmware Updates

* check for Ruby updates via emails 

## Water Chiller

* Turn it on (switch is on the front) 
* There is a water level at the back of the machine next to the fans, check to make sure it’s in the green (normal) region. 
* If it drops below this, empty the old water and then top it up with distilled water make sure to get the water out of the tubes as well.

![cooler_01](assets/cooler_01.jpeg)
![cooler_02](assets/cooler_02.jpeg)
![cooler_03](assets/cooler_03.jpeg)

## Air Assist Pump (small silver sliced machine) 

* Switch this on directly from the wall socket
* Turn this off when you are finished with the machine

![air_pump](assets/air_pump.jpeg)

## Turn machine on 

* With the key, twist and hold for 3 seconds
* When the laser cutter is ready, it will beep a few times and then the lights on the control panel will turn on
* Check it goes to the top left corner

## Green Fans

* The 2 green fans behind the laser cutter turn on with the plug by the fabman 
* check they both turn on with the extraction on and that they don't fall over (they are on black blocks to hold them down)

![fans](assets/fans.jpeg)

## Placing laser in correct spot

* Use the arrows on the pad to move the laser to the top left of the material wanting to cut
* Switch off the machine to bring the laser forward by dragging the metal bar with the laser on it or keep it on and use the arrows by the switch. 

![switches](assets/switches.jpeg)

## Nozzle

* Unscrew the nozzle and check the residue on the end, this should be check the most after wood is used ont he machine. 

![lens_01](assets/lens_01.jpeg)

## Cleaning Mirror on Nozzle

* Unscrew the 2 silver knobs on the top red slanted panel and lift it up slowly. 
* Check the mirror and dab down with cloth and solution if dirty. 

`WARNING – do not wipe first time as this can scratch the mirror – dab it first`

![lens_02](assets/lens_02.jpeg)

## Cleaning Mirror on left side of the machine

* Undo the side panel on the left side of the machine and move it out of the way
* As explained above clean the mirror the same as above 

`WARNING – do not wipe first time as this can scratch the mirror – dab it first`

![lens_03](assets/lens_03.jpeg)
![lens_04](assets/lens_04.jpeg)

## Cleaning Lens

* Unscrew the bottom half of the tube anti clockwise until you can lift the tube. 
* Then unscrew the other half clockwise until it's comes completely off. 
* The lens and mirror should be checked every morning

**` -- WARNING – do not wipe first time as this can scratch the mirror - dab it first -- `**

#### DO NOT FLIP OVER – this could cause the lens to drop out and scratch

![lens_05](assets/lens_05.jpeg)
![lens_06](assets/lens_06.jpeg)

## Cleaning the bed

- using the metal latch on the side of the bed remove the whole bed 
- hoover and brush down to remove as much burnt wood in the cracks as possible (this is what helps course more fires)

**DO NOT HOOVER long pieces of wood this blocks the extration unit**

![cleaning_bed](assets/cleaning_bed.jpeg)

## Cleaning the tray

- with the bed top still removed, remove the tray from under the machine (its heavy to be careful) with all the off cuts that have dropped through
- sweep first to get the large part and then hoover up the smaller dust parts
- if sticky wipe down 

![cleaning_tray](assets/cleaning_tray.jpeg)

- then before putting the tray back in and the bed is still off go back go into the machine and hoover any left over parts in the bottom under where the tray sits
- go around the edge and remove any littel bits and off cuts 
- wipe down if needed 

![cleaning_inside](assets/cleaning_inside.jpeg)
![cleaning_edge](assets/cleaning_edge.jpeg)

