last reviewed 2023-06-01 by EC

# Filament Extruder maintenance

List of common maintenance procedures:
https://support.3devo.com/learn-more#filament-maker-maintenance


### Firmware Updates

https://support.3devo.com/firmware-updates

### General Maintenance

Always purge after finishing an extrusion run: 
https://support.3devo.com/purging-your-filament-maker

Devoclean midtemp will need to be purchased when it runs out as it is needed for purging after every run, apart from raw PLA


### Replacement parts

Contact support@3devo.com
