last reviewed 2025-02-14 by AS


TODO
- Update riosks - do not leave unattended - do not put non-plastic objects in the hopper eg pens/pencils to fool sensor)
 - add links to their documentation
 - add suggested starting settings
 - explain selecting material presets
 - explain no need to preheat - built into system
 - to use tube or not
 - how much to use
 - what happens when you press start

 - how much material might need to be purged
 - how much of your own material will you waste
 - how autotune works


Contamination: 

![](assets/filament-contamination.jpg)





# Filament extruder Induction

## Risks


| Hazard                                                       |  Control                                                     |
|--------------------------------------------------------------|--------------------------------------------------------------|
| **Manual Handling** <br>Machine is heavy and could be dropped when moving | 2 person lift when moving. Ensure route to new destination is clear |
| **Manual Handling** <br>Flying debris while cleaning parts   | PPE: Eye protection to be worn                               |
| **Electricity**<br>Risk of injury due to faulty equipment, contact with live electrical components or improper use. | Machine subject to regular inspections and maintenance. Machine to be inspected before use. |
| **Burns** Extruder, or heated material<br><br>Could be burned by touching hot parts of machine, or handling heated plastic | Care to be taken when guiding filament through machine       |
| **Hazardous substances** Skin contact or inhalation of volatile fumes given off by materials brought in by users, | Users inducted in rules for safe use of extruder– no unauthorised materials permitted, new materials to be assessed by user and workshop manager prior to use. Users cautioned to not burn polymers and consult data sheets for safe melting temperatures before use |
| Fire from unattended extrusion | User must be in the workshop for the duration of the job. |


## Safe Working Practices

### Before use

* Ensure machine is not damaged. Report any suspected damage to workshop staff.
* Ensure work area is clear.

### PPE

* Eye protection when extruding with the door open. 
* Steel toe boots when shifting the machine. (ask permission first)
* gloves when handling hot material. 
* extraction + respiratory protection when working with materials not listed in this induction. (ask permission first)


### *Post-use*
You must purge the machine after use if you're running any material besides raw PLA, HDPE or "Devoclean". 
This will take a while, but it will break the machine if you don't do it. 

Plan for ~1 hour spare for machine purging. **You cannot use the machine unless you can also commit  the purging process. **

### *Preparation* 

Basic usage of the filament extruder requires: 
* Empty spool (we probably have a few)
* Clean, dry plastic pellets of < 4mm average diameter 
* Pliers for manipulating the filament

### *Startup*

This video is a great overview of the basic process of getting started with a new material:

[![](http://img.youtube.com/vi/uxp2iuypDAo/0.jpg)](https://www.youtube.com/watch?v=uxp2iuypDAo "3Devo basics")

Once your materials are prepared, you can heat the machine up. For basic materials such as first-use PLA pellets, the built in temperatures will probably be good enough. 

For other materials, pick a preset as close to your material as possible. you can iterate on this later. 

While heating, you can pour your pellets into the hopper, when getting started don't use the hopper "tube", just fill the built-in hopper. 

Once heated, a very thick plastic string will start exiting the nozzle. when you're ready you can pull the string tight and feed it through the puller wheels. 

The process of feeding the plastic into the puller wheels, and then onto the spool requires trial and error. We recommend you consult the video to get a better idea of how to do this. 

### *Extrusion* 

Tuning your filament diameter is the most important part of the process, there are many variables that feed into getting usable accuracy. 
Adjusting puller wheel speed is probably the best place to start. 

Adjusting temperature can also help, e.g. under-sized filament can be a symptom of a too-low extrusion temperature.

Do not put objects in the hopper (eg pens or pencils to fool the fill sensor) that are not intended to be extruded. These could enter the mixing screw and damage the machine



## Health and Safety 
Some materials require fume extraction, such as ABS, we currently don't have a safe way of doing this so they're not allowed. 

* Always use pliers when manipulating hot filament
* Make sure you know exactly what type of plastic you are putting in the extruder
* Always use clean, dry pellets. even slightly damp plastic can cause spitting and popping. 
* Always check on the machine regularly when in operation. 
* ways purge the machine with devoclean if you are working with a material that cannot be left in the extruder. 

## Materials 
The current list of suitable materials: 
* PLA
* PETG
* HDPE


## Member Induction sign-off form 

Please review and sign this form: [Plus X Workshop Induction — Filament Extruder](https://docs.google.com/forms/d/e/1FAIpQLScI5WdFzwZmU7w7DJ1qsyFkBq1RCPM6Z3ItZId1tWqzIZmVLA/viewform?usp=sf_link)

**This must be completed before any use of the machine**