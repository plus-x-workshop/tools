last reviewed 2023-06-01 by EC

# 3Devo 350 composer filament extruder readme

serial number: TBC

<!-- For groups of tools, or rooms/areas, you may need to adjust this template -->

## Our docs:

<!-- Link to files in this directory -->

* [Induction document](induction.md)

## External docs

<!-- Link to any documents on other websites -->

https://support.3devo.com/

## Contact information

support.3devo.com
support@3devo.com

## Issues

<!-- All machine issues should be tagged with this machine name -->

<!-- Edit this link appropriately -->

* [Issues tagged with Filament Extruder](gitlab_issue-label-Filament-Extruder)

## Services

<!-- All machine issues should be tagged with this machine name -->

<!-- Edit this link appropriately -->

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|


## Maintenance


