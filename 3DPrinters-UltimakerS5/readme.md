last reviewed 2023-06-01 by EC

# Ultimaker S5 readme

serial number: e6584a27-cb41-4303-9eb3-9cace6779d04

## Our docs:

<!-- Link to files in this directory -->

* [Induction document](induction.md)

* [Maintenance guide](maintenance.md)

* Best practice guides, tips etc.

## External docs

* [User manual (PDF)](assets/UltimakerS5_Manual.pdf)

## Contact information

<!-- add companies main number -->

* Company Support Number: 0800 689 1011

<!-- add companies emails for different people-->

<!-- add were to put issues for that machine and what the company needs -->

* website: Creat3d.shop
* email: sales@creat3d.co.uk

## Issues

<!-- All machine issues should be tagged with this machine name -->

<!-- Edit this link appropriately -->

* [Issues tagged with MACHINE NAME](gitlab_issue-label-ALL)

## Services

<!-- All machine issues should be tagged with this machine name -->

<!-- Edit this link appropriately -->

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|