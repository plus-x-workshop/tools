last reviewed  2023-06-08 by AS

# Ultimaker S5 Dual Material FDM 3D printer Induction

**Build volume: 330 x 240 x 300 mm**

<!-- ## Cura Download
Download Cura from [github](https://github.com/Ultimaker/Cura/releases) or [Ultimaker](https://ultimaker.com/software/ultimaker-cura) -->


## Error 56 

PVA is stuck in the tube, ask technician for help

## Purchasing filament 

This machine is designed to use Ultimaker-branded materials, but any compatible 2.85mm filament will work. 
We use https://3dfilaprint.com/. 

**Diameter: 2.85 mm** 

NOTE: any filament must have your name on it otherwise anyone is allowed to use it!

## Filament storage 

- Do not use Plus X filament slots
- Do not remove PVA
- The Material station is not for long term storage, treat it as you would a Prusa spool holder. 
- Someone may use your filament if you leave it in the machine

## Filament loading 

- Core path 1 is on the *left* 
- Core path 2 is on the *right*

- **Place your filament in the slot closest to the Core path you're using**

- **Make sure your print core is correct for your filament**

## Core types

- AA for all materials
- BB for PVA
- CC for abrasive material (eg reinforced, or wood/glow in the dark)


## Identifying filament in the printer interface

If you're loading Ultimaker or other NFC-labelled filament, it shoudl be automatically recognised. For generic filament, follow these steps:

![](assets/ultimaker-screen.jpg)

- Tap on the yellow icon for the bay you have just loaded
- The tap on 'Select type'
- Scroll down to 'Select other material'
- Select 'Ultimaker' (even if its not Ultimaker filament, this lets you specfiy a specific bay if the ame filament type of different colours is loaded)
- Select your filament type (Note, ASA should be loaded as ABS, PVB should be loaded as PLA)
- Select your colour
- Tap 'Confirm'


The interface will now show the filament type with a '?' on that bay. 


![](assets/ultimaker-screen2.jpg)

## Bed adhesion

- Always use dimafix adhesive
- Wait for bed to cool before attempting removal

- **NEVER PRY / BASH THE PART INSIDE THE PRINTER**
- Remove the glass bed before removing the part, unless it is fully detached


## Safety

- **NEVER PLACE ANY OBJECTS UNDER THE BED**

- There is no door interlock, the machine will crush / burn your hand, so never put your hand inside the machine while it is operating. 

- If you want to leave a print overnight, check with staff first. 1/3 of your print must be successfully completed before workshop closing
- Advanced materials like ABS, ASA, Nylon, etc can be printed on this machine, but you must check with staff first. 
- Beware of the hotend (> 200°C) and bed (60-100°C)

## Materials 

- PETG (tough and easy to print, but often leaves strings on parts) **APPLY GLUE STICK - without the glass will be damaged**
- PLA (good looking and easy to print but not good for mechanical parts)
- PVA (support material that can be dissolved rather than cut away)
- ASA/ABS (tough, high temperature resistance, difficult to print) **ASK BEFORE USING**
- Engineering materials (PC, Nylon, etc) **ASK BEFORE USING**

## Setting Up the Machine
- Do not start a job until you have checked the printer is clear of anyone else’s prints
- Check the nozzles are clean
- If the bed looks dirty, remove it and wash under warm water with soap
- Apply dimafix if using anything other than PLA / TPU
- PETG **MUST** have a layer of glue stick applied

## Software workflow (Cura)

**Note** You must use the PC connected to the Ultimaker printer to slice your files.

- Export your CAD file (e.g. Fusion 360,Solidworks, Blender) to an STL file
<!-- - Cura is a free download, can be run on any computer. -->

Check that there is a blue tick mark on the printer icon on the top menu bar (PX-Workshop-Ultimaker-S5". This means that Cura is connectd to the printer and can load material information.)

![Slicer software](assets/cura.png)

## Tabs: PREPARE – PREVIEW – MONITOR

- Prepare: loading your model(s), scaling and orienting them
- Preview: preview of the toolpath the printer will do, useful for checking if a print looks like it won't fail
- Monitor: disabled due to security issue (anyone on the local network could control the printer)

## Preparing your model

- press the folder icon and select your model from the filesystem
<img src="assets/folder_icon.png" alt="cropped screenshot of the top left Cura folder icon" width="50%"/>

- Clicking on your model will allow you to access the prepare tools
- The lay flat command can be used to place a selected face (generally the largest, flattest) facing down on the plate
- The extruder selection buttons allow you to choose which of the two loaded materials you would like to print the model with
<img src="assets/prepare_tools.png" alt="Cropped screenshot of a selected benchy boat model in cura, next to it is the sidebar tools showing buttons: Move (T),Scale (S),Rotate (R), Mirror (M), Per Model Settings, Support Blocker (E), Print selected model with extruder 1, Print selected model with extruder 2" width="50%"/>

- The filament selection dropdown allows you to choose from any filament combination in the material workstation 
<img src="assets/filament_selection.png" alt="Cropped screenshot of the filament selection dropdown in cura, it shows Black ABS in nozzle 1, White PLA in nozzle 2, and in the list below other combinations of filament" width="50%"/>

- The print settings dropdown lets you change print profiles, standard 0.15mm is always a good place to start if you are unsure
<img src="assets/print_settings.png" alt="Cropped screenshot of the print settings dropdown in cura" width="50%"/>

You may need to go into the detail settings below and change individual settings for your filament. Please speak to a member of staff if you would like to load a spcialist material profile

## Printing your model

- **Note** Make sure you have applied glue if needed, before pressing print.

- Once your model is prepared, you can press the blue slice button
- pressing preview lets you see the layers the machine will print

- On the workshop computer: Press "Print over network" to send to the machine wirelessly
- On your own machine: please talk to the technician about this

## While the printer is running

Observe the print for at least the first few layers to ensure the print adheres to the bed. most failures occur on the fist layer.

Be aware of the hazards of the printer:

- Trapping fingers – do not place your fingers near moving part while the machine is running, as they could become trapped.
- Keep clothing, hair and other objects clear of the printer.
- Hot-end and bed: the hot end and nozzle are heated to approx 200°C. The bed may be heated to 70°C or more. Be careful not to touch the hot end when cleaning it or operating the printer. Be aware that the bed could damage objects placed on it.

## Removing Print

- Please do not leave your finished print in the machine for more than 12 hours, others may want to use it. 
- if using dimafix, let the bed cool down and the part will detach easily - if it fails to release, wait 10 minutes and then wash the plate under warm water

## Cleaning

- Leave the machine as you found it
- Wash the glass bed with warm soapy water
- PLA and PETG can be recycled here so please put them in the correct bins! 


## Member Induction sign-off form 

This is the form please follow this link [Plus X Workshop Induction — Ultimaker](https://forms.gle/9c5Xxivtwkyyq4V37)

**this must be completed before any further use of the machine**
