Last reviewed 2022-08-23 by KI

# General Induction readme

## Our docs:

* [Induction document](induction.md)

## External docs

## Issues

## Services 

| Date       | Link to Service Report                                                     |
| -----------| -------------------------------------------------------------------------- |
| 2020-10-16 | ![Fabman Invoice for yrly Subscription](assets/fabman_invoice_2020.pdf)    |
| 2021-10-27 | ![Fabman Invoice for yrly Subscription](assets/fabman_invoice_2021.pdf)    |


