Last reviewed 2022-12-13 by AS

## Tour notes

### General

TOOLS
- No tools leave the workshop without asking workshop staff


FIRE
-	Meeting point: B&Q car park
- Fire alarms
-	Fire doors (N & S)
- Fire bucket in fablab

PANIC BUTTON
-	Why we have it? – your safety 24 hours (after 1 month)
-	How to use it? – two fingers press the two red buttons at the bottom up
-	BN1 Security will come

ELECTRICAL ISOLATORS IN HEAVY WORKSHOP
- Ceiling sockets (to left of Heavy workshop door, labelled "Reel Sockets Isolator")
- Room Extraction (mounted on front of grey Donaldson extraction control unit)

FIRST AID
-	Accident reporting – report any accident or near miss, you must tell us 
-	24/7 danger – tiredness, tell someone they are here 
-	Who is first aid trained? – Andrew 

ACCESS
- Kisi, first month Mon-Fri
- After one month, ask for upgraded access
- 24/7 access is subject to change
- We do sometimes close the workshop for events, maintenance or training. Please check if access is critical.

LONE WORKING

- Lone working rules are subject to change
- For the first month, members can access the workshop Mon-Fri 9-5
- Workshop is partially staffed during these times. 
- Subject to approval by Workshop Manager, after 1 month, members can access after hours, alone
- We do not recommend being in the workshop alone after hours. Some tools are prohibited. See below.
- If you are using the workshops alone please ensure that other people are aware you are in the workshop, and when you are expecting to be finished. 

SHARED SPACE
- Tidy up your mess, take bulky waste home
- Clear space of materials at the end of the day
- Allow others to use the space

CHARGES
 - We may charge for acces to some machines and materials: eg resin printers, photo printer, kiln, spraybooth and others TBC

CONSUMABLES

- We provide basic materials and tooling for inductions and general use
- We advise you provide your own tooling or consumables (e.g. glue, screws, tape, sandpaper) if working on a production job

DAMAGED OR MISSING TOOLS

- **Please tell us** if you break anything or see something that is not right
- We cannot gurantee that equipment is left in good order by other members
- We rely on each other to keep safe


HAZARDOUS SUBSTANCES

- You must not bring hazardous substances into the workshop
- You are legally responsible for complying with COSHH (substances hazardous to health) and DSEAR (explosive, flammable, reactive substances) regulations – see note below
- We may make exceptions by prior agreement. Any substances brought in must be approved by the workshop manager, and you will have to provide a risk assessment and safety data sheet in advance. 


USE OF YOUR OWN EQUIPMENT

- As a rule, you must not use your own electrical equipment in the workshop
- You are legally responsible for complying with PUWER (work equipment) regulations (see note below)
- We may make exceptions by prior agreement. Any equipment brought in must be approved by the workshop manager, and you will have to provide a risk assessment, and safety certification in advance. 


### Rooms

WET AREA/ BIO LAB
-	Not a kitchen - no food prep or food/drink in fridge 
-	What can be used after general induction – fridge, dehydrator, oven, pans, proxxon rotary tool (if used before)
-	What needs training on? – testing machine, vacuum chamer, resin cleaners, ultrasonic cleaner
-	COSHH cabinet – For staff use. Access is only when we are in, largely for IPA for resin printers
- No use of IPA or other flammable substances near hot work
-	Sink – no food or drink - there is a sediment trap, but you must not put solids down sink.

ASSEMBLY SPACE
-	hot desking after induction 
-	first come, first served 
-   only place you can bring food or drink

MATERIAL LIBRARY 
<!-- -	connected to university of Brighton  -->
-	examples of sustainable materials that can be used in the machines 
-	examples of what has been made in the workshop 

HEAVY WORKSHOP
-	What can be used after general induction – wood bench, hand tools, festools with no blade (sanders, drill)
-	What needs training? – woodworking machines, 3 axis CNC, mill, lathe, flatbed CNC, injection moulder, large laser cutter, vacuum former
-	Bins – smaller than hand, if it's waste it needs to be cut down to fit in a bin bag 
-	PPE – steel toes, ear defenders, goggles  

HEAVY WORKSHOP ROOM EXTRACTION
- how to use
- use whenever doing any dust-generating job, even when using Festool extractors
- **do not use** when sanding metals - use Festool extractor marked for metal use


 
WOOD STORE
-	For PX staff
-   Temporary storage for members. No rubbish, no pallets. You can mark it but it may be used at staff discretion.
-	Ask staff before you take anything 
-	NOT for laser materials

FABLAB
-	What can be used? – ‘craft corner’. Glue gun, mats, sewing machine (unless they want an induction)
-	What needs training on after? – vinyl, line bender, large format photo printer, small 3 axis cnc (SRM20), smaller laser cutter (Speedy 300), soldering station, FDM 3d printers (Prusa and Ultimaker), resin 3d printers 
-	Laser cut scrap material – under the table
-	Explain 3D printer bins
-	Computers – for setting up jobs to print etc
-	Fabman – scan QR code and demo using it 
    -	It’s a website not an app
    -	Each one has a different timer depending on the machine 
    -	Remember to log out when finished 
  
SPRAYBOOTH 
-	Clean air comes in and then dirty air is removed out and then filtered 
- No 2-part sprays (isocyanates)

AFTERWARDS
-   Slack - workshop channel
-	Kisi – add to workshop 
-	Fabman – add to fabman
    -	First and last name 
    -	Email 
    -	Chosen package  



## PERSONAL PROTECTIVE EQUIPMENT (PPE)
You are responsible for providing and wearing appropriate PPE at all times. As a convenience, the following are provided for you by Plus X. Please contact Plus X workshop staff if you cannot find the required PPE.

* Disposable nitrile gloves
* Safety goggles and glasses
* Ear defenders
* Splash-proof goggles
* Steel toed capped boots (small supply in limited sizes)

All other PPE for example, must be provided by yourselves, or your employer:

* General workers gloves
* Full face mask
* Dust masks
* Vapour respirators
* Steel toed capped boots
* Heavy Duty Chemical Resistant Gloves
* Chemical Resistant PVC aprons
* Lab coats

PPE is a legal requirement not a choice. Plus X will provide the basics where possible, however it is your and your employer's responsibility to ensure you have the correct PPE for the task in hand. If you are unsure speak to the Workshop Manager. The heavy workshop has been designated a hearing protection area. If the extraction is running all users must wear ear defenders.

## Lone Working

The Plus X Innovation Hub is staffed Monday-Friday 9am - 5pm. The workshop is partially staffed during these times. However, we cannot guarantee staff will be present at all times. 

After a 1-month probation you can apply to access the workshops out of office hours (9am - 5pm). This is discretionary and subject to approval by the Workshop Manager. You must have completed your induction and machine specific training as well as demonstrated the necessary levels of competence.

**Lone working out of core hours is not advised in the workshop**. If you are in the workshop after hours, we recommend you to be with other members or staff. **In addition, due to the risk of serious injury, if you are lone working in the workshop, use of all machines/tools listed below is
prohibited.** If you are in the workshop with other users, these machines can be used if you have completed all relevant training and inductions with Plus X Workshop staff.

- Mitre saw
- Rail/plunge saw
- Pillar drill
- Use of Hazardous Chemicals – any chemical with the Hazard symbol listed below.
  - Corrosive
  - Acute Toxicity
  - Explosive

If you wish to do any of these things, please ensure someone is there with you. 


## Complying with Regulations

You and your employer are legally responsible for complying with all health and safety regulations. You must familiarise yourself with these. 
These include:
- [COSHH](https://www.hse.gov.uk/coshh/index.htm)
- [DSEAR](https://www.hse.gov.uk/fireandexplosion/dsear-background.htm) 
- [PUWER](https://www.hse.gov.uk/work-equipment-machinery/puwer.htm)
- [Control of Vibration at Work Regulations](https://www.hse.gov.uk/vibration/hav/index.htm)



## COSHH Policy

You must comply with our [COSHH Policy, detailed here.](assets/COSHH_Policy_for_Members_Brighton_Workshop.pdf)

In particular, note:

- Absolutely no hazardous substances to be brought in without prior written permission from the workshop manager
- If we agree that you may bring in specific substances, you will carry out a COSHH assessment and provide MSDSs and COSHH Assessments in printed and electronic form in advance.
- You will take any hazardous substances (including waste) oiff the premises and dispose of in accordance with regulations. We do not provide hazardous chemical displosal facilities.

## Use of work equipment 

Incuding tools we provide, or your own tools brought in by arrangement

- Absolutely no equipment to be brought in without prior written permission from the workshop manager
- Don't circumvent safety measures: propping open doors, circumventing interlocks

## Hand-arm vibration

Hand-arm vibration comes from the use of hand-held power tools and is the cause of significant ill health (painful and disabling disorders of the blood vessels, nerves and joints).

Wherever there is exposure to hand-arm vibration, above the EAV, you should be looking for alternative processes, equipment and/or working methods which would eliminate or reduce exposure or mean people are exposed for shorter times.

The table below gives EAV times for tools in our workshop that cause vibration.

| Tool or process name        | Vibration magnitude (m/s² r.m.s.) | Time to reach EAV (2.5 m/s² A) | Time to reach ELV (5 m/s² A) |
|-----------------------------|-----------------------------------|--------------------------------|--------------------------------|
| Belt Sander                 | 5.6                               | 1h 36                            | 6h 23                            |
| Delta Sander                | 3.5                               | 4h 5                             | 16h 20                           |
| Jigsaw and Router           | 8                                 | 0h 47                            | 3h 8                             |
| Shaper Origin               | 2.5                               | 8h                              | >24h                           |
| Orbital Eccentric Sander    | 4.8                               | 2h 10                            | 8h 41                            |
| Oscillator                  | 7                                 | 1h 1                             | 4h 5                             |

## Lithium Batteries

You must comply with our [lithium battery policy, which is detailed here](assets/Lithium_Battery_Policy.pdf). In particular, note: 

- No Lithium batteries are to be manufactured, repaired, or altered.
- No lithium storage weighing over 10kg.
- No unattendad charging

Any use of lithium batteries in the workshop must be agreed with the workshop manager in advance.

## First Aid
First aid boxes are provided in each workshop and the main assembly space. The emergency plan is displayed by the entrance into the workshop area and it details first aiders, fire procedure and out of hours emergency contact. You must familiarise yourself with this document as part of this induction.


## Security

The workshops and machinery should never be accessed by untrained persons, persons under influence of alcohol or drugs or below 18 years old without supervision. You must never facilitate access for anyone else into the workshops.

## Cleaning
- Please clear up after yourself and leave the workspace as you would like to find it. Failure to do so may involve your access to the workshops being refused. We have a **Clear Desk Policy**. At 16:30 everyday all workbenches, desks and computer desktops will be cleared. No items can be left in the workshop without express permission from the workshop team. Bins are clearly labelled for general waste and recycling . 
- We sort sharps, electronics/WEEE, PLA, PET, and wood into separate bins. 
- There is no food or drink allowed in the workshops with the exception of drinks on the central assembly table. 
- The sink is fitted with a trap for collecting grease, oil and particle. Do not put organic waste (e.g. milk) down the sink.

## Maintenance
**Do not** try and fix anything but notify a staff member if anything needs maintaining, repairing or replacing.

## Air Quality
Workshop 3 is fitted with dust and particle extraction and the Workshop 1 Spraybooth is fitted with solvent extraction. Do not use any other space than these for tasks that give off dust and fumes. Do not use solvents in the Workshop 3 or create dust in the Workshop 1. When working in these spaces you must wear appropriate PPE.

## Security
- Access to the workshop is only for inducted members and you will need to use your KISI app to enter. 
- Do not allow others in. Non-members can view the workshop on bookable tours. 
- Never open the rear fire escape or prop it open. For large deliveries we can arrange for it to be opened. 
- Your belongings are not covered by Plus X insurance - we recommend taking out a personal insurance policy and locking valuables in the lockers provided. 
- Do not remove tools from the workshops. The workshops are covered by  CCTV.

To the extent permissible under applicable laws, we are not responsible for any theft, damage, destruction or loss of your property or belongings while using our facilities.

## Assistants, Clients and Guests
- Visitor policy is the same as for the rest of the building, please notify the workshop team if you are bringing in a guest or client to view your work. 
- To the extent permissible under applicable laws, you are responsible for their safety during a visit and must ensure they wear appropriate clothing and PPE. 
- Day passes are available to purchase from Members Portal if your guest is to stay for longer than an hour. 
- Guests are not allowed to use any workshop facilities.

## Consumables, Materials & Breakages

Materials such as 3D printer, consumables such as screws and breakages such as router bits can be purchased from our stock and will be charged to your account at cost. Please do not bring in your own materials without discussing first with the workshop team. Materials without data sheets cannot be used.

## Storage
Please note there is no storage for members materials in the workshop. If you require storage cupboard please speak with the front desk team about lockers available at Plus X.

## Deliveries
You are responsible for being present to receive any deliveries of any item larger than standard post that the front desk can process. We cannot take  large deliveries on your behalf.

## Computers
We use computers and CAD software to run the digital fabrication machines. We have a set of supported workflows for each process, and we will assist you in following these workflows. For training or additional help, see Fabrication, Training & Advice, below. While the machines do have modelling software installed please only use them for tweaks as computer are intended primarily to run the machines. No software is to be installed by members on any computer.

## Fabrication, Training & Advice

The workshop team can give product development advice at £25/hour exc. VAT (charged in 1 hour slots). Basic workshop advice of up to 10 minutes is not charged. If you need help preparing files or with design you can book an advice session with a staff member. If you would like to learn more about a machine, software or process in more detail than the basic training, you can book additional training for £25/hour exc. VAT.

## Privacy and Photography
Please see our Privacy Notice at https://plusx.space/privacy-policy/ for more information about how we process your personal data.

Please see our Privacy Notice at https://plusx.space/privacy-policy/ for more information about photography in the workshops.

## Intellectual Property
This workshop is an open access and shared space. Therefore, it is not appropriate if you need absolute secrecy. It is your prerogative to protect your own IP if needed. Please do not ask staff or members to sign non-disclosure agreements.

## Production, packaging and fulfilment
Please discuss your needs before beginning any batch production. As a shared space we need to balance our users' needs and may need to schedule production at quieter times.

Please discuss with the workshop team any need for use of the Assembly space for large orders that require a lot of packaging or assembly; we may need to book out space or find a room in other parts of the building to help you with fulfilling your project order. This is subject to availability and at Plus X’s team discretion.

## Communication
Please send workshop enquiries to workshop@plusx.space. You can telephone us via the Plus X front desk: 01273 056128 during the office hours.

## Booking
Training and Induction bookings can be made via the Members Portal. Once trained and inducted you can book time on machines and workspaces using Fabman: https://fabman.io/members/1344/equipment



## Events
Plus X Innovation reserves the right to close the workshop for events, maintenance or training without notice. 

## Payments 
Although use of the workshops is included as part of your membership, access to the workshops is conditional upon your agreement to these terms. All additional fees will be processed through Office R&D and you shall pay each invoice submitted to you within 28 days of the date of the invoice to the bank account set out in the invoice]. Your usage of the space will be tracked for maintenance purposes using the booking system and KISI app, as set out in the Privacy Notice at https://plusx.space/privacy-policy/.

*Plus X reserves the right, in its absolute discretion, to refuse/suspend any user's access to the space.*

# Plus X Innovation General Workshop Induction Agreement

*I UNDERSTAND THAT ACCESS TO THE WORKSHOP IS CONDITIONAL UPON MY COMPLETION OF AN INDUCTION AND AGREEMENT WITH THE FOLLOWING TERMS. I CONFIRM THAT I HAVE COMPLETED AN INDUCTION AND I FURTHER AGREE THAT I AM RESPONSIBLE FOR, UNDERSTAND AND AGREE TO THE FOLLOWING TERMS:*

## Health & Safety  
- You are responsible for your own safety, and for knowing how to work without hurting people or damaging machines. 
- After this induction you are permitted to non-exclusively use all rooms, workbenches, manual and power tools. 
- To use machines, you must complete specific training in addition to this induction. 
- Be vigilant at all times of those around you and the machines/tools you’re working with. 
- Never attempt to use a machine you have not been trained on even if you know how to use it.
- It is your responsibility to wear appropriate PPE. 

I understand that using the machinery in Plus X Innovation workshops carries an inherent risk of bodily injury, including, in extreme cases, loss of life. Plus X Innovation has outlined the Health, Safety & Fire measures to me and I understand that, to the extent permissible under applicable laws, use of the workshops is entirely at my risk. 

## Indemnity
I agree, without prejudice to any other right or remedy Plus X Innovation and Plus X Innovation staff may have, to indemnify and keep indemnified, defend, hold harmless and, to the extent permissible under applicable laws, release from all liability Plus X Innovation and Plus X Innovation staff, for ANY LOSS OR DAMAGE RESULTING FROM PHYSICAL OR MENTAL INJURY, DEATH OR PROPERTY DAMAGE arising from my use of Plus X Innovation workshops.

## Severance
If any provision or part-provision of this agreement is or becomes invalid, illegal or unenforceable, it shall be deemed deleted, but that shall not affect the validity and enforceability of the rest of this agreement.

## Governing Law and Jurisdiction
This agreement shall be governed by and construed in accordance with English law.
Each of the parties irrevocably submits for all purposes in connection with this agreement to the exclusive jurisdiction of the courts of England.

*This agreement has been entered into on the latest date it is executed by both parties.*

Please go to [Plus X Workshop Induction — General](https://forms.gle/NjKk4MknTeEadgMH6) to fill out the form. **You must complete the agreement before accessing the workshop**
