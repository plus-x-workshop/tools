last reviewed 2022-08-23 by KI

# Axminster/SIEG SC4 Manual Lathe readme

serial number: 21-2051-A-067

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

## External docs

* [User Manual (PDF)](assets/505189_manual.pdf)

* [Technical Data Sheet](505111_TechnicalDataSheet_1.pdf)

* [Health and Safety note on use of emery cloth](assets/eng_accidents_at_metalworking_lathes_using_emery_cloth.pdf)

## Contact information

* Email: b2beast@axminstertools.com
* Number: (Alex) 07976 968733

* [Website](https://www.axminstertools.com/customer-services)

 ## Issues

* [Issues tagged with lathe](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=lathe&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|