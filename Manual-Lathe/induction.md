Last reviewed 2023-07-27 by AS

# Axminster/SIEG SC4 Manual Lathe Induction

## Documentation

- [Manufacturer website](https://www.axminstertools.com/axminster-engineer-series-sc4-510-bench-lathe-505189)
- [User Manual (PDF)](assets/505189_manual.pdf)

Please also see the book "Lathework for Beginners" in our library, which is an excellent introduction.


## Materials 
-  OK: metals (steel, aluminium, brass, copper, etc), plastics
-  Not suitable: wood

## Safety

- If unsure at any point, ask a member of staff for help
<!-- - The lathe must not be used outside core working hours, when other people are present in the workshop area, and can assist in case of entanglement. -->
- Lathe Fundamentals 101 Lathe Safety: https://www.youtube.com/watch?v=0X1nZwOZH6M


### Key hazards

- Moving tools or hands into the chuck
  - Be aware of the chuck, especially the position of the jaws. Be especially careful of the jaws protruding from the side of the chuck. These are rotating hammers.
  - Do not attempt to circumvent the chuck guard
  - Do not leaving the power feed running unattended (pushing the tool into the chuck)
  - Do not turn on the spindle without work clamped in the chuck. Loose chuck jaws can fly out from the chuck and hit you.
  - Always double check to make sure your work is securely clamped in the chuck or between centers before starting the lathe. Start the lathe at low speed and increase the speed gradually.
- Poorly supported workpiece (long stock whipping around)
  - When working with long stock (more than 5 cm from the chuck), use a steady and/or a centre in the tail and/or outboard turning 
  - No more than 5 cm of material should be protruding outside the machine when outboard turning.
- Using hand files or emery paper
  - Never use emery cloth, use emery paper on a board, or a file
  - Always use a file in the correct handle, never with a bare tang
  - When using any hand tools at the lathe, use extreme caution and a very slow spindle speed
- Body parts, tools, loose clothing or hair being caught in the workpiece, chuck, leadscrew or other moving parts
  - Don’t wear jewellery, loose clothing or long hair if it’s not tied back - remove watches, rings and jewellery before starting work
  - Wear a short sleeved top
  - Know where the emergency controls are. Practice using them.
  - Do not rest your hands on the machine
  - Beware of trapping fingers around the apron when using the powered feed
  - Turn the motor off before attempting to make adjustments to a part
  - Ensure your working area and the lathe bed is clear. Do not store tools (e.g. chuck keys) on top of the lathe. Use one of the mobile benches.
  - Do not power on the lathe with the chuck key in the chuck, or other loose parts are near the lathe
- Handling sharp chips or swarf
  - Do not attempt to remove swarf from a part while the lathe is turning
- Flying debris, loose parts, or broken tooling
  - Wear eye protection - even if wearing glasses (use over-goggles)
  - Do not attempt too aggressive cutting



## The lathe

![lathe diagram](assets/lathe-parts.png)


1. Change gear Cover
2. Emergency stop switch
3. Control handle
4. Touch panel
5. Spindle speed display
6. Chuck guard with power off
7. Tool rest
8. Bed way
9. Splash guard
10. Tailstock
11. Quick locking handle
12. Spindle box cover
13. Apron handle
14. Apron
15. Power feed handle
16. X or Y axis power feed change handle
17. Cover for leadscrew
18. Leadscrew


Not shown on this diagram is the second, lower emergency power cut-off, in front of the machine, which can be operated with your knee.


## Accessories and tools

### Cutting

- HSS tool set
- Carbide toolset
- Parting tools and block
- Centre drills
- HSS drills
- Tap and die set (and die holder for tailstock)
- Hand reamers
- Boring bars
- Keyed MT2 chuck for tailstock for holding reamers and drills
- Tapping and cutting fluid

### Measurement

- Dial test indicator/gauge and magnetic mounting blocks

### Finishing

- Knurling attachment
- Lathe file and handle
- Deburrer
- (Please make your own emery boards)

### Workholding

- Fixed steady for longer workpieces
- MT2 centre for tailstock 
- Live centre for tailstock

### Further options

- Alternate gears: for automatic threading (ask staff to use)
- 4-jaw chuck: https://www.axminstertools.com/axminster-sieg-sc4-4-jaw-independent-chuck-600871 (100mm diameter). Allows eccentric turning and the holding of square bar.  (ask staff to use)
- Internal and external jaws - Outside jaws have concave gripping surfaces arranged to hold larger diameter work than is possible with the inside jaws. (ask staff to use)
- Quick Change Tool Post Set: https://www.axminstertools.com/axminster-engineer-series-quick-change-tool-post-set-for-sc4-106823



## Induction tasks

- PPE and clothing check
- Inspection of the lathe
- Starting up and emergency stop
- Chuck up a small piece of material and carry out some basic operations:
  - Turning
  - Facing
  - Drilling/boring
  - Parting


## Setup

### Starting the lathe

- Sign in with Fabman (when operational).
- Verify the lower emergency power stop is not engaged - it should be pulled out.
- Verify the upper emergency power stop is not engaged - twist clockwise to release.
- Verify the power feed is off. To disengage the power feed, ensure the main power feed handle (15) is in the off position (upwards), and the x-y power feed handle (16) is in the central (0) position.
- Verify the lathe is turning in the right direction. For most jobs, this should be over the top and towards you (anti-clockwise when looking towards the chuck from the other end of the lathe)
- Be sure the cross-slide is well away from the chuck.
- Press the power switch to "I" position, at the same time the green lamp will illuminate. The top display will show "0000" (the spindle speed rpm). 
- First press the "start" button
- Press the up arrow to increase spindle speed, and the down arrow to decrease. 
- To change the spindle rotation direction press the Forward or Reverse button respectively. 
- To stop the machine, press the "stop" button or the Emergency stop switch.
- To the right of the emergency stop button is a handle which disengages the drive. If the chuck isn't turning, verify that this handle is in the left position. 
- Note: The chuck guard must be in the lower position as there is a interlock switch preventing the machine operating with the guard in the raised position.

## Set up the tool

Ensure you have the correct tool for the job. e.g. a right hand knife tool to face the end of a workpiece, or when turning towards the chuck.

Orient the tool so the chip groove and relief faces are in the correct orientation. See the diagram below for a common use case, or see pages 18-20 of the "Lathework for Beginners" book

![](assets/lathe-tool-orientation_Large.jpeg)

Use aluminium shims to adjust the height of the tool in the regular toolpost - or use the adjustable quick change toolpost.

### Changing the jaws

When removing the chuck nuts, be sure to keep your other hand in place under the chuck so that the chuck does not drop onto the ways.

Note that the jaws must be reinstalled in a specific sequence. If in doubt, ask a member of staff to help.

### Using the power feed

- To move the tool automatically along the bed, engage the coarse power feed by turning the main power feed handle (15) down to the right.
- To use a finer feed, turn main power feed handle (15) to the off position (upwards), and turn the x-y power feed handle (16) down.
- To engage the power feed across the part (moving the cross slide, turn main power feed handle (15) to the off position (upwards), and turn the x-y power feed handle (16) down.)
- To disengage the power feed completely, ensure the main power feed handle (15) is in the off position (upwards), and the x-y power feed handle (16) is in the central (0) position.

Note: When using the power feed, you must remain alert at all times, especially to ensure the tool or cross slide does not hit the chuck.



### Some Tips on Basic Operations

#### Turning 
- See lathework book p.20

#### Facing
  
- See: http://www.mini-lathe.com/Mini_lathe/Operation/Facing/facing.htm
- See lathework book p.20

#### Making holes: centre drilling and boring

- See: http://www.mini-lathe.com/Mini_lathe/Operation/Drilling/drilling.htm
- See Lathework book p.27


#### Parting

Be aware that it is very easy to break a parting knife or cause damage to yourself or the workpiece when parting. 

You can make multiple initial cuts at different positions along the length of the workpiece to widen the gap, and reduce friction on the cutting tool.

See: http://www.mini-lathe.com/Mini_lathe/Operation/Parting/parting.htm: 
> For a parting cut the top of the tool should be exactly on the center line of the lathe, or no more than .005 above the center line. If the tool is a little high it will have a tendency to 'climb' the work; a little low will cause a tendency to dig in. The tip of the tool should be exactly perpendicular to the workpiece.

> Parting is always done close to the chuck jaws - no more than 1/2" out, and, preferably, no more than 1/4" out. (Note: this is also relative to the diameter of the workpiece; 1/4" may be right for a 3/4" diameter workpiece, but would be too far out for a 1/8" dia. piece.) Parting cuts impose great tangential force on the workpiece that could cause the workpiece to be forced out of the chuck if you cut too far from the chuck jaws.

- Ensure that you do not have the power feed on!
- If you hear the lathe start to chatter, back off the cut.
- Cutting fluid can help.
- With larger diameter (or longer) pieces, beware of the loose end wobbling as it turns. This could fly off unpredictably.
- In many cases if it easier and safer to part the piece with a hacksaw



#### Filing work

All hand operations are carried out at your own risk.

- Hold the file over the part, with the handle in your left hand and the end of the file in your right
- Wear short sleeves and avoid reaching over the spinning chuck
- Point the file away from the chuck
- Ensure the file does not get caught in the chuck
- Keep the file moving - do not hold the file static against the part
- Never use a file with a bare tang - the tang could be forced back into your wrist or palm. Use the file handle provided.
- Do not use loose emery paper. Fix emery paper to a board.

### After use

- Turn off the motor and be aware that the workpiece may be hot before touching it
- Remove all swarf and waste material from the machine and put in the appropriate bins
- If you have used cutting fluid, ensure the machine and work area is clean from oil  
- If you have spotted any issues or breakages – even if not caused by you – please report these to a member of staff
- Make sure the area is clean before leaving the machine. 

## Member Induction sign-off form 

Please review and sign off this form: [Plus X Workshop Induction — Lathe](https://forms.gle/YCduBZ4WjjV1jzRm6)

#### *this must be completed before any further use of the machine*
