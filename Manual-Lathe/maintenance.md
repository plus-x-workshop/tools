last reviewed YYYY-MM-DD by INITIALS

# Axminster/SIEG SC4 Manual Lathe maintenance

[User Manual PDF](assets/)

### Firmware Updates

n/a

### General Maintenance

pages from manual
videos from online 
top tips 
day to day things

### Replacement parts

## Maintenance

TODO

Daily and Periodic Maintenance Daily Pre-use
1. Using an oil can with a narrow nozzle, oil all the oil points on the machine, incl.
A) Saddle (4), B) tailstock (2), C) traverse slide (1), D) compound slide (2), E) leadscrew gearbox (2), and F) leadscrew end bearing (1).
2. Move the traverse and compound slides to give access to their drive shaft threads and lightly coat with oil, work the oil up the threads to lubricate the thread followers.
3. Spray-oil the slides and the lathe bed, exercise the saddle and the slides to spread the oil to all surfaces, both hidden and visible.
4. Spray up under the rack cover to lubricate the rack. (G)
5. Apply oil to the change gears and their axle mountings. (H) Daily after-use
1. Clean all swarf and chips away from the machine bed, slide surfaces, and the tool post. 2. Exercise the slides and ensure no swarf etc., is lodged in the drive shaft tunnels.
3. If you have been using ‘suds’ make sure the machine is throughly dried off. Clear the suds tray of all swarf and chips, especially around the drain.
4. Check the tool, ensure it is usable the next time, if not re-sharpen or replace the tool tip. 5. Lightly oil spray all the machine beds and surfaces, and the tailstock barrel.
6. Clean and lightly oil any tools you may have been using (centres, drill chucks, spanners chuck keys etc, and put them away.
7. Switch off the power supply. Disconnect the plug. 8. Cover the machine over with a dust cloth. Weekly
a) Check the belt tension.
b) Check the tautness of the slides.
c) Check the level of the suds reservoir. (if you are using suds).


Tips here:
http://www.mini-lathe.com/Mini_lathe/Start/start.htm

