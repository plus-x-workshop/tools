# Rotary Axis Unit ZCL-540

[User Manual](https://gitlab.com/plus-x-workshop/tools/-/blob/master/Mill-RolandMDX540/ZCL-540_USE_EN_R3.pdf)

### Size:

**When a standard table is installed:**
**Maximum 371 mm (14.6 in.)**

**When a T-slot table is installed:**
**Maximum 297 mm (11.7 in.)**

## Part breakdown of 4th axis

![mdx02](assets/mdx02.png)

## Overview of Installation Procedure 

Specific steps required to install this unit.

VPanel automatically detects this unit when it is installed. You do not need to make any specific settings.

![mdx03](assets/mdx03.png)

**Important Notes** 

Before installing, clean away any cutting waste and any soiling on the installation surface. 
If waste is left on the surface this can cause inaccuracy in the part sitting flush on the surface.

**We do not have an ATC Unit.**

## For a Standard Table

Prepare the modeling machine:
1. Remove any tools
2. Use manual feed to move the table toward the front
3. Shutdown the modeling machine and disconnect the power cable

**WARNING**: 
Be sure to disconnect the power cable. Failure to do so may result in danger of injury.

![mdx04](assets/mdx04.png)

Install the base plate 

![mdx05](assets/mdx05.png)

Gently turn the workpiece chuck by hand until the installation hole can be seen
Slide the T-slot nut into the groove
Attach the drive unit 

![mdx06](assets/mdx06.png)

Slide the T-slot nut into the groove.
Attach the tailstock 

![mdx07](assets/mdx07.png)

Install the Z-origin sensor.

![mdx08](assets/mdx08.png)

## Connecting the Cables

After making the connections to the connectors check to make sure the cables don’t become caught when the modeling machine is operated.

![mdx09](assets/mdx09.png)

This completed the installation 

**NOTES**:
Before you start cutting
When you've finished installation, first make the setting for the Y-axis origin. Note, however, that doing this required basic knowledge of how to operate the unit. 
Ask a member of staff for assistance. 

## Make sure you've turned it off the on again to allow the A axis button to come onto the hand panel.

# Hand-wheel Feed

You can rotate the A axis using the hand wheel. This operation is basically the same as for the X, Y, and Z axes.

**WARNING**:
This procedure makes the machine operate. Before you perform this procedure, check to make sure that operation of the machine will not create any hazard or danger.

![mdx62](assets/mdx62.png)

## Displaying the A-axis Location

The screen displays the location of the A axis in the same way as for the X, Y, and Z axes. Degrees are used as the unit of measurement, and this cannot be changed. Selecting the coordinate system is also the same as for the X, Y, and Z axes.

![mdx63](assets/mdx63.png)

## Setting the A-axis Origin

This makes the present location the A-axis origin. This operation is basically the same as for the X, Y, and Z axes. To select the axis for setting the origin, press the [A] key.

### Important Note on theY- and Z-axis Origins

The origin settings for the Y and Z axes made with the handy panel are not for aligning these with the A-axis center. Make the settings for the Y- and Z-axis origins using VPanel.

![mdx64](assets/mdx64.png)

## Manual Feed of the A axis

In the same way as for the X, Y, and Z axes, perform this using VPanel's [Move Tool] or [Base Point].

**WARNING**:
Before you move the A-axis, check to make sure that operation of the machine will not create any hazard or danger.

## A-axis Operating Range
This operating range for this unit is ±21,474,836.47 degrees (approximately ±59,000 turns). Infinite turning is not possible.

## Operation Using the Handy Panel
You can also perform this operation using the handy panel.

![mdx_handle](assets/mdx_handle.jpeg)

## About the A-axis Coordinate

![a-axis](assets/a-axis.png)

The location of the A axis is indicated as an angle of rotation. In this way it differs from how the X, Y, and Z axes are indicated.
Despite this difference between angles and dis- tances, in other respects it is treated the same as X- , Y-, and Z-axis coordinates. As with any of the other axes, you can freely set location of the origin (de- spite it being an "angle" and not strictly a "point") and select the coordinate system.
The unit of measurement used to indicate A-axis coordinates is the degree, and this cannot be changed.

![vpanel45](assets/vpanel45.png)

In actual cutting operations, consideration must also be given to the location of the A-axis center. When you're using the rotary axis unit, instead of simply setting the origin to match the location of the workpiece, the origin location must be determined with consideration also given to the location of the center around which the workpiece rotates. It is most common to align the Y- and Z-axis origins with the center of the A axis, although this may vary according to the cutting method.

## Clamping the workpiece 

You can use the unit’s two-tab chuck to secure either square or round materials in place. 

Secure the workpiece (the material to cut] in place firmly, so that it doesn't slip or wobble. Using manual feed or orient the tightening knob to face upward beforehand can make the task easier to accomplish. 

![mdx10](assets/mdx10.png)

## Tailstock

You use the tailstock to help secure the workpiece (the materials being cut] in place. You use it in combination with the live center and the center drill.

![mdx11](assets/mdx11.png)

## Installing the Live Center or the Center Drill

**Installing**
Insert into the hole in the tailstock spindle. First ex- tend the tailstock spindle by about 5 mm (0.2 in.), then insert. If the tailstock spindle is not extended enough, insertion all the way is impossible.

![mdx12](assets/mdx12.png)

**Detaching**
Retracting the tailstock spindle all the way makes detaching easier.

![mdx13](assets/mdx13.png)

## Drilling a Center Hole

A center hole in the workpiece is required in order to butt the live center flush. Follow the procedure below to drill the center hole.

Attach the center drill.

![mdx14](assets/mdx14.png)

1. Slide the tailstock to bring it flush against the drill.
2. While maintaining contact securely in the direction of the arrow, tighten the retaining screw.

![mdx15](assets/mdx15.png)

Close the spindle cover.

![mdx16](assets/mdx16.png)

1. In VPanel, click the [Cut] icon.
2. Click [DrillWorkpiece].

![mdx17](assets/mdx17.png)

**WARNING**:
This procedure makes the machine operate. Before you perform this procedure, check to make sure that op- eration of the machine will not create any hazard or danger.

Click [Rotate].

![mdx18](assets/mdx18.png)

Slowly turn the adjusting dial and cut in about 3 mm (0.1 in.).

![mdx19](assets/mdx19.png)

1. When you're finished, click [Stop]. 
2. Click [Close].

![mdx20](assets/mdx20.png)

**Important Note on Workpiece Weight**
You can drill workpieces weighing up to 1.5 kg (3.3 lb.) by using this method. 
For workpieces exceeding this, use a different means to drill.

## Butting the Live Center Flush

Butt the workpiece flush securely against the center hole, so that it doesn't slip or wobble. 
Adjust the amount of force to match the workpiece. Applying too much pressure may deform the workpiece or prevent it from rotating smoothly.

Attach the live center.

![mdx21](assets/mdx21.png)

1. Slide the tailstock to bring it flush against the live center.
2. While maintaining contact securely in the direction of the arrow, tighten the retaining screw.

![mdx22](assets/mdx22.png)

1. Rotate the dial a half-turn or so to tighten.
2. Tighten the retaining knob.

![mdx23](assets/mdx23.png)

## The Y- and Z-axis Origin Settings When Using the Rotary Axis Unit

**Understanding A Axis**

- When you're using the rotary axis unit, the way of thinking behind the settings for the origins is different from thinking behind three-axis cutting. 
- When you simply use the A axis as an indexing head and only perform cutting from the top surface of the workpiece, you can accomplish this as an extension of three-axis cutting. 
- A different approach is required for two-surface cutting in which you also rotate the workpiece 180 degrees and cut the bottom surface as well, and for cylindrical cutting, where you perform cutting while rotating the workpiece. This is because rotating the A axis changes the positional relationship between the workpiece and the origins, making it impossible to know the reference position for cutting.
- In such cases, it is most common to set the Y- and Z-axis origins at the center of the A axis. This chapter describes the origin settings for such cases.

![mdx24](assets/mdx24.png)

## Setting the Origins for the Y and Z Axes

- To align the Y- and Z-axis origins with the center of the A axis, you first perform detection of the A-axis center using an origin sensor. This location (the Y coordinate of the A-axis center) is stored in memory in the unit, so you set the Y-axis origin by calling up the saved value.
- Next you set the Z-axis origin. Because this varies according to the length of the tool, you make this setting after first installing the tool. Setting the Z-axis origin is also performed using a sensor.
- For more information on the specific method, refer to the following sections.

`If you are unsure speak to a member of staff.`

**Important !**
When making the settings for the Y- and Z-axis origins , check that no cutting waste and the like is present on the origin sensor and tool. Otherwise, correct measurement may be impossible and the intended cutting results cannot be obtained.

Use manual feed to move the table to-
ward the front.

![mdx25](assets/mdx25.png)

Mount the origin-detection pin on the spindle.
If the machine is equipped with an ATC unit, then load the tool holder mounted with the detection pin in stocker No. 1.
The diameter of the origin-detection pin is 6 millimeters.

![mdx26](assets/mdx26.png)

Connect the sensor cable to the Z-ori- gin sensor.
The sensor cable is included with the model- ing machine.

![mdx27](assets/mdx27.png)

Install theY-origin sensor.
To perform accurate centering, follow the procedure below to attach.
1. Loosely tighten the chuck.
2. Butt the live center flush and secure it in place on the tailstock.
3. Tighten the chuck securely.
4. Tightenthedialahalf-turn.
5. Tighten the retaining knob.

![mdx28](assets/mdx28.png)

## Detection of the Y-origin Sensor

Close the spindle cover.

![mdx29](assets/mdx29.png)

InVPanel,go to the [Options] menu and click [Detect Center of Rotation].

![mdx30](assets/mdx30.png)

**WARNING**:
This procedure makes the machine operate. Before you perform this procedure, check to make sure that op- eration of the machine will not create any
hazard or danger.

Click [Continue].

![mdx31](assets/mdx31.png)

When operation stops and the displayed window changes, replace the cable with
theY-origin sensor.

![mdx32](assets/mdx32.png)

**WARNING**:
This procedure makes the machine operate. Before you perform this procedure, check to make sure that op- eration of the machine will not create any hazard or danger.
Click [Continue].

![mdx33](assets/mdx33.png)

When operation stops and the displayed window changes, rotate the spindle a half-turn by hand.

![mdx34](assets/mdx34.png)

**WARNING**:
This procedure makes the machine operate. Before you perform this procedure, check to make sure that op- eration of the machine will not create any hazard or danger.
Click [Continue].

![mdx35](assets/mdx35.png)

1. Click [OK] to finish detection. 
2. Detach the sensor.

![mdx36](assets/mdx36.png)

### To Make the Tool Descend Rapidly
When it takes a long time for the tool to make contact with the Z-origin sensor, turn the hand wheel on the handy panel counterclockwise. Be careful not to make the tool collide with the sensor. A collision makes accurate detection impossible. If a collision occurs, redo the operation from the beginning.

## Step 2: Set the Y-axis Origin (Call Up the A-axis Center)

Once detection of the A-axis center is finished, setting the Y-axis origin is possible. We recommend performing this operation every time before you start cutting in order to verify that the Y-axis origin is at the A-axis center.

Close the spindle cover.

![mdx37](assets/mdx37.png)

In VPanel, click the [Base Point] icon.

![mdx38](assets/mdx38.png)

1. If you're in the NC-code mode, then select the workpiece coordinate sys-
tem you're using.
2. Select [Y Origin]. 
3. Click [Apply].

![mdx39](assets/mdx39.png)

##  Setting the Z-axis Origin

Once you've installed a tool, then perform the operation to align the Z-axis origin with the center of the A axis. You make the setting using the Z-origin sensor.

Connect the sensor cable to the Z-ori- gin sensor.

*The sensor cable is included with the model- ing machine.*

![mdx40](assets/mdx40.png)

Load the tool.
If the machine is equipped with an ATC unit, then grasp a tool whose tool-length offset value has been registered.

**WARNING**: 
If you're using the stan- dard spindle, be sure to make the amount of tool extension 85 mm (3.3 in.) or less. Otherwise the tool may break, which may cause injury.

![mdx41](assets/mdx41.png)

Close the spindle cover.

![mdx42](assets/mdx42.png)

1. VPanel, click the [Base Point] icon. 
2. If you're in the NC-code mode, then select the workpiece coordinate sys- tem you're using.
3. Select [Z Origin]. 
4. Click[Apply].

![mdx43](assets/mdx43.png)

**WARNING**:
This procedure makes the machine operate. Before you perform this procedure, check to make sure that op- eration of the machine will not create any hazard or danger.

Click [OK].

![mdx44](assets/mdx44.png)

1. Click [OK] to finish making the set- ting.
2. Detach the sensor cable.

![mdx45](assets/mdx45.png)

### To Make the Tool Descend Rapidly
When it takes a long time for the tool to make contact with the sensor, turn the hand wheel on the handy panel counterclockwise. Be careful not to make the tool collide with the sensor. A collision makes accurate detection impossible. If a collision occurs, redo the operation from the beginning.

# Setting the X- and A-axis Origins

The methods for setting the X- and A-axis origins are not particularly different from the methods you use for three-axis cutting. Make the settings to match the shape and size of the workpiece.

In VPanel, click the [Base Point] icon.

![mdx46](assets/mdx46.png)

Move the tool to the location where you want to set the X-axis origin.

![mdx47](assets/mdx47.png)

1. If you're in the NC-code mode, then select the workpiece coordinate sys-
tem you're using. 
2. Select [X Origin]. 
3. Click [Apply].

![mdx48](assets/mdx48.png)

Rotate the workpiece to the angle you want to set as the A-axis origin.

![mdx49](assets/mdx49.png)

1. Select [A Origin]. 
2. Click [Apply].
3. Click [Close].

![mdx50](assets/mdx50.png)

**Important Note on theY- and Z-axis Origins**
In the preceding steps 3 - 2  and 5 - 1, never select the Y-axis origin or the Z-axis origin. Doing so shifts the Y- and Z- axis origins away from the A-axis center.

**Operation Using the Handy Panel**
You can also perform this operation using the handy panel.

## Fine-tuning theY- and Z-axis Origins

When you want to make the position of the A-axis center detected using the origin sensor even more precise, refer to the method described below.
 
**WARNING**:
This procedure makes the machine operate. Before you perform this procedure, check to make sure that op- eration of the machine will not create any hazard or danger.
 
1. In VPanel, click the [Move Tool] icon.
2. If you're in the NC-code mode, then select the workpiece coordinate sys-
tem you're using.
3. Select [Y Origin]. 
4. Click[Move].

![mdx51](assets/mdx51.png)

1. Move theY axis by the amount of the adjustment value.
2. Click [Close].

![mdx52](assets/mdx52.png)

1. In VPanel, click the [Base Point] icon. 
2. If you're in the NC-code mode, then select the workpiece coordinate sys-
tem you're using. 
3. Select [Y Origin]. 
4. Click[Apply].

![mdx53](assets/mdx53.png)

The method just described is for fine-tuning the Y-axis origin. Use the same method for the Z-axis origin as well. For information on how to determine the adjustment value, refer to the following section.

## Determining the Adjustment Value

### AdjustmentValue for theY-axis Origin
- You determine the adjustment value from the difference in levels at the seam produces between the first and second surfaces in two-surface cutting. 
- The estimated adjustment value is one-half the difference in levels. *However*, be careful to note the sign of the value (plus or minus). As the figure shows, the sign of the adjustment value (positive or negative) changes depending on the direction of the misalignment.

- As an example, when the difference in levels is 0.2 millimeters, with the upper level misaligned in the positive direction and the lower level misaligned in the negative direction along the Y axis, then the estimated adjustment value is -0.1 millimeters.

![mdx54](assets/mdx54.png)

### Adjustment Value for the Z-axis Origin
- You determine the adjustment value from the discrepancy between the expected and actual values for the thickness of the finished result of two-surface cutting. 
- The estimated adjustment value is one-half the discrepancy. *However*, be careful to note the sign of the value (plus or minus). - - When the actual value is larger than the expected value, the adjustment value is negative.

- As an example, if data for a height of 50 millimeters yields cutting results that are 50.1 millimeters, the estimated adjustment value is -0.05 millimeters.

![mdx55](assets/mdx55.png)

# Chapter 5

# Getting Ready for and Performing Cutting

## CuttingArea

## Limitations on Workpiece Size

Some limitations affect the size of the workpiece (the material to cut) that you can mount on the rotary axis unit. Exceeding these may cause contact with moving parts, which may cause damage to the workpiece, tool breakage, or malfunction. Be sure to comply with all of the conditions described below.

**WARNING**: 
Failure to comply may lead to danger of injury by a broken tool thrown out with force.

### Height and Depth of the Workpiece

**Maximum Size**
Never exceed the range shown in the figure under any circumstances. Also be careful to note that this may be further restricted by the amount of tool ex- tension.

![mdx56](assets/mdx56.png)

Important Note on Square Material
Be careful to ensure no contact with any corner of the workpiece. For example, when you're using square material whose height is 100 millimeters, the maximum depth is 150 millimeters.

![mdx57](assets/mdx57.png)

### Length of the Workpiece

The length of a workpiece that can be loaded is as indicated below. If an item exceeds this, contact with the live center is impossible.

**When a standard table is installed:**
**Maximum 371 mm (14.6 in.)**

**When a T-slot table is installed:**
**Maximum 297 mm (11.7 in.)**

![mdx58](assets/mdx58.png)

## Limitations onTool Length

The length of the tool is also subject to restrictions. Exceeding these may cause tool breakage, damage to the work- piece, or malfunction. Be sure to comply with all of the conditions described below.

**WARNING**: 
Failure to comply may lead to danger of injury by a broken tool thrown out with force.

## Maximum Amount of Tool Extension
Be sure to keep within the maximum length shown in the figure. Otherwise the tool may collide with the drive unit during initialization, damaging the tool or the unit.
Also be careful to note that this may be further re- stricted by the size of the workpiece.

![mdx59](assets/mdx59.png)

## Limitations Due to Workpiece Size
Make sure that when the tool has been moved to the highest position, the tip of the tool is higher than the rotation range of the workpiece.

![mdx60](assets/mdx60.png)

## Actual SizeThat Can Be Cut

- Cutting the full size of the workpiece is not necessarily possible. 
- The workpiece chuck and the tailstock are located at the two ends of the workpiece. The chucking of the workpiece makes it difficult to cut both ends. 
- Also, the possible cutting-in depth is generally determined by the length of the tool. However, using a lengthy tool to achieve deep cutting reduces allowable size of the workpiece by a corresponding amount.

- It's also necessary to remember that the workpiece rotates during cutting. Depending on the shape of the workpiece and the angle, ensuring the clearance for the tool may not be possible.
- The size of what you can cut varies according to the shape of the object you want to create and the tool you use. Give careful thought to this ahead of time, before you start work.

# Starting Cutting

## Overview of the Procedure

This is an overview of the procedures to follow before you start cutting. This section describes a common cutting method that involves aligning the Y- and Z-axis origins with the center of the A axis.

![mdx61](assets/mdx61.png)

## Setting the Cutting-start Location

- When you've finished loading the workpiece and setting the Z-axis origin, make the settings for the X-axis and A-axis origins. 
- The locations where you should set these origins differ according to the program you're using, but setting them as described below is generally acceptable.

**Typical Setting for the X-axis Origin**
The cutting location along the X axis is determined by the X-axis origin. This is set in various ways depending on the program, such as at the left edge or at the right edge of the range you want to cut. When making the setting, be careful to ensure that the tool does not strike the workpiece chuck or the tightening knob.

**Typical Setting for the A-axis Origin**
For multiple-surface cutting, orient the object so that the surface you want to cut first (the No. 1 surface) faces upward. It may be best to orient it to be parallel with the surface of the table. For round material, setting an A-axis origin may not be necessary.

## Features Added to the Handy Panel

Installing this unit adds the following features to the handy panel.

- Moving the A axis and displaying its location 
- Setting the A-axis origin

The handy panel automatically detects when this unit is installed. You don't need to make any special settings.

# Hand-wheel Feed

You can rotate the A axis using the hand wheel. This operation is basically the same as for the X, Y, and Z axes.

**WARNING**:
This procedure makes the machine operate. Before you perform this procedure, check to make sure that operation of the machine will not create any hazard or danger.

![mdx62](assets/mdx62.png)

## Displaying the A-axis Location

The screen displays the location of the A axis in the same way as for the X, Y, and Z axes. Degrees are used as the unit of measurement, and this cannot be changed. Selecting the coordinate system is also the same as for the X, Y, and Z axes.

![mdx63](assets/mdx63.png)

## Setting the A-axis Origin

This makes the present location the A-axis origin. This operation is basically the same as for the X, Y, and Z axes. To select the axis for setting the origin, press the [A] key.

### Important Note on theY- and Z-axis Origins

The origin settings for the Y and Z axes made with the handy panel are not for aligning these with the A-axis center. Make the settings for the Y- and Z-axis origins using VPanel.

![mdx64](assets/mdx64.png)















## Member Induction sign-off form 

This is the form please follow this link [Plus X Workshop Induction — MDX-540 rotary 4th axis](https://forms.gle/yBeU68n4WU9xzAMm7)

**this must be completed before any further use of the machine**