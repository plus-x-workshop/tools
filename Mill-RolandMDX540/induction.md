Last reviewed 2025-03-06 by AS

# Roland MDX-540 CNC milling machine Induction


<!--Meta-->

## Induction

Author: 
Last reviewed date: 

<!--Intro-->

## Description / intended use

3 Axis modelling machine for compiuter-controlled milling of wood, plastics and non-ferrous metals

## External documentation and links

[Manual](assets/MDX-540_USE_EN_R5.pdf?ref_type=heads)

## Risks


| **Hazard**                                                   | **Controls**                                                 |
|--------------------------------------------------------------|--------------------------------------------------------------|
| **Risk of Trapping/Entanglement**<br>Danger of crushing body parts between moving parts, entanglement with spindle | Machine has a safety cover with interlock switch.            |
| **Electricity**<br>Risk of injury due to faulty equipment, contact with live electrical components or improper use. | Machine subject to regular inspections and maintenance.<br>Machine to be inspected before use. |
| **Noise levels at/above 85 dB(A)**<br>Risk of hearing damage due to exposure to excessive levels of noise. | PPE: Hearing protection to be worn for longer periods of use. |
| **Ejection of debris**<br>Injury to eyes or body             | Machine has safety cover with interlock switch.<br>Training covers fixing stock securely. |
| **Fire**<br>Incorrect milling parameters on wood / metal with coolant can cause fire | Always watch the machine while it is in operation            |
| **Dust / fumes**<br>Some materials may produce hazardous fumes / fine particulates that can be harmful to inhale | Extraction system to be on for all milling operations.       |
| **Cuts**<br><br>Bumping hand into milling cutters while fixing workpiece or cleaning inside machine | Be careful of milling bits when handling material inside the machine |
| **Fire**<br>Cutting waste may get inside the machine and cause fire or electrical shock.	  | Never use compressed air to clean inside machine, use extractor or dustpan and brush |



<!-- Main section -->

## Safe Working Practices

### Before use

* Close machine cover.  
* Turn on main power.  
* Press Enter (on hand panel) when instructed and when machine area is clear.  
* After machine has stopped the origin process, RML light will be on.  

If using VPanel software, you may need to confirm the machine ID (“1”) and command set (“RML-1”) when starting up.
 
#### Opening and closing the cover 

Ensure the cover is properly seated at open and close positions.  
Do not attempt to override the safety cut-offs on the door.  
 
#### Installing tools 

Please ask a member of the workshop team if you need to change a milling bit. 

You will need to consider the bit dimensions and capabilities when designing your model. These include end shape, length of the flutes and diameter.  
 
#### Choosing feeds and speeds 
Please speak to a member of staff if you would like to discuss optimum settings. It is a complex subject and there ae some useful resources online, e.g. https://www.cnccookbook.com/feeds-speeds/ 
Many CAM tools calculate feeds and speeds for you. 
 
* `Feed rate`: rotational speed of the bit 

* `Surface speed`: speed of the head as it moves across the workpiece 

* Link for calculating the rpm, feed rate and power: https://app.fswizard.com/
 
#### Using the rotary attachment 
Please ask a member of the workshop team if you need to use the rotary axis. 
 
#### Extraction 
The workshop extraction should be switched on when the machine is cutting. Ear defenders should be worn when cutting. 
 
#### VPanel and Hand Panel 
Many functions of the machine can be controlled from the hardware hand panel. These controls, and more, are replicated in the software VPanel installed on the PC. 
 
#### Machine vs User co-ordinates 
It is possible to set multiple co-ordinate systems. However, we only use one. Ensure that you are using “User Coordinate System” at all times. 
 
#### Origins and axes 

![Orgins and Axes](assets/origins_axes.png)
 
i.e.  
* The X/Y origin is front left of the machine 
* Y is font/back 
* X is left/right 
* Increasing Z moves the tool up 
 
![warning_z](assets/warning_z.png)



### In use

* Do not attempt to override the safety cut-offs 

* Be careful of milling bits when handling material inside the machine, they can be sharp 
 
* Be careful not to run the milling bit into a workpiece, especially if the spindle is not running. 


#### Emergencies 
 
* Press Emergency Stop button to immediately stop cutting and abort cutting job. 
  
![emergency stop button01](assets/emergency_stop_button01.png)

* To remove machine from E-Stop condition, turn off machine and twist button.  

![emergency stop button02](assets/emergency_stop_button02.png)




 
### Operation Summary 
* Check machine setup – correct bit, sacrificial bed OK 
* Add material 
* Set machine origins 
* Prepare job for milling including CAM origins 
* Mill and tune feeds and speeds 

## Adding Material 
Use VPanel or the hand panel to bring the bed to the front 
    + Press the *Menu* button until you get to the Move Menu 
    + Use the jog wheel to select *View*  
Use double sided tape to fix your workpiece to the sacrificial board.  
Make sure the workpiece and sacrificial bed are free from dust before applying tape. 
If the sacrificial board needs changing, please ask a member of the workshop team. 
Try to align your workpiece square with the front of the bed 
 
## Setting X/Y Origin  
 
**`Move tool to origin point selected in software (usually front left top)`**
 
* Make sure your coordinate system is set to User Coordinates 
  + In Hand Panel, Press Coord System to toggle between User and Machine XY 
* Close the lid 
* Carefully bring tool down in Z so it’s easier to set X/Y origin 
  + Press Z on hand  
 
![warning_tool](assets/warning_tool.png)
 
* Use the XY keys and jog wheel to move the tool to the origin 
* Set origin point at this location by  
  + On hand panel, press X, long press `Origin`, repeat for Y   
    - Check that X and Y values are 0 
  + Or, using Vpanel and selecting `“Set XY Origin (Home) here”`, Click `Apply`, Click `Close` to close window.  
 
## Setting Z Origin 
 
* Place Z0 sensor above material, connect sensor cable to the sensor and Z0 connector (the brass piece of the sensor should face up). 
Diagram, engineering drawing

![z0_sensor](assets/z0_sensor.png)

* Move tool so that it is right above the sensor, about 5 mm away 
* Set Z Origin by: 
  + On hand panel, Long press Z0 Sense button 
  + On VPanel, click on “Set Z origin using tool sensor” and click on Start Detection.  
* After tool touches sensor, remove sensor from cutting area. 
 
    *Machine is now set up and ready to receive commands from SRP Player or other CAM software.*  
 
![origin_point](assets/origin_point.png)

## Software 
 
### VPanel 
Software version of hand panel, for moving tool, setting origin etc. 
Also for sending RML files you have prepared elsewhere (e.g. a PCB from Fabmodules) 

![vpanel](assets/vpanel.png)

### SRP Player 
 
Setting up jobs for milling from an STL file (e.g. a 3D form for molding) 
Also for surfacing material. 
 
## Sending Jobs 
### Using SRP Player to send a 3D job 
  
If you have an STL file from your CAD software, you can use SRP Player to generate tool paths. 
 
![warning_material](assets/warning_material.png)
 
**`Check and confirm model size and orientation`** 
 
* Open your STL in SRP Player 
 
![model_size](assets/model_size.png)
 
## Choose the type of milling  
 
Including whether to optimise for speed or quality, flat or curved surfaces 

![type_milling](assets/type_milling.png)

**`Create toolpaths: 1 Basic setup`**

Choose your material, set the size of your workpiece and set the placement of your model within the workpiece – typically on the top face 
 
![create_toolpath](assets/create_toolpath.png)
 
**`Create toolpaths: 2: Edit roughing toolpath`** 

![toolpath_roughing](assets/toolpath_roughing.png)
 
`Roughing > Top Surface > Modelling Form` 
 
Set margins to a value less than the tool diameter if you don’t want to cut out around your model 
 
![toolpath_margins](assets/toolpath_margins.png)
 
`Roughing > [Milling bit name, e.g. 3mm Square]` 
 
Confirm the milling bit you will use for the roughing pass 
Confirm this is loaded in the machine and that the dimensions match those shown in the software. 
 
Apply and close to confirm changes 
 
**`Create toolpaths: 3: Edit finishing toolpath`** 
 
Repeat for the finishing toolpath 
 
**`Create toolpaths: 4 Create and preview paths`** 
 
Press **Create Tool Path** to generate the paths. 

Then press **Preview Results** > **Preview Cutting** to generate an onscreen preview 
 
Verify the model preview and cutting time, and go back to previous steps to adjust settings if necessary. 
 
You will need to generate new toolpaths every time you change settings.

## Perform Cutting 

Press **Perform Cutting** > **Start Cutting** ... 
 
It will ask you to check if you have the correct milling bit in the machine. 
 
It will not automatically change the bit for you, you will need to come back and change the milling bit (ask a member of staff for help on this) 
 
## ORIGIN 
Make sure you have chosen the correct origin for the machine. Top-Front-Left of the workpiece. 
 
![origins](assets/origins.png)
 
# `SWITCH ON THE EXTRACTION NOW!`  
 
### Troubleshooting 
 
If the machine cuts air instead of the workpiece 
 
1. Verify you have set Z0 to be the top of the workpiece, and that you set your cutting origin in SRP player to the top of the workpiece 
2. Verify that your roughing tool is sufficiently small to carry out the job 
3. Verify that the total cut depth does not exceed the cutting depth limits of the machine.  
    - Use VPanel to move the tool to Z0 (switch the spindle on first to ensure you don’t damage the bit) 
    - Switch to machine co-ordinates and make a note of the Machine Z0 (e.g -110mm) 
    - In SRP Player, check the total cut depth (e.g. 60mm) 
    - Subtract the cutting depth from the machine Z0. If this exceeds –150mm, you have exceeded the machines Z limit and it will not cut 
    - The solution is to build up additional material on the sacrificial bed to raise your workpiece. 

<!-- ### Using VPanel to send an RML file for a circuit board 
We recommend using the SRM20 for PCBs.  -->
 
### Creating an RML file 
 
RML is Roland’s proprietary Machine Language.  
 
Currently we support the use of Fab Modules to produce RML files. We will consider other workflows on request. 
 
### Sending RML files 
Once you have an RML file of the correct spec, you can send it directly with VPanel. 
Press the Cut button in the toolbar to bring up the file selection dialog.  
 
Note that the job will start as soon as you press Output – ensure the machine and bit are set up correctly. 
 
![cut](assets/cut.png)
 
<!-- ## Rotary attachment 
 
This is currently in testing.   -->
 
## In operation 
 
The workshop extraction should be switched on when the machine is cutting. Ear defenders should be worn when cutting. 
 
Observe the machine while cutting, and use the override controls to adjust the travel and spindle speed if you sense too much vibration or noise from the bit. 
 
![warning_colliding](assets/warning_colliding.png)
 


### After use

* Remove your workpiece 
* Clean up any sticky tape residue 
* Use a (clean!) paint brush to wipe down the insides of the machine 
* Hoover out with the Festool extractor  
 * use metal extractor if you have been milling metal
 * do not use air compressor



<!-- ### Staff only: maintenance and cleaning -->

## PPE Requirements

Ear defenders, when extraction is on, or if milling is noisy

## Member Induction sign-off form 

[Plus X Workshop Induction — MDX 540](https://forms.gle/o4LtHT72dHNhk3uC9)

This must be completed before any further use of the machine.
 
 
 



