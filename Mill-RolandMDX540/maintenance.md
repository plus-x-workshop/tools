Last reviewed 2022-08-18 by KI

# Roland MDX-540 CNC milling machine maintenance

### Maunal

* [User Manual (PDF)](assets/MDX-540_USE_EN_R5.pdf)

### Firmware Updates

n/a

## Contacting

* If a technical issue 'log a case' under 'Support' on their [website](www.rolanddg.co.uk) before calling.

## General Maintenance
* Hoover: x-axis, y-axis and z-axis making sure to pull the blue material out of the way and moving the material around to get around teh back of it. 
* check all milling bits are safe and none are missing

### Chapter 6 in [User Manual (PDF)](assets/MDX-540_USE_EN_R5.pdf) pages 96-101

#### Daily Care 
* Cleaning the X Axis: *page 96*
* Cleaning the Y Axis: *page 97-98*
* Cleaning the Z Axis: *page 98*
* Care and Maintenance of the Collet and Spindle Nos: *page 98*
* Cleaning theVentilation-duct Filter: *page 99*

#### Inspection and Maintenance

* Checking the Total Working Time page 100
* Lubricating the Ball Screws page 101
* When to Service the Spindle page 102

## Surfacing the bed

* This can be done manually using the V-panel
