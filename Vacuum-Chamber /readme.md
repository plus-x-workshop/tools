last reviewed 2022-08-24 by KI

# 26L Professional Vacuum Chamber Degassing Chamber readme

serial number: 2048826

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintanence.md)

## External docs

* [User manual (PDF)](vacuum-chamber-manual.pdf)

## Contact information

* [website](https://www.easycomposites.co.uk/ds26-p-professional-vacuum-degassing-system)

## Issues

* [Issues tagged with v-chamber](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=v-chamber&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|