last reviewed 2022-08-25 by KI

# 26L Professional Vacuum Chamber Degassing Chamber maintenance

[User Manual PDF](assets/vacuum-chamber-manual.pdf)

### Firmware Updates

n/a

### General Maintenance

Periodically check the oil level in the pump and top-up as necessary using VPO32 Vacuum Pump Oil. If oil appears contaminated (cloudy), empty it from the pump and replace it with fresh vacuum pump oil.

* Never run the pump unattended.
* Never use the system on the floor.
* Never operate the pump in dusty conditions. Dust particles will be drawn into the pump and will contaminate the oil and accelerate wear.
* Don't use the system as a vacuum dryer. Moisture extracted from damp materials such as wood or plaster will emulsify the oil.
* Never use anything other than DVP BV32(SW40) or Easy Composites’ VPO32 Vacuum Pump Oil to top-up your vacuum pump.
* Never use solvents to clean the pump.

* [website](https://www.easycomposites.co.uk/ds26-p-professional-vacuum-degassing-system)

### Replacement parts

* [Silicone Seal for DC26 Degassing Chamber (Replacement)](https://www.easycomposites.co.uk/replacement-clear-lid-for-dc26-degassing-chamber)
* [Silicone Seal for DC26 Degassing Chamber (Replacement)](https://www.easycomposites.co.uk/silicone-seal-dc26-vacuum-degassing-chamber)