# Vacuum / Degassing Chamber Induction

<!--Meta-->

## Induction

Author: Andrew Sleigh
Last reviewed date: 2024-09-06

<!--Intro-->

## Description / intended use

- For degassing silicone and resins for molding and casting


### Restrictions:
- Don’t use the system as a vacuum dryer / dehydrator
- Don't use to degas water or high water-content liquids: this will damage the compressor. If you see any condensation in the chamber, stop the comprressor immediately, slowly relieve pressure, and sek assistance from a member of staff. 


## External documentation and links

-   [Manufacturer website](https://www.easycomposites.co.uk/ds26-p-professional-vacuum-degassing-system)
-   [Manual (PDF)](assets/vacuum-chamber-manual.pdf)


## Risks

| **Risk** | **Control**                                                  |
| --------------------------- | -------------------------------------------------------------------------------------------------- |
| **Crushing**<br>Chamber is heavy and is often moved to a convenient place before operation. Could fall on feet, and is used in a room where steel toes are not worn | Take care and seek assistance when moving machine                                                  |
| **Electricity**<br>Risk of injury due to faulty equipment, contact with live electrical components or improper use.                                                 | Visual checks to be carried out before use<br><br>Included in regular inspection schedule by staff |
| **Fire**<br>Risk of injury caused by faulty electrical equipment                                                                                                    | Visual checks to be carried out before use<br><br>Included in regular inspection schedule by staff |
| **Maintenance or other abnormal tasks**                                                                                                                             |                                                                                                    |
| **Unauthorised use**<br><br>Crushing, or electrical  damage                                                                                                         | Staff to disable machine and install signage when under maintenance.                               |
| **COSHH**  <br>Skin irritation from machine oil                                                                                                                     | Wear nitrile gloves                                                                                |                                                                   |

<!-- Main section -->

## Safe Working Practices

### Before use

User checks:
- Machine is located on a stable surface
- Machine is in good condition with no visible damage
- Always check the oil level before operation, notify staff if top-up is required

### In use

## Operation
- Check that the inlet lever is closed (set 90 degrees to the fitting) - the lever is found on the right of the chamber
- Remove the acrylic lid and load in the material for degassing
- Reinstall the lid, pressing firmly and checking good contact with rubber seal
- Turn the vacuum pump on using the black plastic switch on the left of the compressor - you may need to press the lid down to initiate seal
- Wait while the pressure to drop inside the vacuum to a level where degassing occurs - about 1 minute
- Wait for your material to degas, as required
- Switch off the pump
- Open the inlet valve to re-pressure the chamber
- Remove the lid and the degassed materials

### After use

- Clean any mess or spills within the chamber

### Staff only: maintenance and cleaning

- Check oil levels

## PPE Requirements

None

## Sign-off

[Plus X Workshop Induction — Vacuum Chamber](https://forms.gle/ViJp2UR76pL3ncaF9)

This must be completed before any further use of the machine
