# How To Use the Tormek Sharpening Wheel

## Safety

- Beware of loose clothing or hair being trapped in the wheels, or your fingers being trappped between the grinding wheel  and tool rest or jig
- Always run the stone in water when sharpening - this is a water-cooled system
- For honing, the wheel should always run away from you (and the cutting edge)
- Do not lift the grinder with the tool rest, use the carrying handle
- Be careful of water spillage around or on the machine. Always clean up any water spills or drops immediately

## Documentation

- Website: https://www.tormek.com/international/en/machine-models/tormek-t-4-original/
- Getting started video: https://www.youtube.com/watch?v=P1qTcD1kKis
- Tormek on Youtube: https://www.youtube.com/channel/UCd7yqvqF2BuoHNCuw8GMGRg

- How to sharpen a damaged chisel: https://www.youtube.com/watch?v=kMCwG8xehGE
- Sharpen chisels with the Tormek Square Edge Jig: https://www.youtube.com/watch?v=KZ07gPtB6pg

 - [Square Edge Jig Manual (PDF)](se77-manual.pdf)


## Setup

Fill water trough up to max level. The wheel will absorb some of the water if dry, You may need up to 0.6 litres of water.

![](assets/fill-trough.png)

Always use fresh water


## Usage

Rest the blade against the tool support (or use one of the blade mounts) and run the edge againt the stone wheel. Use the straight edge jig for chisels and plane blades

![](assets/straight-edge-jig.png)

When initially smoothing the back of a new chisel, use the side of the grinding wheel. **This should only need to be done once – on new chisels.**

Carefully position the tool against the wheel. The edge must not touch the wheel before the heel! Flatten the back of the tool by holding it flat to the grinding wheel while moving it slightly. Otherwise the tip can cut into the wheel and be rounded off.

Let the side of the tool rest on the Universal Support, which should be placed close to the wheel as shown. You do not need to smooth the tool more than 25–30 mm from the edge.


### Using the square edge jig (SE-77)

Line up the chisel against the right angle stop on the far edge of the jig – the chisel must be straight against the wheel for a 90° edge.

Set the angle (25° for a chisel) using the Angle Master guide (WM-200)). The wheel diameter should be set to the diameter of the grinding whel (200mm for the standard SG-200 wheel we have).
Adjust the angle with the height dial on the vertical tool rest supports.

![](assets/angle-guide.png)

Use the safety stops to stop the chisel from coming off the wheel.

Smooth the wheel with the stone regrader (SP-650) for a finer cut. Always regrade the stone with the coarse side after doing this.

Use the leather honing wheel to finish each side and remove the burr. Remember, for honing, the wheel should always run away from you (and the cutting edge)

### Aligning the knobs on the Universal Support

If the chisel is not coming out at 90 degrees this might be because the knobs on the right side of the Universal Support aren't lined up. 

- Use the knobs to align the lines at the back as seen below in the image. 
- If your chisel end or other tools is not coming out straight then adust the knobs as needed 

![knobs.jpeg](assets/knobs.jpeg)![side_knobs.jpeg](assets/side_knobs.jpeg)![line.jpeg](assets/line.jpeg)


### Notes:

<!-- - Grind the cutting edge of the tool, not the flat face underneath  -->
- You only need to apply finger-tip pressure
- Work back and forth across the wheel
- Hold the whole tool over the wheel (not at an angle over the machine casing), so water doesn't run down onto the machine
- Hold the tool with the handle up, so water runs back down onto the wheel, not down the handle.
- The machine can be used in both directions - typically you would sharpen a chisel with the wheel running towards the cutting edge. For honing, the wheel should always run away from you (and the cutting edge)
- For a finer grind, re-grade the wheel with the fine face of the stone grader.


![](assets/usage.png)


## After use

Let the steel particles sink to the bottom of the trough before emptying the water.

Always lower the trough and empty the water, so the stone is not sitting in water. 



## Maintenance



### Cleaning the rubber honing wheel mount

If it becomes coated with wood dust, clean with sandpaper while the wheel is turning

![](assets/honing-wheel-dust.png)

### Periodically apply new honing compound

![](assets/honing-wheel-compound.png)

### Grade the grinding stone 

To keep it flat and clean by running the coarse ege of the stone grader against the wheel for about 20 seconds.
You can use the fine face of the grader to smooth the wheel for a finer finish.

![](assets/stone-grader.png)

### Flatten the grinding stone

Periodically, you may need to flatten the wheel with a Tormek trueing tool


## How do you know when you've succeeded?

## Chisels

- Chisel bottom face is dead flat and mirror finished 
- Cutting edge is straight and perpendicular to length
- Only one bevel is needed (easy to regrind once dullled or damaged)
<!-- 
## Planes

## Knives

## Scissors -->


  