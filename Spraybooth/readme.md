last reviewed 2022-08-23 by KI

# STL Spraybooth readme

serial number: n/a

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

## External docs

## Contact information

Service Administrator:
* Sarah Burdett
* Email: sarah.burdett@sprayboothtechnology.co.uk
* DDI: 01787 314112

Project Manager:
* Ben Fair
* Email: ben.fair@sprayboothtechnology.co.uk
* DDI: 01787 314137
* M: 07939365312

## Issues

* [Issues tagged with Spraybooth](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=Spraybooth&first_page_size=20)

## Services 

| Date       | Link to  Reports                                                      |
| -----------| --------------------------------------------------------------------- |
| 2021-02-08 | [Spraybooth Warranty Terms & Conditions](assets/terms_conditions.pdf)|
| 2021-02-08 | [STL Service Agreement](assets/stl_agreement_service.pdf)            |
| 2022-03-10 | [Serivce Visit 2022](assets/spraybooth-2022-service-visit.png)       |