Last reviewed 2023-09-01 by AS

# Spraybooth and Spraygun Induction

## Spraygun Documentation

-   [Manufacturer website](https://www.hvlp.co.uk/apollo-spray-tan-1500-3s-vari-speed-professional-spray-tan-system/)
-   [Manual (PDF)](assets/spray-gun-manual.pdf)


## Intended use

For spraying of organic finishes such as paint and varnish.

Authorised finishes:
- Vinyl / acrylic / latex paint or primer
- Cellulose paint
- PU (polyurethane) varnish

Please discuss any other finishes with us *before* use.

Specifically prohibited: 
- Enamel or other oil-based points (unless you provide your own spray gun, or use a can)
- **Two-part paints with isocyanate binders**
- Dust-emitting processes (use heavy workshop)
- Any finishes that require baking above room temperature
- Any finishes that require thinning or cleaning solvents that can't be disposed of in domestic waste
- Any hazardous substance that has not been approved by workshop manager

Consider cleanup before selecting a finish. You are responsible for cleaning up equipment and removing waste safely and legally. 

- Used paint and solvents can not be poured down the drain, inside or outside. 
- You must not bring hazardous chemicals such as paint thinners into the building without prior permission
- You must not store or leave behind hazardous chemicals such as solvents or thinners
- You must remove all paints, solvents and waste from the spraybooth after use and dispose of them suitably. We cannot store your waste.

## Safe system of work

### PPE

You are responsible for providing your own PPE. We cannot provide RPE (Respiratory Protective Equipment) for you to borrow.

- You must wear at minimum a half-face spray mask with **A2P2 filters**, for example: <https://www.3m.co.uk/3M/en_GB/p/d/v100570021/>
- You must wear gloves and eye protection when spraying or handling paints and finishes


### Spraybooth

Ensure the door is properly closed when in operation. 

Do not allow access to anyone not wearing the correct PPE.
<!-- 
Mark on the whiteboard on the door when you have finished spraying and when is the safe clear time (4 minutes later), e.g. 

- Your name
- Today's date
- Time finished spraying
- Safe time (finish time + 4 mins) -->

### Spraygun

<!-- - All spray paints can be hazardous to health if not handled correctly -->
- Never direct paint at any part of the human body
- Never dismantle the spray gun or disconnect hoses to compressor while it is switched on or running
- Paints can damage the compressor - always position the compressor away from direction of spray or 'up wind' of direction of air flow
- Do not touch the compressor outlet and hose connection during operation - these will become hot with normal use and require sufficient time to cool down


## Basic spraybooth operation

![Spray controls](assets/spray-controls.jpg)

### Lights

Hold down the red stop button for 3 seconds. Then lights will turn on.

### Extraction

- Hold down the green 'spray' button. 
- Wait till the pressure number on the screen stops increasing
- Make sure the door is closed when spraying 
- Once finished leave on to allow the fumes to be cleared - close the door, and write up your spray finish time on the door so others know when it is safe to enter
- To turn off hold down the red 'stop' button 

### Baking

Baking is not permitted under normal use. Please ask if you would like to use this function. Baking will set off the fire alarm.

<!-- Above 50C -->

### Spraying


#### Preparing the paint

- Prepare and use your paint as per the manufacture's instruction - it is your responsibility to check suitability, compatibility and safety requirements of the paint being used.
- The paint viscosity should be correct to ensure optimum spray and finish - measure the viscosity of the paint using the viscosity cup:

|type of product                |approximate time (s)                          |
|----------------|-------------------------------|
|cellulose|18            |
|creosotes          |use as supplied           |
|emulsion        |27|
|hammer|24            |
|enamel          |24            |
|polyurethane          |24|
|wood stains|use as supplied            |
|adhesives          |17            |
|gel coats          |18|

If in doubt, a good rule of thumb is try to achieve the same viscosity as single cream

Fill the spray cup about 3/4 full and fit the cup to the gun

#### Setting up the compressor

- Remove the gun from the compressor by lifting it while pushing the brass collar down
- Connect the gun to the hose by using the the brass collar
- Set the power knob on top of the compressor to 4 or 5

#### Adjusting spray shape

![Spray Pattern Adjustment](assets/spray-gun-shape.JPG)

#### Adjusting paint flow

The flow of the paint and the heaviness of each layer can be adjusted by using the metal silver knob - it is often better to spray two light coats rather than one heavy coat

<!-- ### Spray cans -->

### Correct position to stand

![Spray position](assets/spray-position.jpg)

- Make sure to stand to the side of your piece with the wall filters to one side
- See *Position B* in the image above on where to stand in the booth correctly 

### After spraying 

#### Cleaning

- Empty any unused material from the paint cup and wash out any residue with appropriate solvents - **the paint cup has a non-stick coating, do not use medal or sharp utensils to clean**
- Ensure the paint feed tube is also clean
- You must provide your own cleaning solvents and ensure they are safe to dispose of in domestic waste.
- Remove air cap and spray jet, and clean with appropriate solvents - ensure the air holes of the cap are clear
- Ensure all parts are clean before re-assembly
- Put approx 100ml of clean water in the cleaned cup and spray it through the gun (at the wall filters) to ensure the whole system is clear

You will be billed for a new spray gun if you leave the gun unusable for the next person.

#### Tidying

- Take all your materials and waste with you
- Do not leave half-used paints in the space
- Leave the spraygun and attachments clean and tidy for the next person (do not leave parts out to sry on the sink)

You can leave your workpiece in the space for 24 hours, but be aware someone else may move it if they want to use the spraybooth.

#### Disposal of waste

Paints and solvents can be highly hazardous to teh environment, wateer supply, and in some cases are. aerious fire risk.

- Used paint and solvents can not be poured down the drain, inside or outside. 
- You must not bring hazardous chemicals such as paint thinners into the building without prior permission
- You must not store or leave behind hazardous chemicals such as solvents or thinners
- You must not put rags or paper towels soaked in flammable solvents in waste bins
- You must remove all paints, solvents and waste from the spraybooth after use and dispose of them suitably. We cannot store your waste.

Some options:
- Consider using water-based finishes first. Unused finish can be left to dry and then disposed of in general waste.
- Local recycling does accept domestic waste, so paints used for personal projects can be taken to Brighton recycling sites: <https://www.brighton-hove.gov.uk/rubbish-recycling-and-streets/recycling/recycling-z#chemicals>
- Use spraycans for small jobs. _Fully discharged_ spraycans can be recycled.


## Member Induction sign-off form 

Please review and sign this form before using the sppraybooth [Plus X Workshop Induction — Spray Booth](https://forms.gle/kqeQktCQTY6vs6d59)  *this must be completed before any use of the machine*

<!-- 

## Member Induction sign-off form
This is the form please follow this link [Plus X Workshop Induction — Spray Gun](https://forms.gle/B6vNeNVE9EjWcPB37)

#### *this must be completed before any further use of the machine* -->

