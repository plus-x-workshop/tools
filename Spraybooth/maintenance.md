last updated 2022-08-25 by KI

# STL Spraybooth maintenance

[User Manual (PDF) - n/a](assets/)

### Firmware Updates

n/a

### General Maintenance

* they will do a yearly maintenance check as well as an LEV test to check nothig is leaking
* if something was to go wrong you shuould contact them and not try and sort the issue out
* see [readme](https://gitlab.com/plus-x-workshop/tools/-/blob/master/Spraybooth/readme.md) for contact details 

### Replacement parts

