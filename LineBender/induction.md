Last reviewed 2024-02-27 by AS

# Formech 500 Linebender Induction

|   **What are the hazards?**   |   **Who might be harmed and how?**   |   **Controls**   |  
|---|---|---|
|   **Electricity** Damaged casing or cables could be live     |    Electrocution or electrical shock  |   Regular PAT testing of all devices. <br> Operator to visually inspect tool for damage before use.  |  
|   **Fire** Plastic material or other material that falls into element could catch fire  |   Risk of fire and burns  |   No unattended use allowed.  No unauthorised materials allowed.    |  
|   **Hazardous substances** Volatile fumes from plastics  |   Inhaling toxic fumes from plastics  |   No unauthorised materials allowed. Machine used in ventilated space.  |  
|   **Hazardous substances** Skin contact or inhalation of unknown finishes brought in by users   |    Unknown risks faced by operators if they bring in unknown or unauthorised finishes  |   Users inducted in rules for safe use of spray booth – no unauthorised finishes permitted, new finishes to be assessed by user and workshop manager prior to use. Users given guidance on how to do a COSHH assessment.  |  
|   **Burns** Heating element, top plate of machine, or heated material   |    Could be burned by touching hot parts of machine, or handling heated plastic  |   Gloves to be worn Heating element has guard.  |  



## Materials

- Acrylic
- Polystyrene
- ABS
- Others TBC. If you would like to use a material not on this list, please check with workshop staff.

Some plastics such as polystyrene have a wide tolerance to heating times giving consistently good mouldings. Care must be taken with other materials such as ABS, as the surface will blister if the temperature limit is exceeded.
The sheet should be formed when the plastic is sufficiently relaxed to bend to the desired angle.

## Main Components and Accessories

- Line bender
- Material clamp
- Width reducers
- Cooling jig


### Typical heating times

- 3 mm Acrylic: 1-2 minutes at heat setting 5
- Other TBC

## Basic Operation

Remember:
- Never leave the machine unattended
- Be aware of the hot element and top area of machine – even after it is switched off
-  There is a risk of being burnt when handling heated plastics. Always wear suitable personal protective equipment such as gloves.


1. Plan the order of your bends – protoype with cardboard first
2. Set the cooling jig to your desired angle
3. Start with the temperature regulator set to position 5 and raise the material clamp to the rear position. **Always raise the material clamp from the heating element when  not in use**
4. Place the material on the machine top over the element and lined up on the grid, and then bring down the material clamp. The clamp helps to keep the material flat over the element during the heating process.
5. Lift the clamp, and try to bend the material carefully in situ to see if it is ready for bending.
6. Once the material has been heated to the correct temperature, lift the material clamp and remove the material. The material is then placed into the cooling jig to cool down to a predetermined angle. You can use a piece of wood or other former to push the sheet into the jig.
7. **Remember to switch the line bender off after use.**


## Using the width reducer

- The width reducer can be used if you require a particularly sharp bend in thicker material or to create an accurate sharp bend in material of less than 1mm.
- The standard width reducer is simply placed over the element and is self locating.
- There is an additional narrower width reducer for materials in the region of 0.5mm thickness

## Member Induction sign-off form 

[Plus X Workshop Induction — Line Bender](https://docs.google.com/forms/d/e/1FAIpQLSfCU-nMjoYx9qjx3iOu3jQD_C7zmVNdL_hbKiEwS9KjiLl5Hw/viewform?usp=sf_link)

#### this must be completed before any further use of the machine
