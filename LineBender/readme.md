last reviewed 2022-08-23 by KI

# Formech 500 Linebender readme

serial number: 3093

## Our docs: 

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

## External docs

* [User manual (PDF)](assets/Manual-FLB500.pdf)

## Contact information

* [Website](https://formech.com/contact/)

* Email: Claire Simpson (Logistics Manager) claire@formech.com  
* Tel:  +44 (0) 1582 469 797

 ## Issues

* [Issues tagged with line-bender](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=line-bender&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|