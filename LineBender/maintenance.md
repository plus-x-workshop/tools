Last reviewed 2022-08-18 by KI

# Formech 500 Linebender maintenance

[User Manual PDF](assets/Manual-FLB500.pdf)

### Firmware Updates

n/a

### General Maintenance

- inspect the machine, check the cables aren't damaged 
- any major issues should be taken to Formech and sorted via their technican. 

