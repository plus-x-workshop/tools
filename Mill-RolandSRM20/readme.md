last reviewed 2022-08-26 by KI

# Roland SRM-20 CNC milling machine readme

serial number: ZEH2245

## Our docs:

* [Induction document](induction.md)

* [Maintenance guide](maintenance.md)

* [Standard Operating Procedure](sop.md)

## External docs

* [User manual (PDF)](SRM-20-manual.pdf)

## Contact information

* Email: techsupport@rolanddg.co.uk
* Number: +44 (0)1275 335540
* If a technical issue 'log a case' under 'Support' on their website before calling [website](www.rolanddg.co.uk)

## Issues

* [Issues tagged with SRM20](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=SRM20&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|