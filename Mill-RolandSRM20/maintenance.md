Last reviewed 2022-08-26 by KI

# Roland SRM-20 CNC milling machine maintenance

[User Manual PDF](assets/SRM-20_user_manual.pdf)

### Firmware Updates

n/a

### General Maintenance

* Hoover: x-axis, y-axis and z-axis making sure to pull the blue material out of the way and moving the material around to get around teh back of it. 
* check all milling bits are safe and none are missing

#### Manual pages

[User Manual PDF](assets/SRM-20_user_manual.pdf)

* The Machine Run-in: *pages 81-82*

* Important Notes on Care and Maintenance: *pages 118-144*

* Cleaning after Cutting Operation Ends: *pages 119-122*

* Replacement of the Consumable Parts: *pages 122-126*

* What to Do If: *pages 126-143*

* Responding to Error Messages: *pages 144*

### Surfacing 

* Change to a 6 mm square end mill (change collet too if needed for a larger shank)
* Measure the size of the bed (use the *View* option in  VPanel to move the bed forward). Allow 5-10mm extra in each axis
* Use *VPanel* software to set XYZ origin to be on current surface of bed and just outide front left corner
* Open *Clickmill* software:

![srm20-surface1](assets/srm20-surface1.png)

* Switch to the *Surface* tab
* Select *Lower left point and lenghts of sides* from dropdown
* Set 0,0 as lower left point
* Enter the width and length you measured
* Select a suitable Z depth (e.g 2 mm) and a safe Z-up clearnce height (e.g. 10 mm)
* Press *Cut...*

![srm20-surface2](assets/srm20-surface2.png)

* Ensure the correct bit is selected
* Select the appropriate material (e.g. Chemical Wood (Hard) for MDF)
* Press *Cut*. The job will start immediately
* After the job, vacuum up the waste material and change the collet back to the regular one.

### Replacement parts

* [shop-apt](https://www.shop-apt.co.uk/) for milling bits


