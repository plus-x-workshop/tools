Last reviewed 2021-10-09 by AS

# Roland SRM-20 CNC milling machine Induction

## Manuals

* [SRM-20 Manual](srm_20_manual.pdf)

## Materials
* NO: Fibre Glass, FR4
* YES: FR1 and FR2 [Copper Resin Sheets](https://uk.rs-online.com/web/p/plain-copper-ink-resist-boards/4539035)

## Safety 

*	Do not attempt to override any safety measures in the machine, for example, to enable milling with the lid open.
*	When cleaning the machine, be careful of the sharp end of the milling bit against your hands.

You are much more likely to damage the machine (especially the fragile milling bits) than yourself:
*	Do not crash the milling bit into the bed or the workpiece.
*	Be careful when changing bits not to let them fall onto the workpiece.
*	Always use the correct spindle and travel speed for the material you are cutting
*	Ensure the bit is sufficiently held in the chuck 
*	Do not overdrive the machine when you are rushing to get a job done.
*	Never put a damaged bit back in the box for someone else to use – be aware that with small bits you will need to examine them under a microscope to see damage.

### Recommended bits

*	`PCB traces`: 0.4 mm square end mill
*	`PCB outline`: 0.8 or 1 mm square end mill

    Please ask a member of staff if you want to swap out a bit or believe a bit might be damaged.

## Operation compared to the MDX540

  The SRM 20 works in the same way as the MDX-540, with a few key differences:

*	There is no hardware panel; all operations must be carried out using VPanel and other software on the PC attached.
*	There is no Z Origin sensor, so the process for setting Z0 is different
*	The bed size is smaller, and so this machine is used for small jobs such as PCBs

## Setting up the Machine 

### Check the bit
*	Verify that the bit is the correct one, and inspect it for damage. 
*	You will likely need to remove it to do this. 
*	Use the USB microscope to inspect smaller bits (< 0.5mm dimeter)
*	When replacing the bit, use the small allen key and ovoid over-tightening the grub screw (the allen key will flex to help you avoid this)

### Position material

*	Make sure there is no dust on bed. Use a clean paintbrush and/or vacuum to clean the machine and bed
*	Use double sided tape to fix your material down

## Setting origin

Open up the VPanel software on the PC.

![vpanel.png](assets/vpanel.png)

*	Make sure both parts of VPanel are set on *`‘User Coordinate system’`*

### Setting X/Y Origin

*	Use X/Y controller in VPanel to set XY to front left corner of workpiece
*	You may need to move Z origin closer to piece
*	Under ‘Set Origin Point’ on the right, press X/Y to set X/Y Origin
*	Verify X/Y positions on left read 0 mm

### Setting Z Origin 

1.	Move the tool down to approx. 5 mm above workpiece
    +	Be careful – it’s very easy to crash the bit into the workpiece
    + Don’t use the ‘Continue’ mode while the bit is close to the workpiece, use the x100, then the x10 or x1 speed options

2.	Use the small, long-handled allen key to loosen the collet grub screw and let the tool gently drop onto the workpiece. 
    +	Do not do this from a large height
    +	Do not let too much of the tool extend from the bottom of the collet
    +	Retighten the grub screw – be careful not to over-tighten the grub screw

![warning_crashing.png](assets/warning_crashing.png)

3.	Under ‘set origin point’ press Z to set Z Origin
    +	Verify Z point reads 0 mm

4.	Close the lid

**`Remember to move the tool up off the workpiece before doing anything else, using the move control in VPanel.`**

### Sending Gcode files

Once you have a .NC file of the correct spec, you can send it directly with VPanel.
Press the Cut button in the toolbar to bring up the file selection dialog. 

![cut.png](assets/cut.png)
 
![warning_deleting.png](assets/warning_deleting.png)

#### `Note that the job will start as soon as you press Output – ensure the machine and bit are set up correctly.`

## Making .NC files 
We only officially support Eagle and Kicad for generating designs, if you use another piece of software you may be able to get it working but we can't guarantee that it will. 


We recommend using [Carbide copper](https://copper.carbide3d.com/) to convert GERBER files to gcode
Make sure you have the correct bits selected in each toolpath generation stage 

## After the job is done

*	Press the view button to move the bed forward
*	Use a clean paintbrush to clear out dust from the work area into the dust tray below
*	Use a scraper or flat headed screwdriver to gently remove the workpiece

## Member Induction sign-off form 

This is the form please follow this link [Plus X Workshop Induction — SRM-20](https://forms.gle/xALAGYEfsDvSz18U9)

#### *this must be completed before any further use of the machine*