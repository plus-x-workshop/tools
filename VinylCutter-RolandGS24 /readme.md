last reviewed 2022-08-24 by KI

# CAMM-1 GS-24 vinyl cutter readme

serial number: MEH7114

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

## External docs

* [User manual (PDF)](https://files.rolanddga.com/Files/GS-24_UsersManual/Responsive_HTML5/index.htm#t=GS-24_index.html)

## Contact information

* Email: techsupport@rolanddg.co.uk
* Number: +44 (0)1275 335540
* If a technical issue 'log a case' under 'Support' on their website before calling [website](www.rolanddg.co.uk)

* [website](https://www.rolanddg.eu/en-gb/products/vinyl-cutters/camm-1-gs-24-desktop-vinyl-cutter?utm_source=google&utm_medium=cpc&utm_campaign=Products_SEA_UK_DM_AQ&gclid=CjwKCAjwmJeYBhAwEiwAXlg0AT68OlGke0gjICLLSMdSMzxqN6jOd_IWdXbTLOPZygNx3OW9qd8MKRoCmskQAvD_BwE&gclsrc=aw.ds)

## Issues

* [Issues tagged with vinyl](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=vinyl&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|