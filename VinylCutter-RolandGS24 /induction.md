last reviewed 2021-10-09 by KI

# CAMM-1 GS-24 vinyl cutter Induction

## Purchasing Materials

[Vinyl website](https://www.mdpsupplies.co.uk/categories/sign-vinyl-supplies)

## Setting up the Vinyl in the machine

* Log into Fabman 
* Turn machine on and wait for the cutter to move to the far right edge
* Place your material in the machine and make sure it is vertically straight with the wheels 
* Place the rubber wheels on the edge of the vinyl, they must be inside the white markings on the machine above the wheels - this will be the widest part of your vinyl when measured. 
* Pull the leaver on the left-hand side to lock the wheels into place

## Measuring the Vinyl 

* Turn on the cutter, using the arrows on the machine select the correct type of size sheet you have `‘roll, sheet, *piece’` and press `‘entre’`
* The machine will now measure your piece of vinyl

## Inkscape

* Open file up in Inkscape or Illistrator
* Check your file is the correct size
`Go to File –> document properties and write down the size of your piece of your vinyl from the machine` 

**`Note: where your file is on the piece on inkscale is where it will be cut`**

## Printing

* If you are needing to move the cutter use the arrows on the machine to change the origin 
* `REMEMBER to hold down the ‘origin set’ button until it flashes`*
* Now click on File – Print – printer preferences and click `‘get dimensions from the machine’` + `Press ‘ok’` and then `‘print’`

**`Note: it will start straight away when you press 'print'`**

## Member Induction sign-off form 

This is the form please follow this link [Plus X Workshop Induction — Vinyl Cutter](https://forms.gle/wWvkjhMXGisYjB1i9)

#### *this must be completed before any further use of the machine*



