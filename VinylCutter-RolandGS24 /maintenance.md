last reviewed 2022-08-25 by KI

# CAMM-1 GS-24 vinyl cutter maintenance

[User Manual PDF](assets/https://files.rolanddga.com/Files/GS-24_UsersManual/Responsive_HTML5/index.htm#t=GS-24_index.html)

### Firmware Updates

n/a

### General Maintenance

* [Vinyl cutter mainenance section](https://files.rolanddga.com/Files/GS-24_UsersManual/Responsive_HTML5/index.htm#t=GS-24_USE_EN_10.html)

#### 3 Sections 
* [Cleaning](https://files.rolanddga.com/Files/GS-24_UsersManual/Responsive_HTML5/index.htm#t=GS-24_USE_EN_10_1.html)
* [Changing blade](https://files.rolanddga.com/Files/GS-24_UsersManual/Responsive_HTML5/index.htm#t=GS-24_USE_EN_10_2.html) 
* [Consumable items](https://files.rolanddga.com/Files/GS-24_UsersManual/Responsive_HTML5/index.htm#t=GS-24_USE_EN_10_3.html)

### Replacement parts

* [Replacing the Blade section](https://files.rolanddga.com/Files/GS-24_UsersManual/Responsive_HTML5/index.htm#t=GS-24_USE_EN_10_2.html)
    * [Step 4: Install the Blade Holder](https://files.rolanddga.com/Files/GS-24_UsersManual/Responsive_HTML5/index.htm#t=GS-24_USE_EN_04_4.html)
    * [Step 5: Perform a Cutting Test](https://files.rolanddga.com/Files/GS-24_UsersManual/Responsive_HTML5/index.htm#t=GS-24_USE_EN_04_5.html)

