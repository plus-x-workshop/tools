last reviewed 2022-08-25 by KI

# Z3 Z5 AML Testing machine maintenance

* [User manual (PDF)](assets/Z3-Z5-Z10-Z20-Z50-Z100-01-05-2017-Operation-Manual.pdf)

* [testing machine information](https://amlinstruments.co.uk/wp-content/uploads/2014/06/Z3-Z5-Z10-50-Tensile-Tester-Operation-Manual.pdf)

* [changing loading cell](https://plusxteam.sharepoint.com/sites/WorkShop/Shared%20Documents/Forms/AllItems.aspx?FolderCTID=0x012000A0B3AFFBAEAC154098C8772DD8B67B12&id=%2Fsites%2FWorkShop%2FShared%20Documents%2FMachines%2FMachine%20Videos%2FTesting%20Machine%20Video%2FPassword%20to%20change%20loading%20cell%2Emov&parent=%2Fsites%2FWorkShop%2FShared%20Documents%2FMachines%2FMachine%20Videos%2FTesting%20Machine%20Video)

* [testing machine - tensile video](https://plusxteam.sharepoint.com/sites/WorkShop/Shared%20Documents/Forms/AllItems.aspx?FolderCTID=0x012000A0B3AFFBAEAC154098C8772DD8B67B12&id=%2Fsites%2FWorkShop%2FShared%20Documents%2FMachines%2FMachine%20Videos%2FTesting%20Machine%20Video%2FMachine%20and%20Tensile%20Testing%20%2Emov&parent=%2Fsites%2FWorkShop%2FShared%20Documents%2FMachines%2FMachine%20Videos%2FTesting%20Machine%20Video)

* [testing machine - bending video](https://plusxteam.sharepoint.com/sites/WorkShop/Shared%20Documents/Forms/AllItems.aspx?FolderCTID=0x012000A0B3AFFBAEAC154098C8772DD8B67B12&id=%2Fsites%2FWorkShop%2FShared%20Documents%2FMachines%2FMachine%20Videos%2FTesting%20Machine%20Video%2FBending%20test%2Emov&parent=%2Fsites%2FWorkShop%2FShared%20Documents%2FMachines%2FMachine%20Videos%2FTesting%20Machine%20Video)

* [testing machine - compression video](https://plusxteam.sharepoint.com/sites/WorkShop/Shared%20Documents/Forms/AllItems.aspx?FolderCTID=0x012000A0B3AFFBAEAC154098C8772DD8B67B12&id=%2Fsites%2FWorkShop%2FShared%20Documents%2FMachines%2FMachine%20Videos%2FTesting%20Machine%20Video%2FCompression%20Test%2Emov&parent=%2Fsites%2FWorkShop%2FShared%20Documents%2FMachines%2FMachine%20Videos%2FTesting%20Machine%20Video)


### Firmware Updates

n/a

### General Maintenance

* Accessories, Maintenance
    * 1 USB cable
    * 1 110/220V cable
    * 2 pins 8 mm for adapter 1 certification of load cell
    * Grips see also http://www.grip.de

* Maintenance
    * Keep all parts clean. Work with vacuum cleaner not with compressed air.
    * Grease: We propose grease for bearings and ball screw Klüber ISOX NBU 15 ( all 300 hours of active usage - bal bearing has a greasing possibility in the centre of machine remove cap in center of protection sheet ) do not use oil

### Replacement parts

# Loading Cells 

## changing loading cell amount

- in main menu press the number 5 which should then ask for a password = 01922

- we have two loading cells 
    1. index 1 5000N
    2. index 2 500N

- nothing should be changed in the load cell information especially the 4999/ 499 limit as this will damage the load cell.

- using an allen key in the top and spin out the bolt and replace it over. 
- the black wire is the signal cable so make sure this is removed and then plugged back into the back of the machine once swapped over

![image of cell removing](replacing_load_cell)

