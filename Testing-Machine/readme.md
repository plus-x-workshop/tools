last reviewed 2022-08-23 by KI

# Z3 Z5 AML Testing machine readme

serial number: Z3 X500 

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

## External docs

* [User manual (PDF)](assets/Z3-Z5-Z10-Z20-Z50-Z100-01-05-2017-Operation-Manual.pdf)

## Contact information

* They have a great online messaging serivice [website](https://amlinstruments.co.uk/instruments/tensile-test-machine-universal-testing-machine/). 

AML:
* Alex Leeson
* Email: alex@amlinstruments.co.uk
* Telephone: 01522 789 375

Re-calibration: 
* Cost = £650 exc VAT
* emailed asking if we wanted one in October 2022 however I have said no. 
* They will call us regarding if we wanted one rather than sending out a yearly email.

## Issues

* [Issues tagged with testing-machine](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=testing-machine&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|