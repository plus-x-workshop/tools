# Z3 Z5 AML Tensile Testing Machine (and Die Cutter) Induction



<!--Meta-->

## Induction

Author: Katie Isard 
Last reviewed date: 2024-09-06  
Reviwed By: Andrew Sleigh

<!--Intro-->

## Description / intended use

- For determining Tension, Shear, Flexure and other mechanical and physical properties of materials. 
- For application of up to 5000N of force (with appropriate load cell)
- Die cutter: For cutting coupons or soft materials  for the tensile test machine

### Restrictions:
- Load cell limits (500 and 500N)
- **No compression or bend testing permitted on this machine.**
- Members must not install load cells, which are easily damaged by twisting forces (when screwing cell into machine) and machine must have maximum load limit reset every time.

## External documentation and links

## Risks


| Risk                                                         | Control                                                      |
|--------------------------------------------------------------|--------------------------------------------------------------|
| **Crushing**<br>Danger of crushing body parts between jaws or upper jaw and limit switches | Machine fitted with emergency stop button.<br>Users instructed to keep hands away from machine while in operation <br>Users instructed: Limit switches must be set to reduce the travel of the crosshead enabling a safe working area. |
| **Ejection of debris**<br>Material being tested shattering under pressure or tension and material hitting people | PPE: eye protection must be worn No compression testing to be carried out.     |
| **Electricity**   <br>Risk of injury due to faulty equipment, contact with live electrical components or improper use. | Visual checks to be carried out before use Included in regular inspection schedule by staff |
| **Fire**<br> Risk of injury caused by faulty electrical equipment    | Visual checks to be carried out before use Included in regular inspection schedule by staff |
| **Maintenance**<br>Unauthorised use: Crushing, electrical or ejected material damage | Staff to disable machine and install signage when under maintenance.   |

## Risks of damage to the machine

### The load cell can be damaged by overload 		

- Machine has load limit setting, which must be set by operator 	
- Ensure that you know which load cell is installed and that the force limit does not exceed the value of that load cell

### The load cell can be damaged by sideways pressure			

- No compression testing to be carried out.
- Do not attempt to change the load cell yourself, ask a member of staff.

### Damage to machine caused by colliding upper and lower jaws			

- Limit switches must be set to reduce the travel of the crosshead enabling a safe working area. 



<!-- Main section -->

## Safe Working Practices

### Before use

- Machine, including power supply, is in good condition with no visible damage
- Report unexpected behaviour to staff to investigate.

### In use

#### Cutting dies (using die cutter)

- Keep hands away from machine while in operation
- Ensure machine is stable, and pressure on handle will not cause it to topple over


#### Before loading material

Set limit switches to avoid:
- Unexpected travel of the head over long distances
- Head colliding with lower jaw

The limit switches are held in position by a locking screw. To set the limit switch, unlock it by turning the locking screw in the counter-clockwise direction and slide to the required new position. To lock the limit switch, rotate the locking screw in the clockwise direction. (Note: To eliminate slippage, re-tighten locking screw securely).

#### Before running tests

- Check correct load cell is installed
- Confirm settings are correct:
 - End position – relative to the current position. Positive values are up
 - Velocity
 - Force Limit


#### Power On/Off Switch 

The On (1) and Off (0) power switch is located on the back side
of the machine and should be in the Off (0) position before applying power to the machine.

-  The picture below indicates location of the power switch:

![on-off](assets/on-off.png)

#### Emergency Stop Push Button

The Emergency Stop push-button is located on the front panel of the machine.

* To activate the emergency stop you have to push the button. To deactivate the emergency
stop turn the button to the left and the button jumps out.
 
* The picture below indicates location of the **emergency stop push-button**: 

![emergency](assets/emergency.jpeg)

#### Mechanical Limit Switches

* *Mechanical Limit Switches* Before attempting to move the crosshead in either direction it will be required to set the mechanical crosshead limit switches. 

* The purpose of the limit switches is to:
    - reduce the travel of the crosshead enabling a safe working area
    - protect your hand from being crushed
    - protect your work from being damaged

* A typical example is to protect the load cell and attachments for unforseen overtravel and in doing so avoid a collision. These limit switches are located on the left hand side of the machine.

The diagram below shows the location of the mechanical crosshead limit switches: 

![lock](assets/lock.png)

* The limit switches are held in position by a locking screw. 

* To set the limit switch unlock by turning the locking screw in an anti-clockwise direction and slide to the required new position. 

* To lock limit switch rotate locking screw in a clockwise direction. (Note: To
eliminate slippage re-tighten locking screw securely). 

#### CONTROL DISPLAY UNIT

* *Control Display Panel* This unit contains the LCD Display, the Numeric Keypad and the
Emergency Stop push-button.

![screen-buttons](assets/screen-buttons.jpeg)

#### Numeric Keypad

* Numeric Keypad These keys are normally used for entering numeric values on the
LCD-Display (Speeds, Extension, etc.)

![buttons](assets/buttons.jpeg)

* The **down arrow** is used for lowering the crosshead and for inputting negative values. 
* The **Up arrow** is used for raising the crosshead. 
* The **Menu Key** with this key the operator can make settings for the next test. The
adjusted settings should be confirmed with the Return key.
* By pressing the **Return** key the operator stops or returns the
crosshead to zero extension after the test. 
* The **0 key** This key will zero the Force and Extension display. 
* The **1 key** This key shows the maximum Force and the referring Extension of the last test.
* The **2 key** This key shows the Force and the Extension of the Rupture. 
* The **5 key** This key is used to enter the configuration menu. This menu is protected with the password (09122). 
* The **8 key** This key allows switching to constant force mode. 
* All other keys have no fuction

#### Loading Cells 

- if you would like to changing loading cell amount please speak to a member of staff:

- We have two different loading cells for different needs: 

    - index 1 = 5000N
    - index 2 = 500N

![tensile](assets/tensile.jpeg)

#### Computer

- password = tensile
- user name = tensile 

- plug the machine using hte USB cable into the computer 

- the software is automatically set up for tensile testing but if you go to the top of the page you can see 'method' and choose from there. 
- we have:
    1. tensile 
    2. compression 
    3. bending 

![image of mode screen](assets/mode_screen.png)

#### Tensile Testing Process

Steps:
- sample dimentions
- set speed
- set displacement 
- set maximum load 
- set breaking force 

![image of mode method](assets/mode_method.png)

#### sample description

- choose the correct sample size 

    - square sample, tube sample etc (click on the sample image to get up the correct shape you need)
    ![sample_description](assets/sample_description.png)
    - choose limits of length/ diamiter, width etc
    - using an accurate tool such as calipers or micromiter (we have one of these in the store room, please ask a member of staff for this), **do not use a ruler this need to be as accurate as possible**

![image of sample size layout](assets/sample_size_layout.png)

#### settings

- chart is set up for load and displacement 
    - *displacement* is the travel of the crosshead
    - *load* is the load cell

![settings_tensile](assets/settings_tensile.jpeg)

#### testing speeds 

- certain standards will tell you what speed you should be going at by looking it up or you can se your own standards:

- **maximum displacement** = how much the crosshead moves up
    - eg. you might not want to break something but stretch it over a distance 
- **maximum load** = how much force the machine will get to before it stops
    - eg. only want the load cell to go to 100N then it will stop when it reaches that chosen number
- **breaking force** = how long the machine will go to until it stops the test 
    - eg. 10N means that if it feels a drop of 10N it will stop

- then click on update to make these changes

![mode_update](assets/mode_update.png)

**BEFORE DOING ANY TEST MAKE SURE THE NEWTON'S AND MM's ARE SET TO 0** 

- hold down the 0 to get to 0.0N and 0.00 mm 
- this will also change on the computer 

*the cross head will also always return to the starting position for the next test*

#### material 

- place material in the clamps ready to go, making sure that if you are doing multipul tests they are going to be clamped in the same way and force. 

#### Chart

- you then need to go the chart section and press play
- check that your on 0.0N at the begging which you can see to the right of the chart.

![chart](assets/chart_00.png)

![chart](assets/chart.png)

![mode_play](assets/mode_play.png)

- this will bring up a new window and allows you to add "Part No" so that you can keep trac of how many samples you have done. 
- you shuld not be changing anything else on this screen 
- just double check the dimentions are correct
- once you have checked everything you can hit ok 
- the test will begin **STRAIGHT AWAY**

![part_number](assets/part_number.png)




### After use

Tidy up and put away all accessories

### Staff only: maintenance and cleaning

Disable machine to ensure no unauthorised use

## PPE Requirements
- Eye goggles must be worn

## Sign-off

[Plus X Workshop Induction — Testing Machine](https://forms.gle/s2kFaKk1qWhSJdfJA)

This must be signed before any use of the machine



