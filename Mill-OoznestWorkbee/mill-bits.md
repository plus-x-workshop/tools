**This is a list of the currently available mill bits that work with the Ooznest Workbee.**


[Here](https://a360.co/3wSbLvT) is a Fusion360 tool library of the below tools.


### Manufacturer: Ooznest

| Type | Cut Diameter (D) | Shank Diameter (d)| Cut Length (L) | Bit Length (BL) | Reach (R) | Flutes Angle (A) |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| Square | 1/4″ (6.35mm) | 1/4″ (6.35mm) | 1/2″ (12.7mm) | 5/2″ (63.5mm) | na | 2 | na |
| Square | 1/8″ (3.175mm) | 1/8″ (3.175mm) | 0.375″ (9.525mm) | 3/2″ (38.1mm) | na | 2 | na |
| Square | 1/16″ (1.5875mm) | 1/8″ (3.175mm) | 0.188″ (4.7752mm) | 2.0″ (50.8mm) | 0.590″ (14.986mm) | 2 | na |
| Ball | 1/4″ (6.35mm) | 1/4″ (6.35mm) | 3/4″ (19.05mm) | 5/2″ (63.5mm) | na | 2 | na |
| Ball | 1/8″ (3.175mm) | 1/8″ (3.175mm) | 0.375″ (9.525mm) | 3/2″ (38.1mm) | na | 2 | na |
| Ball | 1/16″ (1.5875mm) | 1/8″ (3.175mm) | 0.188″ (4.7752mm) | 2.0″ (50.8mm) | 0.590″ (14.986mm)	2 | na |
| Engraver | 0.05″ (1.27mm) | 1/8″ (3.175mm) | na | 3/2″ (38.1mm) | na | 2 | 90 |

### Manufacturer: Kyocera

| Type | Cut Diameter (D) | Shank Diameter (d)| Cut Length (L) | Bit Length (BL) | Reach (R) | Flutes Angle (A) |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| Square | 1/8″ (3.175mm) | 1/8″ (3.175mm) | 0.375″ (9.525mm) | 3/2″ (38.1mm) | na | 2 | na |
| Square | 1/16″ (1.5875mm) | 1/8″ (3.175mm) | 0.188″ (4.7752mm) | 2.0″ (50.8mm) | 0.590″ (14.986mm) | 2 | na |
| Ball | 1/4″ (6.35mm) | 1/4″ (6.35mm) | 3/4″ (19.05mm) | 5/2″ (63.5mm) | na | 2 | na |
| Ball | 1/8″ (3.175mm) | 1/8″ (3.175mm) | 0.375″ (9.525mm) | 3/2″ (38.1mm) | na | 2 | na |
| Ball | 1/16″ (1.5875mm) | 1/8″ (3.175mm) | 0.188″ (4.7752mm) | 2.0″ (50.8mm) | 0.590″ (14.986mm)	2 | na |
| Engraver | 0.05″ (1.27mm) | 1/8″ (3.175mm) | na | 3/2″ (38.1mm) | na | 2 | 90 |

