Last reviewed 2021-10-09 by AS

# Ooznest Workbee flat bed CNC Induction

Model Name : Ooznest WorkBee v2.2.1

### Workbee Local

[http://workbee.local](http://workbee.local/)

### Fusion Post Processer installation 

[How to install a cloud post processor in Fusion 360](https://knowledge.autodesk.com/support/fusion-360/learn-explore/caas/sfdcarticles/sfdcarticles/How-to-install-a-cloud-post-in-Fusion-360.html)

[Download Workbee Firmware](https://learn.ooznest.co.uk/Wiki/WorkBee_Firmware_Releases)


## Safety
* The `router and the Workbee are powered seperately`. They can be powered on or off independently.
* The `Emergancy Stop Button is located towards the bottom left hand side of the work bench`. Pressing this button will turn off the whole system, shuting off the router as well as the WorkBee. **important** Because the router has its own on/off switch on the router body it will immediately power back on when the emergency stop button is reset.
* Suggested workflow for dealing with emergencies: Hit Emergency Stop Button - Open Doors And Turn off Router - Deal With The Issue - Reset Emergency Stop By Pulling Out Until Yellow Ring Is Visible.
* When you are not cutting material, make sure that the router is switched off
* *`Under no circumstances must you open the protective case whilst the machine is cutting`*. If something has gone wrong, press the emergancy button, wait for the WorkBee and router to fully power down and then address the issue
* `Room extraction` must be on while cutting
* **PPE: Steel toe caps, safety glasses** (and ear defenders whilst operating the machine, and extraction is on)
* Use of a `damaged milling bit`, or aggressive feedrate can cause the tool to shatter, which may result in sharp metal pieces flying unpredicatbly at high speed. Use the tool cautiously. If in doubt, cut at a shallower depth, with a slower feedrate.
* Always ensure the router is turned on and at the top speed before commencing a cut, or setting the Z origin.

## Materials

With the appropriate feeds and spindle speeds, you should be able to cut most materials on the WorkBee e.g.

 * MDF 
 * Plywood 
 * Valchromat 
 * Aluminium 
 * Plastic     

If you are unsure that you material can be cut on the WorkBee, gather a material specification sheet form the supplier and then ask a member of the workshop team for validation first. 

The workshop team can advise on suitable feedrates and spindle speeds for most common materials. We can also advise on the appropriate milling bits to use.


## Tools 
There are a range of tools you can use to cut material with the WorkBee. All milling bits will specify the material it is made out of which could be, e.g. High Speed Steel (HSS) or Carbidee. The shape of the milling bit must be considered for each tool path you are doing such as: 

* Sqaure End Mills
* Ballnose Mills 
* V Bit Mills
* Radius Mills

Whenever you buy milling bits, the dimensions of each bit will be listed on its box. Some of them look very similar so only handle one at a time. Be careful how you handle it and if you're not using a milling bit, put it back in its box. In many cases, the dimensions will be in imperial units so covnert this into metric units if needed (this is particularly important if you CAD / CAM software is setup to only take measurements in mm) 

## Changing a Milling Bit

### `Make sure the router and WorkBee are off before attempting to change the milling bit.`

* Be careful of how you handle them as **`dropping it can cause significant damage to milling bit`**. 
* The `black button` on the side of the router to lock the collet holder in place. Using a a spanner (or an adjustable spanner), remove the hex bolt which will also remove the collet. 
* Slide your milling bit firstly up into the collet about half way up the shank and then re-screw this back into the router using the hex bolt and spanner
* With your hands turn the router to breifly check that the milling bit has been instered correctly and that it's not wobbling from side to side.

## Key Terminology

### Workbee and Router 
- Feed Rate: How quicky the WorkBee moves on its XYZ axis 
- Spindle Speed / Speed Rate: How fast the milling bit spins
- Chuck: A metal piece within the router and tightend by a bolt which grips onto the milling bit

### Milling Bits
- Flute: The cutting length of the milling bit 
- Shank: The smooth portion of the milling bit which slides inside the chuck
- Diamater: The maximum width of the bit that it will be able to cut 

### Cutting Motion 
- Toolpath: These are the paths defined on the CAM software that the WorkBee will take
- Stepdown: The amount of travel the router will move downwards in the Z direction between each path. An extra path may be required to cut thorugh the stock
- Stepover: The amount of travel the router will move sideways in the X and Y direction between each path
- Clearance Height: The amount you must raise the router bit above the workpiece when making travel moves in order to clear any clamps

### CAM Modelling  
- Dog Tags / Holding Tags: Tags are small pieces of material that are 'skipped' by the tool path so that there is something holding your design to the stock. This makes sure it dosen't move as the router is cutting through the stock
- Dog Bones: Because the mill bits are ciruclar, they cannot cut perfect 90 degree corners.  So 'dog bones' have to be designed into the toolpath to cut extra material out at the corners.
- Stock dimensions:  the dimensions of the material you are about to cut on the WorkBee

## Ooznest Control Board 
The Workbee control board is accesible in a web browser through [http://workbee.local](http://workbee.local/). This link will only work when the Fabman for WorkBee is activated and the machine is on.

**`You must only operate the control software when you are in front of the machine, paying attention to the movewments you are controlling.`**

* Like any CNC machine, the WorkBee moves on an XYZ basis. 
* Before using the machine, `you must home the X and Y axis`. This moves the router to the top right corner so it knows where it is located in relation to the spoilboard.

* You can then use the machine movement steps to position the router to the correct location for your origin (X and Y 0). Always *`use smaller increments first`*, then confirm the direction you are moving in and use larger steps if needed. 

**`Note that you should never use a large increament step when one of the axes is close to its limits, otherwise it will run out of track and crash`**

## Setting Origin 

* The exact placment of the X, Y & Z origins will be determined in conjunction with the design of the part and the tool pathing. As an example, some will set the Z at the top of the spoil board while others will set it at the top of the stock. 
* Set each axis's origin by selecting "SET X, SET Y, SET Z". 
* When setting the origin for the *`Z axis`*, make sure the *`router is turned on`* so it can cut through the material in case you step it down too much. However, you want it to be slighlty touching the spoilboard so it can comfortably cut through your material.
* When you are ready, upload your g-code by selecting the `UPLOAD & START` button. Be aware that as soon as you have this uploaded your job it will start cutting to 

## Check list Before the Job

* Ensure that the `doors` for the enclosure are closed
* Remove any `unnecessary` `material` or `clamps` on the spoilboard
* Add the `hose cover` to the end of the router for extraction 
* Selected the `approprite speed`
* `Turn on the router` 
* Turn on the `room extraction`

## During the Job
Depending on how big your job is, the WorkBee will take some time

* Keep your safety goggles and ear defenders on
* Be present by the machine and observe all motion
* Be ready to use the emergency stop at all times.

## After Job 
Once your is complete, go through this checklist to make sure the WorkBee is set up for the next user 

* Turn off the router 
* Use the cprrect festool vacuum cleaner (metal or wood) to vacuum up any swarf or dust left over from your job
* Clean up any mess around the enclosure as well.
* Remove any clamps and material you have screwed into the spoilboard 
* Make sure you have fully logged out and closed the Ooznest Control Board as you can still operate the WorkBee when the window is open

### `REMEMBER TO ALWAYS TURN OFF THE ROUTER MANUALLY BEFORE LEAVING THE MACHINE`

## Induction Task

Supervised by a member of the workshop team, cut a sample piece using the files provided:

 * CAD file [Link TBC]
 * CAM file (G-Code) [Link TBC]

## Key Resources 
There are plenty of resoruces out there that can help your with how you can use the WorkBee.

Useful Software:
* Fusion 360 (CAD and CAM features)
* FreeCAD (a free software and has CAD capabilites, but better used for CAM features for creating toolpaths)
* V-Carve (licensed software, but has good CAM features)
* Solidworks (great CAD and CAM capabilities)

Each software may ask for different file formats in order to make tool paths. In most cases, they will require a STEP file

## Helpful Videos

[Fusion 360 Digital Fabrication for ](https://www.youtube.com/watch?v=JOrn4BejMq8)

[FreeCAD - Basic How to Use Tutorial](https://www.youtube.com/watch?v=MWFC17MIfOE&t)

[Ooznest Guide on How to Use V-Carve to Setup a Job](https://learn.ooznest.co.uk/Guide/How+To+Set+up+a+Job+on+the+WorkBee+CNC+Machine/107)

# Member Induction sign-off

This is the form please follow this link [Plus X Workshop Induction — Ooznest Workbee](https://docs.google.com/forms/d/e/1FAIpQLSf450NAIk-wgMA6j3AbHXjzfC6iGLAhzkYgiLT91yMLy1_-iA/viewform?usp=sf_link)

#### *this must be completed before any further use of the machine*
