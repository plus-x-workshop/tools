# Toolpaths Induction 

A walkthrough guide on how safely create toolpaths for the workbee using Fusion 360

## 1) Creating a CAD File 

Using the desgin features of fusion, create you CAD files according to your design, specifications and requirements. It's important to continuously save your work and once you have finished designing, you'll be ready to enter the computer aided manufactuer functions of fusion 

## 2) Entering the CAM Workspace

To enter the CAM workspace, select the drop down menu underneath the top toolbar which should sya 'DESIGN'. Click the drop down and select 'MANUFACTURE' to enter the manufacturing work space. Note the the workspace will look enteirley different and you will no longer be able to edit the model of the design 


## 3) CAM Setup

a) Using the Fusion Libary, clicked the folder icon on the top toolbar and add the 'Autodesk Generic 3 Axis Machine' as a machine and change the setup operation mode to milling. As for the WCS, leave this as it's default setting or change as required by selecting the small grey spheres to change the origin which is called 'Stock point'


b) The Stock tab is where you can edit the size of the stock you are cutting out of. Using the drop down, select the appropriate 'Mode' and edit the width, depth and height. It is also imporant to understand the orientation of the stock relative to the workbee and CAM software (the XYZ axis). So when you are entering the value for the width, make sure you are editting the X value and not the depth (Y) value


## 4) Creating Tool Paths for Milling 
_when making toolpaths, make sure you disbale the coolant as the workbee dosen't use a coolant whilst cutting_

**(i) Facing**

The facing tool path will smoothen out the top face of the piece / stock you are working on

**Tool Tab** - Select the tool you would like to use to face the stock. For facing, it's recommened that you use a larger sqaure end mill

**Passes Tab** - To make sure the workbee dosen't remove too much material, go to the passes tab and select multiple depths. Set the maximum step down to an appropriate level and confirm  

------------------------------------------------------------------------------------------------------------------------------------------------

**(ii)Pocket** : Creates the cavity / cavities into the piece you are cutting

First, make sure the milling tool bar has been selected and go across the the '2D' drop down and select '2D Pocket'

**Tool Tab** - For each operation, pick the correct tool from the tool library. At this point, you may need to add tools directly which can be done from the top right of the window. Once you click ok, you'll see the tool apear by the part you want to cut. Also make sure that the multiple depths option is ticked as this will determine how far deep each pass will go into the material 

**Geomertry Tab** - Set the Machine Boundary drop down to 'selection' and start to select the areas you wish for the operation to perfomrm in. If it is in multiple areas, then select them all at once and click ok

_Tip : If you right click where you've created the toolpath under the creation history, you can simulate the motion of the cutter to make sure it's performing the correct task_

------------------------------------------------------------------------------------------------------------------------------------------------

**(iii)Contour** : Cuts around the shape of your design part

Select the '2D' drop down and click '2D Contour'. Again on the tool tab, select which milling bit you want to use and disable the tool coolant option.

For the Geomerty, use the contour selection to select the bottom edge of the part you are cutting

**IMPORTANT** - Tabs are used to make sure that as you are cutting your part out of the material, it dosn't come out / become misalinged dutring the process. They leave behind tabs that attach the part to the stock which means they'll need to be manually cut out later. Make sure you tick the tickbox and adjust the settings to change the size, shape and positioning of the tabs

**Passes Tab** - Most material will be to thick to cut in one pass so go to the 'passes tab' and tick 'multiple depths'. The maximum roughing stepdown is the value that determines how far deep the cutter goes into the material on each pass

------------------------------------------------------------------------------------------------------------------------------------------------

## 5) Running the Toolpath Simulation 

The final check would be to make sure the toolpaths are run correctly. You can do this by selecting all operations on the setup tree OR selecting 'simulate' on the top tool bar. Use the play contorls and the timeline at the bottom of the screen to control the simulation video.

On the right hand side, a new window will apear to help you with the viewing settings of the simulation 

**Display** - if requried, use the drop down (once you have checked the tool tickbox) so that you cna view the just the tool, the chuck or both during the simulation. You can also use the 'toolpath' option to view the travels of the tool. Adjust the mode so that you can see the travels of the tool

Lastly enable the stock so you can visually see your piece of mateiral being cut by the workbee and check 'stop on collision' so you can see if and where there is a collision with your clamps or something else in the job.

------------------------------------------------------------------------------------------------------------------------------------------------

## 6) Exporting the g-code 

Once you have simulated the tool path and you are happy with everything, you can then export the toolpath so that it is ready for cutting on the Workbee.

Right click on the toolpath tree and select post process. Save the file to an easy to find location and you'll be ready to go (Fusion360 will automatically download Microsoft Visual Stuido is your PC dosen't have it so that you can easily view your g-code)

------------------------------------------------------------------------------------------------------------------------------------------------

## Additional Step

There is a 'software handshake' that is required between Fusion 360 and the CNC Workbee control board. To do this, download [this](https://learn.ooznest.co.uk/Wiki/WorkBee_Firmware_Releases#Section_Fusion360) file and follow [these](https://knowledge.autodesk.com/support/fusion-360/learn-explore/caas/sfdcarticles/sfdcarticles/How-to-install-a-cloud-post-in-Fusion-360.html) steps to add it to Fusion 360.
