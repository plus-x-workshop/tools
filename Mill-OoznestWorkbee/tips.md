last reviewed YYYY-MM-DD by INITIALS

# Tips and tricks for using the Workbeee CNC

## Links to useful resources



## Types of cut

From: https://www.autodesk.com/products/fusion-360/blog/10-2d-cnc-milling-toolpaths/

| Toolpath	| Uses |
| ------ | ------ |
|Face	|Finishing the face of a part.|
|Contour	|Machining loops, open pockets, stick fonts, dovetails, keysets, or saw cuts. |
|Chamfer|	Deburring and creating chamfers using either a tapered mill or center drill.|
|Fillet	|Creating fillets using a Corner Rounding Tool.|
|Pocket	|Roughing or finishing pockets of various shapes and sizes.|
|Slot Mill|	Machining straight slots or arc slots. |
|Drill|	Creating spot drill, tapped, bore, or reamed holes. |
|Bore|	Making holes, typically greater than .75in diameter.|
|Thread Mill 	|Machining ID threads over .75in diameter, milled OD threads of any size, or custom threads.|

