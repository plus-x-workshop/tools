last reviewed 2022-08-23 by KI

# Ooznest Workbee flat bed CNC readme

serial number: n/a

## Our docs:

* [Induction Document](induction.md)

* [Maintenance guide](maintenance.md)

* [Toolpaths Induction Document](toolpaths.md)

* [Best practice guides, tips etc.](tips.md) 

* [Milling bit sizes](mill-bits.md) 

## External docs

* [User manual (PDF)](assets/WorkBee-Full-Kit-Assembly-Manual.pdf)

## Contact information

* [Workbee website](https://ooznest.co.uk/product/original-workbee-z1plus-cnc-machine/#size-colour)

## Issues

* [Issues tagged with Workbee](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=Workbee&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|