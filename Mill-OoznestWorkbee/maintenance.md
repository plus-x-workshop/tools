Last reviewed 2022-18-08 by KI

# Ooznest Workbee flat bed CNC maintenance

* [Ooznest learning portal](https://learn.ooznest.co.uk/c/Root)

* [youtube videos on the workbee](https://www.youtube.com/c/ooznest/videos)

* [full kit assembily instructions](assets/WorkBee-Full-Kit-Assembly-Manual.pdf)

### Firmware Updates

n/a

## General Maintenance
* Hoover all areas
* check all wires are tucked away and can't get court 
* check all the nuts and bolts are tight on the z-axis - nothing is moving 

* check all milling bits are safe and none are missing

## Surfacing the bed

* This can be done manually or can use the [surfacing nc. file](assets/surfacing-workbee-v8.nc)

## Repairs 

* [replacement parts](https://ooznest.co.uk/product-category/workbee-cnc-machine/workbee-replacement-parts/)
