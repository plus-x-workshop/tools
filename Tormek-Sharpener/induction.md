# Tormek Sharpening wheel induction

<!--Meta-->

## Tormek T-4 sharpening station

Induction by Andrew Sleigh
Last reviewed date: 2024-09-03

<!--Intro-->

## Description / intended use

Water-cooled sharpening system for chisels, planes and knives. 

## External documentation and links

* Tormek website: [Tormek T-4 Original](https://tormek.com/en/products/sharpening-machines/tormek-t-4-original)
* Getting started video: [Tormek T-4 sharpening system: Getting Started with Alan Holtham](https://www.youtube.com/watch?v=P1qTcD1kKis)

Documents:
* [HB-10 Tormek Handbook](assets/hb-10-en-v105.pdf)
* [T4 Breakdown / parts list](assets/TOR-T4_breakdown.pdf)
* [Square Edge Jig SE-77 manual](assets/TOR-SE-77.pdf)


## Risks

| **How could people be harmed?**<br>                          | **Control measures**<br>                                     |
|--------------------------------------------------------------|--------------------------------------------------------------|
| Entrapment in moving parts                                   | Wheel turns at slow speed.<br>Use a jig where available to hold tools |
| COSHH: Inhalation, skin or eye contact with grinding compound or particles from tools | The grinding paste is not classified as dangerous according to regulation (EG) 1272/2008 (CLP). <br><br>Eye protection and nitrile gloves to be worn |
| Electrical shock: Water in use near electrical appliance     | Machine to be plugged into socket located above tool<br><br>Water to be discarded immediately after use<br><br>Tool put away in locked staff store cupboard when not in use. |
| Cuts from tools being sharpened                              | Use safety stops on jig to stop the tool sliding off the wheel.<br>Care to be taken |

<!-- Main section -->

## Safe Working Practices

### Before use

* Ensure machine is not damaged. Report any suspected damage to workshop staff.
* Position the machine on a stable surface pointing in the orientation appropriate for the task (typically with the water bath on the right side, so wheels turn away from you on the top edge)
* **For honing, the wheel must always turn  away from you (leather wheel on left) otherwise the tool will get caught in the leather.**
* Use the handle to move the machine, not the polished metal jig mount.

![](assets/wheel-direction.jpeg)

### In use

#### Power
* Plug the machine into a power socket located above the machine. 
* Ensure no water from the machine can come into contact with the socket.

#### Water
* Position the tray in the upper position so the grading wheel is partly submerged in the water
* Start the machine up
* Fill the water tray with cold tap water as the wheel turns
* Refill the tray as water is soaked up into the wheel. You may need 0.5-0.75l water

#### Grinding wheel prep
* Run the coarse side of the stone grader against the wheel for about 20 seconds to ensure the wheel surface is flat and clean


#### Jig setup
* Use the correct jig for the tool. Typically the SE-77 chisel/plane jig.
* Slide the chisel into the jig mounting the tool to the square right side. Make sure the tool is square in the jig
* Tighten the jig down on the tool
* Slide the jig onto the support rod
* Use the safety stops on the support rod to stop the tool sliding off the wheel
* Find the angle using the plastic angle tool (WM-200) and then set using the height stop

#### Grinding a chisel

* For grinding the support rod should be mounted vertically above the station
* Start the wheel, ensure it is turning away from you and move the chisel from side to side across the wheel applying even pressure throughout
* If necessary, re-surface the stone to get a finer 1000-grit setting and re-grind
* **Ensure you do not splash water on the machine case** 

![](grinding.jpeg)

![](assets/surfacing.jpeg)

#### Polishing a chisel
* For polishing, or removing a burr, the support rod should be mounted horizontally in front of the station
* **For honing, the wheel must always turn away from you (leather wheel on left) otherwise the tool will get caught in the leather.**

![](assets/honing.jpeg)

Consult the SE-77 manual for detailed chisel/plane sharpening instructions. Or watch the video here: [Sharpen chisels with the Tormek Square Edge Jig SE-76](https://youtu.be/KZ07gPtB6pg?si=bFRKzJFwgkieuYCd)



### After use

* **Important:** empty the water tray and position the tray on the station in the lower position. Do not leave the wheel in the water.
* Slowly pour off the water from the top of the tray down the drain, and then scoop out the metal and dust particles and put in the bin
* Ensure the tool is clean, dry and all jigs and consumables put away
* Report any damage to workshop staff.

### Staff only: maintenance and cleaning

* No special considerations
* Check wheel and casing for damage. 

## PPE Requirements

* Nitrile gloves if handling honing paste or machine oil
* Eye protection
* Blue towel to clean up any water splashes or spills

## Sign-off

Sign the induction form before any use of the machine: 
https://forms.gle/Y9XEouABoj767K2r7
