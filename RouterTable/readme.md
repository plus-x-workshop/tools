Last reviewed 2023-05-22 by AS

# Router Table readme

The router table consists of:

- JessEm Mast-R-II Router Table
  - JessEm Mast-R-Lift II (Metric)
  - JessEm Mast-R-Top Phenolic Router Table Top (Metric)
  - JessEm Mast-R-Fence II (Metric)
- AUKTools 2400W Fixed Base Router Remote Speed Control and NVR with £ 316.63 ER20 Collet
- INCRA I-Box Jig for Box Joints
- AUKTools Workshop Vacuum Hose - 57mm x 3m
- Pushsticks and blocks

<!-- For groups of tools, or rooms/areas, you may need to adjust this template -->

## Our docs:

<!-- Link to files in this directory -->

* [Induction document](induction.md)

<!-- * [Standard Operating Procedure](sop.md)

* Best practice guides, tips etc. -->

## Manuals

- [Lift](assets/02120_-_MAST-R-LIFT_II_-_ENGLISH_AND_FRENCH.pdf)
- [Top](assets/03006_Mast-R-Top.pdf)
- [Fence](assets/04010_Master-R-Fence-Rout-R-Fence_Manual.pdf)
- [Stand](assets/05005_-_Stand_Manual.pdf)
- [Router](assets/AUKTools-Router_FBR24ER_Manual_AW.pdf)


## Contact information

- Retailer: https://woodworkersworkshop.co.uk/jessem-mast-r-ii-router-table-package/
- Manufacturer: https://jessem.com/pages/product-manuals

## Issues

<!-- All machine issues should be tagged with this machine name -->

<!-- Edit this link appropriately -->

* [Issues tagged with router-table](https://gitlab.com/plus-x-workshop/tools/-/issues/?label_name%5B%5D=router-table)
<!-- 
## Services -->

<!-- All machine issues should be tagged with this machine name -->

<!-- Edit this link appropriately -->

<!-- | Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)| -->


## Maintenance

Bought August 2022


## Inspections

- Check table is rigid
- Check router guard is installed and undamaged
- Check push sticks/blocks are available and undamaged
- Check for damage to electrical cord or router housing
- Run router and check it sounds OK
- Verify emergency stop works
- Clean dust and debris from vents, using compressed air if available
- If the supplied replacement brushes need fitting, please have them fitted by a qualified electrician or power tool repairer.
  


