Last reviewed 2022-08-23 by KI

# Prusa	i3 MK3S+ Printer 1 FDM 3D Printer maintenance

[Link for Printer Maintenance](https://help.prusa3d.com/en/category/printer-maintenance_247)

### Prusa Manual

* [Prusa Assembily Manual](https://help.prusa3d.com/en/guide/5-e-axis-assembly_28536?lang=en#_ga=2.190063983.516785959.1604922347-99564966.1603460532)
 
### Firmware Updates
* [Firmware updating (MK3S+/MK3S/MK3)](https://help.prusa3d.com/en/article/firmware-updating-mk3s-mk3s-mk3_2227)

### General Maintenance
* [Maintenance tips](https://help.prusa3d.com/en/guide/maintenance-tips_23200)
* [i3 Printers Regular Maintenance](https://help.prusa3d.com/en/article/i3-printers-regular-maintenance_2072)
* [Adjusting belt tension MK3/S](https://help.prusa3d.com/en/article/adjusting-belt-tension-mk3-s_112380)

* check the computer and SD cards and remove any files from members that have been saved

### Safety
* [Testing safety features (MK3/MK3S)](https://help.prusa3d.com/en/article/testing-safety-features-mk3-mk3s_2079) 

### Filament/ Nozzle
* [Cold pull (MK3S/MK2.5S)](https://help.prusa3d.com/en/article/cold-pull-mk3s-mk2-5s_2075)
* [Removing filament from extruder manually](https://help.prusa3d.com/en/article/removing-filament-from-extruder-manually_121491) 
* [Checking/re-aligning the Bondtech gear (MK3S/MK2.5S)](https://help.prusa3d.com/en/article/checking-re-aligning-the-bondtech-gear-mk3s-mk2-5s_133176)
* [Changing or replacing the nozzle (MK2.5S/MK3S/MK3S+)](https://help.prusa3d.com/en/article/changing-or-replacing-the-nozzle-mk2-5s-mk3s-mk3s_2069)

### How to Replace... 
* [How to replace a hotend thermistor (MK3S/MK3S+)](https://help.prusa3d.com/en/guide/how-to-replace-a-hotend-thermistor-mk3s-mk3s_131675)
* [How to replace a hotend (MK3S/MK3S+)](https://help.prusa3d.com/en/guide/how-to-replace-a-hotend-mk3s-mk3s_161575)
* [How to replace a heatbed thermistor (MK3S+/MK3S/MK2.5S/MK2S)](https://help.prusa3d.com/en/guide/how-to-replace-a-heatbed-thermistor-mk3s-mk3s-mk2-5s-mk2s_19272)
* [How to replace a heatbreak/heatsink/heaterblock (MK3S+/MK3S/MK2.5S/MMU2S)](https://help.prusa3d.com/en/guide/how-to-replace-a-heatbreak-heatsink-heaterblock-mk3s-mk3s-mk2-5s-mmu2s_16104)
* [How to replace an IR-sensor (MK3S/MK3S+)](https://help.prusa3d.com/en/guide/how-to-replace-an-ir-sensor-mk3s-mk3s_162262)
* [How to replace a print fan (MK3S/MK3S+)](https://help.prusa3d.com/en/guide/how-to-replace-a-print-fan-mk3s-mk3s_160876)
* [How to replace SuperPINDA (MK3S/MK3S+)](https://help.prusa3d.com/en/guide/how-to-replace-superpinda-mk3s-mk3s_179920)
* [How to replace a hotend PTFE tube (MK3S+/MK3S/MK2.5S/MMU2S)](https://help.prusa3d.com/guide/how-to-replace-a-hotend-ptfe-tube-mk3s-mk3s-mk2-5s-mmu2s_21664)
* [How to replace a hotend heater (MK3S/MK3S+)](https://help.prusa3d.com/guide/how-to-replace-a-hotend-heater-mk3s-mk3s_221422)

### If a blob is stuck 
* [Video on how to get rid of the blob](https://www.youtube.com/watch?v=mSmJKjibMT0&ab_channel=Prusa3DbyJosefPrusa)

### g-codes
* ![Fan strut](assets/fan_shroud_mk3s_adapted_mk3s_0.2mm_PETG_MK3S_36m.gcode.zip)

### Prusa parts
* ![printables website](https://www.printables.com/model/88272-i3-mk3s-printable-parts/files)

**Settings**

If you decide to generate your G-codes, it is recommended to use:

**Material**: PETG
**layer height**: 0.2 mm QUALITY preset
**Infill**: 20% GRID

The 'Fan-shroud'- part, which directs air from the print-fan to the part, should be printed in ASA or ABS, but otherwise with the same settings.