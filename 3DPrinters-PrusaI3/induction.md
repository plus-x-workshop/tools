Last reviewed 2021-10-09 by KI

# Prusa	i3 MK3S+ Printer 1 FDM 3D printer Induction

**build volume = 250 x 210 x 210 mm or 9.84 x 8.3 x 8.3 in**

## PrusaSlicer Download

Download PrusaSlicer from [github](https://github.com/prusa3d/PrusaSlicer/releases) or [prusa](https://www.prusa3d.com/page/prusaslicer_424/)

## Purchasing filament 

To buy filament go to [filamentive](https://www.filamentive.com/) they are also sustainable with cardboard rather than plastic spool. 

Weight = 1 kg
Thickness = 1.75 mm 

NOTE: any filament must have your name on it otherwise anyone is allowed to use it!

## Safety

- If you want to leave a print overnight, check with staff first. 1/3 of your print must be successfully completed before workshop closing
- No ABS to be printed on these printers (PETG, PLA, TPU only)
- Beware of the hotend (> 200°C) and bed (60-70°C)

## Software workflow (PrusaSlicer)

- Export your CAD file (e.g. Fusion 360,Solidworks, Blender) to an STL file
- PrusaSlicer is a free download, can be run on any computer.

![Slicer software](assets/prusa-slicer.png)

### Plater (basic workflow)

Provides basic overview of all settings, good enough for many print jobs.

- Quality: 0.2 mm speed is good for a quick print. If your job is printing poorly please speak to a member of staff for advice.
- Filament: Ensure you have the correct filment selected (e.g. generic PLA)
- Printer: Select: Prusa i3 MK3S+
- Supports: if your model has overhangs select supports as appropriate (or redesign your model so it doesn’t have overhangs)
- Infill: Typically 20-30%
- Brim: can help with bed adhedion

#### Model size, rotation, orientation

- Choose which face of your model you want to orient down on the bed (typically the largest flat face with fewest overhangs)
- Verify your model is the correct size

#### Export

- Press **`Slice Now`** to generate a G-Code file 
- Verify the print looks correct in the preview window (check overhangs, support, infill are as you expect). 
- Check the print time and adjust quality settings if necessary
- Press **`Export G-Code`** tosave the G-Code file to an SD card

### Advanced workflow

Use the tabs along the top to adjust advanced settings

- Print settings
- Filament settings
- Printer settings

<hr/>

## Setting Up Machine
- Do not start a job until you have checked the printer is clear of anyone else’s prints
- Check machine has correct PLA/PETG colour spool
- No other filaments are to be used. If you would like to change the spool, please ask a member of staff to do this.
- Check the nozzle size is correct (we have 0.4 mm and 1 mm). Speak to a member of the workshop staff if you are unsure of the nozzle size
- Check the nozzle is clean
- If necessary, clean the bed with IPA spray and blue roll. Do not spray directly onto the bed, but first on blue roll
- The bed is magnetic, so to remove it, lift and slide the bed towards you. When placing the bed back down keep it up at a 45° angle and push it to the back locking into the screws on the underbed


## Printing File
- Place SD card into printer to the left of the screen holder - Press the knob on the PRUSA screen holder
- Scroll down to ‘print from SD’
- Select your file, the machine will now heat up to the correct temperature you selected in PrusaSlicer
- Once heated the machine will calibrate the nozzle to the bed
- It will then move to the front left-hand side of the bed and start printing a line, to purge any old filament in the nozzle.

## While the printer is running

Observe the print for at least the first layer to ensure the print adheres to thbed and the quality looks good. 90% of failed prints fail on the fist layer.

Be aware of the hazards of the printer:

- Trapping fingers – do not place your fingers near moving part while the machine is running, as they could become trapped.
- Keep clothing, hair and other objects clear of the printer.
- Hot-end and bed: the hot end and nozzle are heated to approx 200°C. The bed may be heated to 70°C or more. Be careful not to touch the hot end when cleaning it or operating the printer. Be aware that the bed could damage objects placed on it.

## Removing Print

- Make a note of the time of when your piece is due to be finished. Please do not leave your print on the bed as others may wish to use the machine.
- Wait for the nozzle to move away from the machine (Press down on the knob – this will bring up the Z-axis for you to move it away)
- Wait for the bed to cool down – this will make it easier to remove from the bed
- Lift the bed up from the front at an angle and pull away (keep in mind the bed is magnetic so can snap back into place)
- Hold the bed in both hands and bend it gently back and forth until your piece pops off. If it doesn’t come off easily, do not use anything that can scratch the bed – ask for help.

## Cleaning

- Leave the machine as you found it
- PLA and PETG can be recycled here so please put them in the correct bins! 
- Don’t forget to unload and take your filament if it is your own (use the unload function in the printer menu)

## Member Induction sign-off form 

This is the form please follow this link [Plus X Workshop Induction — Prusa](https://docs.google.com/forms/d/e/1FAIpQLSeuD47mYHaTXkwLxPabjxVr7Vu08SAnUcyYPT-ScRRYWba22Q/viewform?usp=pp_url)

#### *this must be completed before any further use of the machine*




