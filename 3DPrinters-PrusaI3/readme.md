Last reviewed 2022-08-24 by KI

# Prusa	i3 MK3S+ Printer 1 FDM 3D Printer readme

serial number: 

* PX01-CZPX2420X004XC93766
* PX02-CZPX2420X004XC93778
* PX03-CZPX2220X004XC90193
* PX04-CZPX2420X004XC93768
* PX05-CZPX2420X004XC93788
* PX06-CZPX2520X004XK95144

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

## External docs

* [User manual (PDF)](assets/prusa3d_manual_mk3s_en.pdf)

## Contact information

### Contact information for Prusa:

* Email: info@prusa3d.com

* Live Chat: via [online store](https://shop.prusa3d.com/en/) in the bottom right corner (make sure you are logged in - 27/7)

* Email connected to Login: workshop@plusx.space

* Go to [workshop information](https://plusxteam.sharepoint.com/:x:/r/sites/WorkShop/_layouts/15/Doc.aspx?sourcedoc=%7B57330AB3-8F04-496D-979A-5137FDA7BED7%7D&file=Workshop%20information.xlsx&action=default&mobileredirect=true&cid=43894818-0f4f-418f-8783-54019f443943) to find log in and password for account 

 ## Issues

* [Issues tagged with Prusa-i3](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=Prusa-i3&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|