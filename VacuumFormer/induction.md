last reviewed 2021-10-09 by AS

# Formech 450DT Vacuum Former and Formech Trimmer FT10 Induction

## Documentation

* [Manufacturer website](https://formech.com/product/450dt/)
* [Manual (PDF)](assets/vacuum-former-manual.pdf)

## Sizing: 

- Please be aweare this is the size if the window so your sheet should be larger then these. 

* small window = 228 x 203 mm
* large window = 430 x 280 mm 

## Materials 

* **`Maximum material thickness = 6mm`**   
    + HIPS (High Impact Polystyrene)  
    + PVC (Polyvinyl Chloride) 
    + Polycarbonate  
    + PETG (Polyethylene Terephthalate Glycol) 
    + ABS (Acrylonitrile Butadiene Styrene) 

## Safety

* **`Never leave unattended`**
* **`Always remember to turn off`**
* When finished use the `'still hot'` sign to notify other users of the potential danger
* Avoid touching the metal ‘HOT’ plate   
* Only pull the heater across when the bed is in its natural lowered position 
* Be careful when removing the mould from plastic sheet and always consider what suitable tools are required to do so  
* Do not leave the vacuum on for too long (longer than 10 seconds) as this can damage your mould and sheet material  
* **Never place anything on the grills when using the vacuum former**  
* If unsure at any point, ask a member of staff for help

### Checking the machine:

* Make sure your `surroundings are clear` 
* Check that there is nothing placed `on top of the grills`  
* Check that nothing is in the way of the move able parts of the machine i.e nothing is blocking the motion of bring the heating bed across

### Setup:

* Position the machine somewhere in the workshop where you will have room to works safely
* Use the red switch to make sure the vacuum former is turned on 
* The screen will then turn on (it’s a touch screen) and you will hear a loud beep shortly afterwards 
* Turn the heater on by pressing the wavey icon `Figure.1`
* The heater can take up to 15 minutes to come up to temperature
* Press the play button `Figure.2` to go to either manual (hand icon) or saved files (folder icon) `Figure.3`
* The heater elements are separated into 5 areas which can be individually controlled to vary the temperature. 

**`Note: if the elements are not heating up then it is possible that this feature has been used to turn off the elements. It is good practice to check and set the element temperatures whenever using the machine`**

`Figure.01: `

![fig.1](assets/fig01.png)

`Figure.02: `

![fig.2](assets/fig02.png)

`Figure.03: `

![fig.3](assets/fig03.png)

### In use:

* When you select the manual settings, pull the heater across the bed – this will start the timer just above the heat icon (counts in seconds)
* The exact length of time the heater should be over the material will depend on the material and the element temperatures. It is good practice to experiment and record your results to achieve consistent results
* If unsure of how long your material reuquires, periodically retract the heater and check the material until you can feel it is going soft – avoid letting it droop too much as this can cause lines 
* Once your material has been heated for a sufficient amount of time, in a rapid but smooth motion, push heater back, pull the lever to raise the bed and press the vacumm button `Figure.4`
* Similar to the heat, the time required to leave the vacuum on is dependant on the material. 
**`Note: most materials begin to go hard within 60 seconds but will remain hot to touch`**
* Lower the bed using the lever and you’ll then be able to remove your mould from your vacuum formed material 

`Figure.04: `

![Fig.4](assets/fig04.png)

### After use:

* Turn off the machine
* Fit the `'still hot'` sign to the handle of the heater
* Inform a member of the workshop team, they will return the machine to its original possition in the workshop once it has cooled down

### Mould design:

* It is preferable to use wood as the material for your mould. Having a mould made out of plastic or foam might be possible but can be problematic since the mould will melt and deform when the vacuum former heater turns on. 
* When designing the mould for the part that will be vacuum formed, make sure your mould can be released with ease from the sheet of plastic you are using. To do this, slightly ‘draft’ straight edges so the mould can slip out the plastic sheet.  
* This can be done by including draft angles in your cad design or slightly sanding edges that are parallel to the motion of release.  


## Member Induction sign-off form 

This is the form please follow this link [Plus X Workshop Induction — Vacuum Former](https://docs.google.com/forms/d/e/1FAIpQLSduisUg-kurFpQ_HWhSt5KZPa5dRlxFUtNytmz237xE-XNSJw/viewform?usp=sf_link)

#### *this must be completed before any further use of the machine*