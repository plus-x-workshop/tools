last reviewed 2022-08-25 by KI

# Formech 450DT Vacuum Former and Formech Trimmer FT10 maintenance

* [User manual](https://files.rolanddga.com/Files/GS-24_UsersManual/Responsive_HTML5/index.htm#t=GS-24_index.html)
* [website](https://uk.formech.com/product/formech-450dt-vacuum-former)

### Firmware Updates

n/a

### General Maintenance

* [What the GS-24 can be Used For](https://files.rolanddga.com/Files/GS-24_UsersManual/Responsive_HTML5/index.htm#t=GS-24_USE_EN_03_1.html)
* [Maintenance/Replacement](https://files.rolanddga.com/Files/GS-24_UsersManual/Responsive_HTML5/index.htm#t=GS-24_USE_EN_10.html)
* [What to Do If](https://files.rolanddga.com/Files/GS-24_UsersManual/Responsive_HTML5/index.htm#t=GS-24_USE_EN_11.html)

* Optional Extras and Accessories: *page 18*
    * It is unlikely that you will need to service or repair your machine for many years provided you follow the maintenance information contained in this manual; however the table and clamp seals, which are considered to be consumable items, will need to be replaced depending on the usage of the machine.

* Service / Repair: *pages 28 - 35*
    * Replacing Seals: *page 28*
    * Replacing a Heating Element: *pages 29/30*
    * Electrical Troubleshooting: *page 31*
    * Vacuum / Pressure Troubleshooting: *pages 32-34*
    * Cleaning: *page 35*
    * Lubrication: *page 35*

### Replacement parts

