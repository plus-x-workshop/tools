last reviewed 2022-08-24 by KI

# Formech 450DT Vacuum Former and Formech Trimmer FT10 readme

serial number: 35141 & 34706

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintanence guide](maintanence.md)

## External docs

* [User manual (PDF)](vacuum-former-manual.pdf)

## Contact information

* [Website](https://formech.com/contact/)

* Email: Claire Simpson (Logistics Manager) claire@formech.com  
* Tel:  +44 (0) 1582 469 797

## Issues

* [Issues tagged with v-chamber](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=v-former&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|