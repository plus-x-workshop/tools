last reviewed YYYY-MM-DD by INITIALS

# MACHINE NAME readme

serial number: 

<!-- For groups of tools, or rooms/areas, you may need to adjust this template -->

## Our docs:

<!-- Link to files in this directory -->

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* Best practice guides, tips etc.

## External docs

<!-- Link to any documents on other websites -->

* User manual (PDF)(assets/manual)

## Contact information

<!-- add companies main number -->

* Company Support Number: XXXX XXX XXXX

<!-- add companies emails for different people-->

<!-- add were to put issues for that machine and what the company needs -->

* website
* email 

## Issues

<!-- All machine issues should be tagged with this machine name -->

<!-- Edit this link appropriately -->

* [Issues tagged with MACHINE NAME](gitlab_issue-label-ALL)

## Services

<!-- All machine issues should be tagged with this machine name -->

<!-- Edit this link appropriately -->

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|


## Maintenance


