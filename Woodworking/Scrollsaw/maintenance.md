last reviewed 2022-08-17 by KI

# T406SS/EX-16 Scroll Saw 230V maintenance

- [Manual (PDF)](assets/scrollsaw_manual.pdf)

### Firmware Updates

n.a

### Maintenance, Adjustments & Servicing

* *pages 14-15*

## Changing the Saw Blade

* Installation & Assembly: *pages 8-10*