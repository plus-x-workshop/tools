# T406SS/EX-16 Scroll Saw 230V Induction


<!--Meta-->

## Induction

Author: AS
Last reviewed date: 2025-03-07

<!--Intro-->

## Description / intended use

For making curved cuts on small flat pieces of material such as wood, plastics and non-ferrous metal.

## External documentation and links

- [Manual](https://cdn.axminstertools.com/media/downloads/101771_101772_manual.pdf?_ga=2.265710348.2006284982.1643385652-734873540.1641808764&_gac=1.221271402.1643385652.Cj0KCQiAxc6PBhCEARIsAH8Hff21pW3y6jv-KrqGlItHAcp_khfYm2ek4UadwDFacLd2uSNwnJlQ7eMaAtXnEALw_wcB)


## Risks

| **Hazard**                            | **Control**                                                  |
|---------------------------------------|--------------------------------------------------------------|
| Blade could cut or entangle           | Blade is protected with guarding.  Operators are instructed not to wear gloves during operation to prevent greater risk of entanglement. Operators instructed to wear correct PPE and clothing and to keep hands away from moving parts. |
| Incorrect fitting of blade            | Operators are trained to check blade for damage and to ensure it is installed correctly. All operators are trained on how to replace blade |
| Inhalation of material swarf or dust. | Members and staff, instructed to use dust/fume mask if required.<br><br>Use Metal extractor for cleanup depending on material cut.  |
| Operator could be hit by small fine particles/chips or a workpiece that binds in blade | Operator wears PPE: eye protection <br/>Care taken to ensure workpieces are not forced through machine |





<!-- Main section -->

## Safe Working Practices

### Before use

* Check that the area where you cut is clean and smooth 
* Check you have measured your piece correctly  
* Check the `black on and off button` (see image) is switched on
* Move the guard via the black knob to the left of the saw up or down depending on your material 
* Check the blade is facing down like so in `image A`
* Check the white leaver above the blade is pulled tight and the black knobs are tight holding the blade in place. 

![top_on_off](assets/top_on_off.png)

![blade_direction](assets/blade_direction.png)

### In use

* Place your material on the cutting area flat  
* The black tube with an orange nozzle on the end – this is air flow which blows away the dust that is made as you are working. 
* The black knob with a white face on the top of the machine is the speed of the blade. This can be change depending on the material being used. 
* Press the green button wait for the blade to get to full speed and guide your material through 
* **DO NOT ALLOW YOUR FINGERS TO GO INSIDE THE GUARD AREA AROUND THE BLADE**  
* If your material is jumping up and down – STOP THE MACHINE and move the guard down flush with your material 

### After use

* Turn off the machine and wait for the blade to stop
* Remove all your scraps from the cutting area – place them in the bins provided  
* Make sure the area is clean before leaving the machine 

<!-- ### Staff only: maintenance and cleaning -->

## PPE Requirements

Eye protection

  
## Member Induction sign-off form 
[Plus X Workshop Induction — Scroll Saw](https://forms.gle/Dxww6rmR9QLJiqAf7)

*this must be completed before any further use of the machine*