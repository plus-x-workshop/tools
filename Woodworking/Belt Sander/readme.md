last reviewed 2022-08-24 by KI

# JET JSG96 6" X 9" Belt and Disc Sander 230V readme

serial number: 180530635

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

## External docs

* [User Manual (PDF)](assets/beltsander_manual.pdf)

## Contact information

* Email: b2beast@axminstertools.com
* Number: (Alex) 07976 968733
* [customer services website](https://www.axminstertools.com/customer-services)

* [website](https://www.axminstertools.com/axminster-trade-series-atdp20f-floor-pillar-drill-102555)

## Issues

* [Issues tagged with beltsander](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=beltsander&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|