last reviewed 2025-02-14 by AS

# JET JSG96 Belt and Disc Sander Induction

## Documentation

- [Manufacturer website](https://www.axminstertools.com/axminster-industrial-series-sbw3501h3-bandsaw-103472)
- [Manual (PDF)](assets/bandsaw-manual.pdf)

## Description / intended materials

+  OK: flat sheet and block materials: MDF, plywood, timber boards
+  Not suitable: metals, small pieces (that cannot be held securely)


## Risks


| **Hazard**                                                   | **Control**                                                  |
|--------------------------------------------------------------|--------------------------------------------------------------|
| **COSHH** Wood dust inhalation                               | Ensure room extraction is on when sanding. Supplement with dust mask if necessary. |
| **COSHH** Debris hitting eyes<br><br>Sanding could cause dust or debris to hit eyes and cause injury | PPE: Eye protection to be worn                               |
| **Mechanical** Risk of trapping between the belts and pulleys, between the sanding surface and a fence, or between the sander and the table.   Hair, jewellery & clothing can become entangled with rotating parts. | Fences and guards fitted, and not to be removed. Users instructed to remove jewellery, and secure hair and clothing. |
| **Mechanical** Workpieces, belts & discs can be ejected.<br><br>Risk of cuts & abrasions. | Operators instructed to only use downward side of disc sander. |
| **Electricity**<br><br>Risk of injury due to faulty equipment, contact with live electrical components or improper use. | Machine subject to regular inspections and maintenance. Machine to be inspected before use. |
| **Noise Levels At/Or Above 85dB(A)**<br><br>Risk of hearing damage due to exposure to excessive levels of noise. | PPE: Ear defenders to be worn when extraction is on          |



<!-- Main section -->

## Safe Working Practices

### Before use

- Inspect the sander for damage
- check the sanding disc / belt isn't worn out
- Ask a member of staff to swap in a new disc / belt if needed.

- Ensure the extraction vent at the back of the machine is open


- If you need the **fence** slot it in either worktable groove.
-  Check the disc **table is level**, if not go to the right side of the machine and look under the bed, you’ll see two black levers. The one on the left loosens the bed so you can adjust the angle, the one on the right fastens the bed in place. Make sure you tighten up both. 
- Check the belt table is level, if not it can be adjusted with the two black levers either side of it, using a square or angle gauge to set a specific angle. 

### In use

- Turn on the extraction
-  Press the green button  under the red cover on the right side of the machine, wait for the disc to get to full speed.
-  If you think the disc / belt doesn't sound like it isn’t running smoothly, please ask a member of staff to check! 
-  **DO NOT ALLOW YOUR FINGERS TO GET CLOSER THan 10cm TO THE DISC / BELT**
-  If your piece is large and you need help PLEASE ASK! DO NOT ATTEMPT BY YOURSELF  


- **Always sand on the downstroke side of the disc, sanding on the upstroke can cause the material to jump.**
- Never have the machine on or plugged in with the disc guard open. 
- Extraction must be on when machine is in use.
- Check all loose clothing is secured out of the way and hair is tied back.
- Never sand without the worktable supporting the stock.
- Do not sand pieces too small to hold securely.
- Be cautious when sanding end-grain, it can cause the disc to bite aggresively.
- Never reach under the table while the sanding disc is running to tighten the bed. 
- Always stand in front of the disc, never directly in line with the plane of rotation. 


### After use


- Turn off the machine by pressing the red button, wait for the disc to stop completely before adjusting the machine. 
-  Remove all your scraps from the worktable. 
-  Use the festool extractors to clear any dust around and on the machine. 

<!-- ### Staff only: maintenance and cleaning -->

## PPE Requirements

- PPE must be worn: Steel toe caps, goggles, ear defenders, dust mask if sanding for an extended time, or MDF for any period.


## Sign-off

[Plus X Workshop Induction — Beltsander](https://forms.gle/ju4RiM8P4a29kS39A)

*This must be completed before any further use of the machine*