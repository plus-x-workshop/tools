last reviewed 2022-08-17 by KI

# JET JSG96 6" X 9" Belt and Disc Sander 230V maintenance

* [User Manual (PDF)](assets/beltsander_manual.pdf)

### Firmware Updates

n/a

## Maintenance and inspection

**General notes:**

- Maintenance, cleaning and repair work may only be carried out after the machine is protected against accidental starting by pulling the mains plug.
- Check sanding disc and belt regularly for faults. Replace a defective sanding disc of belt immediately.
- Clean the machine regularly.
- Inspect the proper function of the dust extraction daily.
- Defective safety devices must be replaces immediately.
- Repair and maintenance work on the electrical system may only be carried out by a qualified electrician.


## Changing/ cleaning the belt sander

* Use ['ABRASIVE BELT CLEANER'](https://www.screwfix.com/p/titan-abrasive-belt-cleaner-76mm/16210) first and if the belt and disc sander is still not abrasive enough then change the discs/ belt/. 

* *Pages 5/6* Fig. 7.1 to Fig. 9

| Specifications       |                     |
| -------------------  | ------------------- |
| Sanding belt dimensions (mm) | 100 mm x 1220 mm |
| Sanding disc diameter (ø-mm) | 230 mm      |

### Replacement parts

- [HERMES RB 377 YX ABRASIVE BELT 150 X 1,220MM - 80G](https://www.axminstertools.com/hermes-abrasive-belt-150-x-1-220mm-x-80-grit-110224)
- [Axminster Workshop Abrasive Disc SIA 230mm - 80g](https://www.axminstertools.com/jet-abrasive-disc-sia-230mm-80g-300085?queryID=2bbbc74c1220762f75dd68d3bdf16816)
- [Ebay](https://www.ebay.co.uk/itm/334936516177)


