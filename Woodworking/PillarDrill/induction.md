Last reviewed 2025-02-14 by AS

# AT540PD Floor Pillar Drill Induction


<!--Intro-->

<!-- ## Description / intended use -->

## Materials 
* OK: flat sheet and block materials: MDF, plywood, timber boards, aluminium 
* Not suitable: hard metals, small pieces (that cannot safely be clamped in vice)
* *Cutting speed is set up for wood by default*, if this needs to be changed ask a member of staff

## External documentation and links

- [Manufacturer website](https://www.axminstertools.com/axminster-trade-series-atdp20f-floor-pillar-drill-102555)
- [Manual (PDF)](assets/pillar-drill-manual.pdf)

## Risks


| **Hazard**                                                   | **Controls**                                                 |
|--------------------------------------------------------------|--------------------------------------------------------------|
| **Noise** Workers and others in the vicinity may suffer temporary or permanent hearing loss from exposure to noise | Extraction not generally used for this tool, but user could be exposed to noise if other users have extraction on.    Hearing protection provided.    |
| **Electric power** Risk of electric shocks and fire risks including smoke inhalation and burns to people in the vicinity | Yearly PAT testing and weekly maintenance to check for cable/machine damage. Internal fire checks for workshop completed monthly, with Fire audit completed yearly. Fire extinguisher and fire blanket provided in workshops and across office space. |
| **Injury through entanglement**  Operative could suffer serious injuries due to becoming entangled with moving parts | Spindle is protected with interlock guarding. Operators are instructed not to wear gloves during operation to prevent greater risk of entanglement. Operators instructed to wear correct PPE and clothing and to keep hands away from moving parts. |
| **Injury from guard misuse**  Operative and others could suffer serious injury from strike and puncture wounds if guards are not used correctly | Spindle guard is interlocked, if damaged or open spindle will not operate. Operators are also instructed to wear safety glasses |
| **Incorrect fitting of cutting tool**  Person operating the tool and other persons within local vicinity may suffer strike wounds | Spindle guarding located around spindle. Operators instructed to wear safety glasses which are provided by CRL. |
| **Incorrect securing  of material**  Person operating the tool and other persons within local vicinity may suffer strike wounds | Staff and members are trained how to correctly clamp material using the selection of clamping system. |
| **Burns**  Burns to fingers and hand, from cut materials or cutting tools. | User instructed to use coolant system while operating to reduce temperature of material and cutting tools. Also trained to not touch freshly used tools. |
| **Respiratory** Inhalation of material swarf or dust.        | Members and staff, instructed to use dust/fume mask if required. Workshop staff are provided half mask with ABE1 filters, but members are instructed to purchase a suitable dust/fume mask. |



<!-- Main section -->

## Safe Working Practices

### Before use

* `Extraction` must be on when machine is in use for any significant time - otherwise clear up with the Festool extractor
* Check all `loose clothing` is out of the way and hair is tied back
* Do not touch any of the black knobs or levers on the back of the machine unless told so on this document
* Check the cutting area is clear
* (if aplicable) Position and secure the vice to the machine bed so that the drill bit can pass through the work piece without contacting the vice
* Choose the `correct drill size` and place into the chuck use both hands one on the top (A) and one on the bottom (B) piece
* Check the **emergency top button** is pulled out to start the machine

![Drill chuck diagram](assets/drill-chuck.jpg)

* Test, while unpowered, that the drill bit is extending as nessacery
* If the drill bit does not extend as nessacery, either:
    + Bring the bed up to the correct height for your piece, to move the bed up and down use the black handle to the right side of the back of machine
    + Bring the drill down with the silver knobs to the left of the machine. These can allow you to set a depth gauge when drilling. This can also prevent the drill from moving down so move the bottom silver knob up if very low 
* Secure your workpiece in the vice
* Bring the perspex chuck guard from the left side of the machine to around the front of the chuck
* Check the table is locked in a stable position
* Power on the drill and check the bit is held centrally in the chuck


**`NOTE: The chuck guard is electronically connected and will prevent the machine from turning on if it is not correctly positioned. An audible click can be heard when correctly positioned`**

### In use

* Turn on the extraction
* Check the red button is not pushed in – to pop it out just turn it clockwise
* Push the green button, allow the drill to start up to full speed and check your drill piece is straight – if it is not spinning straight turn it off and reinsert the drill piece as explained above 
* **DO NOT ALLOW YOUR FINGERS TO GO INSIDE THE GUARD AREA AROUND THE DRILL** 
* Pull the three handled handle on the right-hand side of the machine, then guiding it through your material slowly  
* Bring the handle up turn off the machine and check you have gone all the way through your material


### After use


* Turn off the machine and wait for the drill to stop completely before going near  
* Remove all your scraps from the cutting area – place them in the wood bins provided   
* Make sure the area is clean before leaving the machine  


If drilling aluminium:
* Use the metal-compatible dust extractor
* Clean the tool of accumulated metal dust

<!-- ### Staff only: maintenance and cleaning -->

## PPE Requirements

* PPE must be worn: Steel toe caps, goggles, ear defenders

## Sign-off

[Plus X Workshop Induction — Pillar Drill](https://forms.gle/Hi8jswwdRe7nA3mb8)

*This must be completed before any further use of the machine*

