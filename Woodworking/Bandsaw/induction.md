last reviewed 2025-02-14 by AS


# AT3086B Band Saw Induction



## Description / intended use

+  OK: flat sheet and block materials: MDF, plywood, timber boards up to 50mm in thickness
+  Not suitable: metals, small pieces (that cannot safely be passed through blade)


## External documentation and links

- [Manufacturer website](https://www.axminstertools.com/axminster-industrial-series-sbw3501h3-bandsaw-103472)
- [Manual (PDF)](assets/bandsaw-manual.pdf)


## Risks

| **Hazard**                                                   | **Controls**                                                 |
|--------------------------------------------------------------|--------------------------------------------------------------|
| Sprains, fractures and tissue damage could be suffered by operatives or public from slipping, tripping or falling over tools, materials, machinery or floor areas in poor conditions | Work area always kept clear. Daily checks are completed by Plus X workshop staff. Operator to complete machine and area check before use. Anti slip matting used in front of machine. Process in place to ensure workshop staff are informed if risk is present. |
| Blade could cut or entangle leading to lacerations, fractures, or amputations.   | Users trained to use push sticks and guides when cutting on machine and to lower guard so no more than 20mm on the blade is visible above the material. Operators not to wear gloves when blade is moving. |
| **Hazard to hands from general factory work**<br>Operatives can suffer skin disease and damage including dermatitis by prolonged contact with a range of materials | Operators are unable to wear hand protection during operation of machine as increasing tangling risk. Operators are encouraged to wash hands after use. |
| **Manual handling**<br>Staff may receive back and other injuries if correct practices are not adhered to | Larger items to be cut on machine may be too heavy for one operator so operators are trained to user help when required. |
| **Fire / explosion**<br>All people in the vicinity could suffer smoke inhalation or burns | This machine is restricted to just wood and plastic cutting to prevent fires from metal cutting. Co2, water and fire blankets located in workshop room. Room power Emergency stop buttons located at Fuse box. Sprinkler system across building. |
| **Noise**<br>Workers and others in the vicinity may suffer temporary or permanent hearing loss from exposure to noise | The workshop is not designated a hearing protection zone due to the intermittent noise levels generated, however hearing protection is provided should it be required.    |
| **Electric power**<br>Risk of electric shocks and fire risks including smoke inhalation and burns to people in the vicinity | Yearly PAT testing and weekly maintenance to check for cable/machine damage. Internal fire checks for workshop completed monthly, with Fire audit completed yearly. Fire extinguisher and fire blanket provided in workshops and across office space. |
| **Injury through entanglement**<br>Operative could suffer serious injuries due to becoming entangled with moving parts | Blade is protected with interlock guarding. Operators are instructed not to wear gloves during operation to prevent greater risk of entanglement. Operators instructed to wear correct PPE and clothing and to keep hands away from moving parts. |
| **Injury from guard misuse**<br>Operative and others could suffer serious injury from strike and puncture wounds if guards are not used correctly | Blade guard is adjustable to cover blade as much as possible and trained to allow no more 20mm above cutting material. Operators are also instructed to wear safety glasses |
| **Incorrect fitting of blade**<br>Person operating the tool and other persons within local vicinity may suffer strike wounds | Only trained workshop staff can replace blades. Machine is tested after change to ensure machine is functioning safely and correctly. |
| **Respiratory** <br>Inhalation of material swarf or dust.    | Members and staff, instructed to use dust/fume mask if required. Workshop staff are provided half mask with ABE1 filters, but members are instructed to purchase a suitable dust/fume mask. Machine is connected to LEV system which removes fine dust from cutting process. Filters through HEPA filter. M rated Vacuum used to cleaning any surface dust. |



<!-- Main section -->

## Safe Working Practices

### Before use


- Extraction must be on when machine is in use
- Check all loose clothing is out of the way and hair is tied back
- Check that the area where you cut is clean and smooth 
- Bring the **guard down** by using the rolling handle on the right side of the machine - this is to cover the blade rather than to hold the material down.  
- If you need the **fence** slot it in the grove, locking both black levers in place  
-  Check the **table is level**, if not go to the back of the machine and look under the bed you’ll see two black levers. The one on the left loosens the bed and the one on the right if you twist it will angle the bed. Make sure you tighten up the left lever well.  
-  Check all **black knobs on machine** on the right side are locked – these are the doors to the blade and will course you harm if opened  
-  Check **red arrow in the screen** on the left of the machine is between 3 and 4 – please ask a member of staff if this has changed 
-  By the buttons check the top dial which us the **break is on ‘run’**  
-  Make sure the **red button** with the key in it is popped up (ask a member of staff if it is not) 
-  Check the **Emergency Stop Button** is pulled out
-  Check you have measured your piece correctly  



### In use

- Turn on the extraction
- Place your material on the cutting area flat  
- Press the green button wait for the blade to get to full speed and guild your material through keeping your fingers away from in front of the blade 
- If you think the blade sound like it isn’t running smooth please ask a member of staff to check! 
- **DO NOT ALLOW YOUR FINGERS TO GO INSIDE THE DARK GREY AREA AROUND THE BLADE** 
- Use the push sticks for help guild your piece through  
- If your piece is long and you need help PLEASE ASK! DO NOT ATTEMPT BY YOURSELF  


### After use

- Turn off the machine and wait for the blade to stop completely before going near your piece 
- Remove all your scraps from the cutting area – place them in the wood bins provided  
- Make sure the area is clean before leaving the machine. 

<!-- ### Staff only: maintenance and cleaning -->

## PPE Requirements

- PPE must be worn: Steel toe caps, goggles, ear defenders 


## Sign-off


[Plus X Workshop Induction — Bandsaw](https://forms.gle/7sCzBtGRFmyVjnnt8)

*This must be completed before any further use of the machine*
