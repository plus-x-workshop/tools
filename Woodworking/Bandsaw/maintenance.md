last reviewed 2022-08-17 by KI

# AT3086B Bandsaw maintenance

* [User Manual (PDF)](assets/bandsaw-manual.pdf)

### Firmware Updates

n/a

## Changing the Saw Blade

* *Pages 30 and 31* to change the blade 
* Correct bandsaw blade information: *page 34 and 35*
    - [bandsaw finder blade:](https://www.axminstertools.com/accessories/machinery-accessories/sawing/bandsaw-blades?a_bandsaw_blade_length=23389)  
    - Bandsaw brand: Axminster
    - Bandsaw model number: AT3086B
    - RECOMMENED: [AXCALIBER GROUND TOOTH BANDSAW BLADE 3,086MM(121.1/2") X 12.7MM 6 TPI 103793](https://www.axminstertools.com/axcaliber-ground-tooth-blade-3-086mm-121-5-x-1-2-x-6-tpi-103793)

| Specifications       |                     |
| -------------------  | ------------------- |
| Bandsaw Blade Length | 3,086 mm (121.1/2") |
| Blade Width Min\Max  | 3 mm to 19 mm (alwasy go for the wides blade) |

[ Website information on bandsaw blades knowledge](https://knowledge.axminstertools.com/axcaliber-bandsaw-blades/)