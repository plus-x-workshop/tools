last reviewed 2022-08-24 by KI

# AT3086B Band Saw readme

serial number: 20040010

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

## External docs

* [User Manual (PDF)](assets/bandsaw-manual.pdf)

## Contact information

* Email: b2beast@axminstertools.com
* Number: (Alex) 07976 968733
* [customer services website](https://www.axminstertools.com/customer-services)

* [website](https://www.axminstertools.com/axminster-trade-series-atdp20f-floor-pillar-drill-102555)

## Issues

* [Issues tagged with bandsaw](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=bandsaw&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|