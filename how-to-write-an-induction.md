# How to write an induction

Suggested sections (see for example https://gitlab.com/plus-x-workshop/tools/-/blob/master/Powertools/router-induction.md)

- Documentation (links to website, videos, manuals)
- Materials (permitted safe materials)
- Safety (general safety hazards)
- How to use (break down into differenent stages of use)
- After use (what users need ot do to tidy up and leave the tool safe)
- Task (a suggested induction task to verify they can confidently use the tool)

## How to include images

Upload your images to an `assets` directory, inside the tool directory. You may need to create this directory yourself.

Link to the image with this markdown:

```
![Alternate text](assets/image-name.png)
```

## How to make induction sign-off forms

Forms and response spreadsheets live in Google Drive.
<!-- 
Shared folder here: XXX -->

### Create the form

- Select an existing form and duplicate it.
- Rename the form to "Plus X Workshop Induction — {Tool Name}”.
- Where {Tool Name} is the name of the tool — and that’s an em dash, by the way ( Shift+Option+Minus on your keyboard).
- Make sure to remove “Copy of…” in the title if it has been added when you duplicated the existing form.
- Also change the name of the form in the title area.


### Edit the questions

- The form should include an email address question at the top, so we can send them a copy.
- Each question should include yes/no options
- Members sign the form by adding their name
- Every question is required
- A timestamp is automatically collected by Google Form



### Set up response storage

- Click on the Responses tab at the top of the form editor
- Click on the green sheets icon “Create Spreadsheet”
- Choose the "Select existing spreadsheet” option
- Choose the "Plus X Workshop Induction Sign-off (Responses)” sheet from the file picker
- Open that sheet: "Plus X Workshop Induction Sign-off (Responses)”
- You will see a new tab has been created with a generic name. Select that tab, check that the questions match those that you have set up.
- Rename that tab to the tool name for the induction form you’ve just created.

### Add a confirmation step

Since anyone can fill in the form, it is important that we also verify that the user has been inducted. 

- Add an extra column in the reponse spreadsheet called "Confirmed by staff"
- Add your initials here whenever someone signs off an induction.

### Test

- Fill out the form yourself and check that all data is being recorded correctly

### Link to from the induction document

- Go back to the form editor
- Click the `Send` button
- Choose ` Send via link`
- Click the `Shorten URL` checkbox
- Copy the URL
- Add this section to the end of the induction (replacing the URL with yours):

```
## Member Induction sign-off 

Please confirm you have been fully inducted by completing this short sign-off form:

**example.com**

Once we have received a copy of your sign-off, you will be able to use the tool, and if necessary, we can set up Fabman access for you.
```

- Test the link works!

