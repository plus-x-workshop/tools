last reviewed 2022-08-18 by KI

# Apollo Pro-Spray gun maintenance

[User Manual PDF](assets/spray-gun-manual.pdf)

### Firmware Updates

n/a

### General Maintenance

* Preventive Maintenance: *page 12/13*

* minumum maintenance, check the turbine filters everytime you use the turbine, replace the filters every 6 months or sooner in dusty enviroment
* check leaking pn the sray gun gaskets and seals at regular intervals. 

### Replacement parts
 
* [Apollo Spare parts website](https://sprayguns.co.uk/collections/apollo)