Last reviewed 2023-09-20 by AS

**This induction is no longer used, it has been incorporated into the Spraybooth induction.**

<!--
# Apollo Pro-Spray gun Induction

## Documentation

 -   [Manufacturer website](https://www.hvlp.co.uk/apollo-spray-tan-1500-3s-vari-speed-professional-spray-tan-system/)
-   [Manual (PDF)](assets/spray-gun-manual.pdf)

## General Safety
- All spray paints can be hazardous to health if not handled correctly
- Never direct paint at any part of the human body
- Never dismantle the spray gun or disconnect hoses while to compressor it switched on or running
- Paints can damage the compressor - always position the compressor away from direction of spray or 'up wind' of direction of ventilation flow
- Always wear appropriate face mask and respiratory equipment - as per the manufacturing guidelines 
- Do not touch the compressor outlet and hose connection during operation - these will become hot with normal use and require sufficient time to cool down

## Operation

**Preparing the paint**
- Prepare and use your paint as per the manufacture's instruction - it is your responsibility to check suability, compatibility and safety requirements of the paint being used.
- The paint viscosity should be correct to ensure optimum spray and finish - measure the viscosity of the paint using the viscosity cup

|type of product                |approximate time (s)                          |
|----------------|-------------------------------|
|cellulose|18            |
|creosotes          |use as supplied           |
|emulsion        |27|
|hammer|24            |
|enamel          |24            |
|polyurethane          |24|
|wood stains|use as supplied            |
|adhesives          |17            |
|gel coats          |18|
 **If in doubt, a good rule of thumb is try to achieve the same viscosity as single cream**
-fill the spray cup about 3/4 full and fit the cup to the gun

**Setting up the compressor**
- Remove the gun from the compressor by lifting it while pushing the brass collar down
- Connect the gun to the hose by using the the brass collar
- Set the power knob on top of the compressor to 4 or 5

**Adjusting spray shape:**
- The shape of the spray pattern can be adjusted for optimum results

![Spray Pattern Adjustment](assets/spray-gun-shape.JPG)

**Adjusting paint flow**
- The flow of the paint and the heaviness of each layer can be adjusted by using the metal silver knob - **it is often better to spray two light coats rather than one heavy coat**

## After use
**Cleaning**
- Empty any unused material from the paint cup and wash out any residue with appropriate solvents - **the paint cup has a non-stick coating, do not use medal or sharp utensils to clean**
- Remove air cap and spray jet, and clean with appropriate solvents - ensure the air holes of the cap are clear
- Ensure all parts are clean before re-assembly

**Tidy**
- remove spray gun from hose and re-seat it on the compressor
- Re-coil the hose
- Leave ready for the next user
## Member Induction sign-off form
This is the form please follow this link [Plus X Workshop Induction — Spray Gun](https://forms.gle/B6vNeNVE9EjWcPB37)

#### *this must be completed before any further use of the machine*

-->
