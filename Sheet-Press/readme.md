last reviewed 2022-08-24 by KI

# CR Clarke R30 Sheet Press readme

serial number: R30.00290

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

## External docs

* [User manual (PDF)](assets/r30_sheet_press.pdf)

## Contact information

* [website](https://www.crclarke.co.uk/products/recycling-system/r30-sheet-press)

## Issues

* [Issues tagged with sheet-press](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=sheet-press&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|