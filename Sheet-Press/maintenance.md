last reviewed 2022-08-25 by KI

# CR Clarke R30 Sheet Press maintenance

* [User Manual PDF](assets/r30_sheet_press.pdf)
* [website](https://www.crclarke.co.uk/products/recycling-system/r30-sheet-press)

### Firmware Updates

n/a

### General Maintenance

* Material Cassette Care Instructions page 9

    * The material cassette is coated with a high-temperature, non-stick coating. *This coating should only be cleaned when cold using a soft cloth*. Surplus material will not stick to the cassette, and can be wiped off when it has cooled down.

    * The handles of the material cassette are designed to slide in when not in use, to avoid protruding from the machine. To lift the material cassette, pull the handles out to their extended position. They will lock as the cassette is lifted.

### Replacement parts

