# CR Clarke R30 Sheet Press Induction

Last reviewed 2023-11-14 by AS

## Introduction 

* The R30 Sheet Press uses heat and pressure to reform plastic granules into flat sheets. 
* The machine has two high-mass aluminum hotplaces, each with a temperature control. The upper hotplate is fixed in position, and the lower hotplate can be raised and lowered using a manual hydraulic pump.
* Shredded plastic material is loaded onto a material cassette that is then clamped shut and loaded into the machine
* The hydraulic clamp is then used to press the material, forming a flat plastic sheet
* After pressing, the sheet is then allowed to cool in the press stand and then removed from the cassette

![sp-wholemachine](assets/sp-wholemachine.jpeg)

## Health and Safety Information

| Hazard                                                       | Control                                                      |
|--------------------------------------------------------------|--------------------------------------------------------------|
| **Fumes from melting plastic**<br><br>Inhalation of toxic fumes released by melting or burning plastics     | No plastics to be heated to burning temperature. Only authorised materials to be used. Press to be used only in a ventilated room (spraybooth or heavy workshop with extraction on) |
| **Electricity** Risk of injury due to faulty equipment, contact with live electrical components or improper use. | Machine subject to regular inspections and maintenance. Machine to be inspected before use. |
| **Moving Machinery (Risk of Trapping)**   <br>Risk of trapping fingers inside press.   | Press manually operated. User can release press with accessible lever.   |
| **Heated press plates and removable insert** <br>Operator could be burned by touching hot press plates or when handling heated insert.   | PPE: gloves to be worn.                                      |
| **Handling heavy inserts**<br>Could drop insert on feet while inserting or removing, especially when hot | Insulated gloves to be worn. Caution to exercised when handling inserts. |
| **Fire - Ignition of nearby materials**<br>Materials placed on top of press, or objects above press could become hot , potentially to flash point if volatile | Nothing to be placed on top of press. Press to be operated in clear space away from combustible materials. Do not attempt to press materials which may have a flash point below 250°C Do not press materials which have been washed with any solvent that may have a flash point below 250°C. |
| The sheet press does not turn off after use. The timer is an indicator for the user, it does not switch off the heaters when elapsed.   | No unattended use. (Staff will switch off machine if left unattended) |

### Intended use and materials

* The R30 Sheet Press is intended for the pressing of granulated thermoplastic materials. 
* Only load shredded plastics into the cassette 
* Materials being pressed must be clean and dry. 

### Prohibited use

* Do not use as a general press for flattening materials
* Do not attempt to press materials which may have a **flash point below 250°C**. 
* Do not press materials which have been washed with any solvent that may have a flash point below 250°C.
* **If you are in any doubt as to the source and characteristics of a material that you wish to use in the press, seek advice before loading it into the machine.**




### Manual Handling

Beware the insert trays are heavy. Ensure you can carry them safely before loading or unloading a tray.

### PPE

* Insulated gloves must be worn



## Press settings


* Suitable times and temperatures are listed in the table below:

| Material                           | Time    | Temperature-Upper (°C) | Temperature-Lower (°C) |
| ---------------------------------- | ------- | ---------------------- | ---------------------- |
| HDPE (milk bottles)                | 10 mins | 180                    | 180                    |
| HIPS (vacuum forming sheets)       | 10 mins | 200                    | 200                    |
| PS (drinking cups)                 | 10 mins | 200                    | 200                    |
| Polypropylene (school chair bases) | 10 mins | 200                    | 200                    |
| PLA (3D printer filament)          | 30 mins from cold start  | 180              | 180              |
| PET (drinks bottles)               |    ?     |        ?                |       ?                 |


* Please note that the R30 will not fully homogenise the material, and therefore the orginial colours of the granules will be maintained. To produce a single colour sheet, ensure that the granualated material is not contaminated with other colours. 

* Should you wish to produce a multi-coloured sheet or one with a specific effect or pattern, either place a random colour mix or seperate colours as required into the material cassette. 



## Cleaning the cassette

Use a soft cloth to wipe of any plastic still on the cassette. **Do not use any metal implements or abrasive materials, as these will damage the teflon coating.**

The tile compresion mold is uncoated, and should also be cleaned with a soft cloth or plstic screaper. **Do not use any metal tools or abrasives** as these will damage the surface of the mould, making it unusable.

## Switching on machine 

In the clear box under the machine there should be:

      * set of gloves
      * black wire to plug in machine to power
      * small tile mold
      * soft cloth for cleaning cassette

Plug the machine into the mains and turn on the large white switch on the front of the machine

![sp-switch](assets/sp-switch.jpeg)


<!-- 

## Material Cassette Care Instructions

* the material cassette is coated with a hig-temperature, non-stick coating. 
* this coating should only be cleaned when cold using a soft cloth
* surplus material will not stick to the cassette, and can be wiped off when it had cooled down

* the handles of the material cassette are designed to slide in when not in use, to avoid protruding from the machine
* to lift the material cassette, pull the handles out to their extended position
* they lock as the cassette is lifted

![sp-handles](assets/sp-handles.jpeg) -->


## Control Panel Operation

* The touchscreen display illuminates, and after the initialisation stage comes to the main screen
* The available control are as follows:

![sp-display-explained](assets/sp-display-explained.jpeg)

## Hydraulic lever and release

* The front panel of the machine also has the control for the hydraulic clamp. 
* To clamp material to the press, turn the release knob fully clockwise, then raise and lower the clamping lever repeatedly (this will also start the timer). 
* To release the clamp, turn the know 1/2 turn anti-clockwise.

![sp-pressureram-02](assets/sp-pressureram-02.jpeg)
![sp-pressureram-01](assets/sp-pressureram-01.jpeg)

## To Prepare for use

* Set temperature and time at shown in *Control Panel Operation*
* Place the material cassette onto a workshop surface, and sprinkle with suitable thermoplastic granuals. 
* The granules should be spread in an even layer of approx 4 mm thickness, up to the raised lip at edge. 

![sp-tray01](assets/sp-tray01.jpeg)

* Secure the latches at the front of the material cassette

![sp-latch](assets/sp-latch.jpeg)

* load the material cassette into the machine 

![sp-tray02](assets/sp-tray02.jpeg)

* turn the release knob fully clockwise to lock the pump

![sp-pressureram-02](assets/sp-pressureram-02.jpeg)

* pump the clamp lever to raise the lower hotplate
* stop pumping whent he firm resistance is felt 

![sp-pressureram-01](assets/sp-pressureram-01.jpeg)

* the action of pumping that clamp lever will automatically start the timer. 
* the timer can be manually started at any point by pressing the *Start* button on the touchscreen 
* every few minutes, give the clamp lever an additional pump, to ensure the the granules are still under pressure as they soften and fuse
* at the end of the pressure cycle, a short buzzer will indicate that the pressure material is ready to be released. 
* the timer can be cancelled at any point by pressing the *Cancel* button on the touchscreen

* turn the release knob anti-clockwise by 1/2 turn to lower the hotplate

![sp-pressureram-03](assets/sp-pressureram-03.jpeg)

## Removing the cassette 

* slide the material cassette out of the machine 

**--- WARNING: you must be wearing gloves - the cassette is very hot! ---**

![sp-tray-gloves](assets/sp-tray-gloves.jpeg)

* slide into the location on the R30S stand 

![sp-bottomtray](assets/sp-bottomtray.jpeg)

* once cooled, release the clamps and remove the pressed sheet of material 

![sp-tray01](assets/sp-tray01.jpeg)

* turn machine off and allow to cool down


**Note: The sheet press does not turn off after use. 
- The timer is an indicator for you, the user, it does not switch off the heaters when elapsed. 
- Therefore, you must be present when pressing sheets.
- Staff will switch off machine if left unattended.**



## After use

* Ensure the cassette is clean for the next user
* Put away all supplies.


## Tips for good results

Avoid putting too much pressure on the cassette, as it can be unevenly distributed, and result in a sheet that is thinner in the middle. See images below of a sheet that has been over-pressed:

![](assets/sheet-press-sheet1.jpeg)
![](assets/sheet-press-sheet2.jpeg)

## Member Induction sign-off form 

This is the induction sign-off: [Plus X Workshop Induction — R30 Sheet Press](https://forms.gle/cDZacaj54gs9CkgA9)

**This must be completed before any use of the machine**
 
