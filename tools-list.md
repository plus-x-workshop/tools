# Plus X Brighton Workshop Tools List

A list of tools and machines at the Plus X Brighton workshop, along with key specs and links to more information.

For more details on the workshop, please see <https://workshop.plusx.space>


<!-- [[_TOC_]] -->


# Fab Lab


## Trotec Speedy 300 Laser Cutter

[Manufacturer documentation](https://www.troteclaser.com/en-gb/trotec-laser-machines/laser-engravers-speedy-series/)

[Website](https://www.troteclaser.com/en-gb/laser-machines/laser-engravers-speedy-series)

- High accuracy CO2 laser cutter
- Bed size: 720 x 430 mm
- Cut up 6 mm thick flat pieces of material
- Materials: Plywood, Card, Acrylic, Foam (subject to verification)

## 3devo 350 Filament extrusion system

[Manufacturer documentation](https://support.3devo.com/)

[Website](https://www.3devo.com/filament-makers)

- Experimental filament extrusion machine
- Can produce 1.75 or 3mm (2.75mm) filament, or anything inbetweeen. 
- Can work with PLA, PETG (More to come when fume extraction is in place)
- Good for recycling plastics, or mixing custom colour / additive filaments

## Roland SRM 20 CNC Milling Machine

[Manufacturer documentation](https://www.rolanddg.co.uk/products/3d/srm-20-small-milling-machine/specifications)

[Website](https://www.rolanddga.com/products/3d/srm-20-small-milling-machine)

- Working area: 203 x 152 x 60 mm
- Typically used for milling copper PCBs and small 2.5 axis jobs such as wax or foam moulds


## Roland GS 24 Vinyl Cutter
[Manufacturer documentation](https://www.rolanddg.co.uk/products/vinyl-cutters/camm-1-gs-24-desktop-vinyl-cutter/specifications)

[Website](https://www.rolanddga.com/products/vinyl-cutters/camm-1-gs-24-desktop-vinyl-cutter)

- Rolls - 610 mm (l) x 25 meters (w)  

## Prusa i3 MK3S FDM 3D Print Farm (6 Printers)

[Manufacturer documentation](http://https/www.prusa3d.com/original-prusa-i3-mk3/)

[Website](https://www.prusa3d.com/product/original-prusa-i3-mk3s-3d-printer-3/#Features)

- FDM 3D Printers
- Build volume = 210 (l) x 210 (w) x 250 (h) mm
- PETG and PLA filament

## Ultimaker S5 Multi-material 3D printer with MMS

[Manufacturer documentation](https://ultimaker.com/3d-printers/s-series/ultimaker-s5/)

[Website](https://ultimaker.com/3d-printers/s-series/ultimaker-s5/)

- FDM 3D Printer
- build volume = 330 x 240 x 300 mm
- PETG, PLA, ABS, ASA, Carbon Fiber, PVA
- Can print with 2 Materials at once, useful for aesthetic and engineering purposes. 

## Formlabs Form 3 SLA Resin 3D Printers (2 Printers)

[Manufacturer documentation](https://www.goprint3d.co.uk/form3-complete-package.html)

[Website](https://formlabs.com/uk/3d-printers/form-3/)

- SLA (UV-cured resin) 3D printers
- Working area: 145 (l) x 145 (w) x 185 (h) mm
- Material:  Formlabs resin

## Singer 4432 Heavy Duty Sewing Machine

[Manufacturer documentation](https://www.singer.com/Heavy-Duty-4432-Sewing-Machine)

[Website](https://www.johnlewis.com/singer-heavy-duty-4432-sewing-machine/p979090?sku=232868876&s_ppc=2dx92700064000560572&tmad=c&tmcampid=2&gclid=Cj0KCQiA09eQBhCxARIsAAYRiynkPATqIob1pUP1A7TQkhTJ9glFqoD5aPWCSkuwcPJqwkRRXCM2fRQaAjhTEALw_wcB&gclsrc=aw.ds)

- One-Step Buttonhole
- Built-In Needle Threader 
- Drop Feed
- Adjustable Stitch Length / Width
- Adjustable Presser Foot Pressure
- High Speed Stitching (1100/min)
- Free Arm

# Electronics Tools

- 2 temperature controlled Weller soldering irons
- ESD-protected electronics benches
- Weller heat gun for soldering rework
- PCBite kit with 2x SP200 200 Mhz handsfree oscilloscope probes

## Antex Electronics 760RWK Hot Air station 50W, 230V

[Manufacturer documentation](https://docs.rs-online.com/67fe/0900766b8132f740.pdf)

[Website](https://uk.rs-online.com/web/p/soldering-stations/3440697)

- Digital soldering & hot air rework station
- Temperature controlled
- Digital temperature display
- Push button range adjustable from +200°C → +450°C
- Iron voltage 24 V
- Station switchable between Soldering and Hot air control

## Oscilloscope 

- Rigol MSO5204 200MHz BW, 4 Channel, 8 GSa/S Digital Oscilloscope 
- [User Guide](assets/Rigol-MSO5000-User-Guide.pdf) 
- [Website](https://telonic.co.uk/product/rigol-mso5204-200mhz-digital-oscilloscope/?utm_source=Google%20Shopping&utm_campaign=Telonic%20GG%20Shopping&utm_medium=cpc&utm_term=876&gclid=Cj0KCQiAmeKQBhDvARIsAHJ7mF4r687cP4nfaP5LTNCbV05c9eHqVQAUDuzmXsiHHN-sZkt36xknILQaAvUJEALw_wcB)

## Reflow plate/oven

- Quick 870ESD 800WPreheater Hot Plate 220V 
- [Information Sheet](assets/hotplate-spec-sheet.pdf)

# Heavy Workshop

## JessEm Mast-R-II Router Table

- TODO



## Itech Auto tool changer CNC router

[Manufacturer documentation](https://www.scosarg.com/itech-k6090t-q-series-desktop-cnc-router#/)

- 5 Tool ATC
- Vacuum hold-down bed
- 2.2Kw spindle, suitable for anything up to soft metals like Aluminium / Brass
- V-carve support, simple to use / learn compared to fusion 360 or other CAM packages

## Trotec Q500 Laser Cutter

[Manufacturer documentation](https://www.troteclaser.com/en-gb/)

- Large format CO2 laser cutter
- 120 Watts
- Bed size: 1300 x 900 mm
- Cut up 15 mm thick flat pieces of material
- Materials: Plywood, Card, Acrylic, Foam (subject to verification)
    - Cut and engrave: Wood (inc plywood), paper and cardboard, textiles, leather, cork, ABS, acrylic, laser rubber, polyamide, polybutylene terephthalate, polycarbonate, polyethylene, polyester, PET, polyimide, polyoxymethylene (POM) e.g. Delrin, polypropylene, polyphenylene sulfide, polystyrene, foam (PVC free, e.g. polyurethane)  
    - Engrave only: Mirror, glass, stone, ceramics, aluminum, (and anodized), precious metals, titanium, stainless steel, coated metal (varnished), brass, copper


## Roland MDX 540 CNC Milling Machine

[Manufacturer documentation](https://www.rolanddg.co.uk/products/3d/mdx-540-benchtop-milling-machine/specifications)

[Website](https://www.rolanddga.com/support/products/milling/modela-pro-ii-mdx-540-3d-milling-machine)

- Multi axis CNC milling machine  
- Working area of 398 x 398 x 154 mm 
- Includes rotary attachment for multi-axis milling
- Materials: wood, light metal, plastic, modelling wax, copper PCBs and foam


## Formech 450DT Vacuum Former 

[Manufacturer documentation](https://formech.com/product/300xq/)

[Website](https://formech.com/product/450dt/)

- Forming area of 430 x 280 mm
- We also have a trimmer for finished moulds

## Formech 500 Line Bender

[Manufacturer documentation](https://formech.com/product/flb500/)

[Website](https://formech.com/product/flb500/)

- For high quality bends in plastic sheet up to 10mm thick
- Lines up to 500mm wide

## Standard Machine Tools 

[Manufacturer site](https://www.axminstertools.com/)

- Axminster AT3086B Band Saw, AT540PD Floor Pillar Drill, JET JSG96 6" X 9" Belt and Disc Sander 230V, T406SS/EX-16 Scroll Saw 230V
- Standard tools for working in wood, metal and plastic. 
- Including movable workbenches and high-quality chip and dust extraction.

### AT3086B Band Saw

[Manufacturer documentation](https://cdn.axminstertools.com/media/downloads/103472_manual.pdf)

### AT540PD Floor Pillar Drill

[Manufacturer documentation](https://cdn.axminstertools.com/media/downloads/102552_102555_manual.pdf?_ga=2.172357792.2012953735.1645630318-734873540.1641808764&_gac=1.158227912.1643385663.Cj0KCQiAxc6PBhCEARIsAH8Hff21pW3y6jv-KrqGlItHAcp_khfYm2ek4UadwDFacLd2uSNwnJlQ7eMaAtXnEALw_wcB)

[Website](https://www.axminstertools.com/axminster-trade-series-atdp20f-floor-pillar-drill-102555)

### JET JSG96 6" X 9" Belt and Disc Sander 230V

[Manufacturer documentation](https://go.rockler.com/tech/JSG-96-Benchtop-Disc-Belt-Sander-Manual.pdf)

[Website](https://www.wurthmachinery.com/Jet-Tools-708595-JSG-96-Benchtop-6-X-48-Belt-9-Disc-Sander-3-4-HP-Single-Phase-115V.html)

### T406SS/EX-16 Scroll Saw 230V

[Manufacturer documentation](https://www.axminstertools.com/media/downloads/505078_manual.pdf)

## Hand Power Tools

[Manufacturer documentation](https://www.festool.co.uk/)

[Website](https://www.festool.co.uk/products)

- Festool Cordless Oscillator OSC 18, Belt Sander BS 75, Cordless Drill T 18 , Delta Sander DTS 400 R, Domino DF 500, Eccentric Sander ETS EC 150, Pendulum Jigsaw PSB 420, Slide compound mitre saw KS 120 , Mobile Dust Extractor Wood (hoover) , Circular Saw/ Rail, Saw TS 55, Router OF 1010, Hole drill set LR 32-SYS
- Including on-tool extraction for wood and metal dust
- Proxxon precision rotary tools for detail work

## Axminster Engineer Series SC4 Bench Lathe

[Manufacturer documentation](https://cdn.axminstertools.com/media/downloads/505189_manual.pdf?_ga=2.199295662.184301956.1634294240-1386396129.1632209301&_gac=1.24907720.1632229423.CjwKCAjwhaaKBhBcEiwA8acsHNCr9TFPQop1fIb6RkpnMKVIaLD0ZnUOg1OKZTN6TDS1Btw2QKXXEBoCYrgQAvD_BwE)
[Website](https://www.axminstertools.com/axminster-engineer-series-sc4-510-bench-lathe-505189)


- Electronically variable spindle speed range of 125rpm to 2,000rpm
- 1,000W high torque, brushless motor
- Metric and imperial thread cutting, simple change wheel setting


## Axminster Engineer Series SX3 Mill Drill Digi

[Manufacturer documentation](https://cdn.axminstertools.com/media/downloads/505106_manual.pdf?_ga=2.198381359.184301956.1634294240-1386396129.1632209301&_gac=1.57690456.1632229423.CjwKCAjwhaaKBhBcEiwA8acsHNCr9TFPQop1fIb6RkpnMKVIaLD0ZnUOg1OKZTN6TDS1Btw2QKXXEBoCYrgQAvD_BwE)
[Website](https://www.axminstertools.com/axminster-engineer-series-sx3-mill-drill-digi-505106)

- 1,000W brushless DC high torque motor, quiet and reliable
- 3 axis digital read-out of the table position (X & Y),and head stock height (Z)
- Digital downfeed and spindle speed indicators for precise control
- Head tilts up to 90° for angled and horizontal drilling/milling
- Dual downfeed controls, coarse for drilling, fine for milling



## Shaper Origin + Shaper Workstation

[Manufacturer documentation](file:///Users/katie/Downloads/ENG-Origin%20Product%20Manual.pdf)

[Shaper Support](https://support.shapertools.com/hc/en-gb/articles/115002734774-Getting-Started-Guide)

[Shaper Workstation Website](https://www.shapertools.com/en-gb/workstation/overview/)

[Website](https://www.shapertools.com/en-gb/?utm_source=adwords&utm_medium=ppc&utm_term=shaper%20workstation&utm_campaign=Vanity+Queries&hsa_ad=572327303216&hsa_acc=9056753060&hsa_kw=shaper%20workstation&hsa_mt=p&hsa_tgt=kwd-1192804341814&hsa_ver=3&hsa_cam=15731394175&hsa_grp=134533062711&hsa_net=adwords&hsa_src=g&gclid=Cj0KCQiA09eQBhCxARIsAAYRiyneNoTFqcwOL6mUo6qKIvKh6l9emeglE3Q7eVTCLTzaqTNU-HOB0NQaAt45EALw_wcB)

- Handheld precision routing. 
- Intuitive interface
- On-Tool Design
- Automatic Cut Correction
- Proven spindle design

# Wet Area / Bio Lab

Work with materials such as plaster and silicone, or develop new biomaterials. 

- Fridge, Oven, Dehydrator, Hobs, Sink
- AML Z3 X500 Tensile Tester - [Website](https://amlinstruments.co.uk/instruments/single-column-tensile-tester-universal-testing-machine/)
        * Loading cells = 5kN and 500N
- 26 kN toggle press with coupon die cutters

- RS pro ultrasonic cleaner 

## vacuum chamber

- Useful for casting and moulding, removing bubbles from silicone and resin
- can be used for dehydrating substances

[Manufacturer documentation](https://cdn.axminstertools.com/media/downloads/505189_manual.pdf?_ga=2.199295662.184301956.1634294240-1386396129.1632209301&_gac=1.24907720.1632229423.CjwKCAjwhaaKBhBcEiwA8acsHNCr9TFPQop1fIb6RkpnMKVIaLD0ZnUOg1OKZTN6TDS1Btw2QKXXEBoCYrgQAvD_BwE)

[Website](https://www.easycomposites.co.uk/ds26-p-professional-vacuum-degassing-system)


# Spray Booth

- Walk-in fully-ventilated spray room
- 18 sqm
- Only used for low-hazard finishes (no isocyanate-based paints/lacquers)

## Apollo Spray Gun

[User Manual](assets/spray-gun-manual.pdf)

[Website](https://bambi-air.co.uk/products/professional-system/)

# Extra

## Artec Eva 3D Scanner

[User Guide](assets/artec-scanner.pdf)

[Website](https://www.artec3d.com/portable-3d-scanners/artec-eva)

- Structured light scanner, good for scanning large objects. 
- Smaller objects possible but harder. 
- Cannot scan shiny objects 

## TORMEK T-4 Original Sharpening system

[Tormek Leaflet](https://www.tormek.com/media/3087931/tormek-leaflet-en.pdf)

[T-4 Machine Breakdown](https://www.tormek.com/media/1728799/t-4_machine_breakdown.pdf)

[Material Safety Data Sheet](https://www.tormek.com/media/3333545/pa70_material_safety_data_sheet.pdf)

[Material Safety Data Sheet](https://www.tormek.com/media/1770953/material-safety-data-sheet-en.pdf)

[Link to videos](https://www.tormek.com/uk/en/machine-models/tormek-t-4-original/)


- Grindstone: Tormek Original Grindstone, Aluminium oxide, ∅ 200×40 mm (8"×1 5/8") 120 rpm, torque 8.4 Nm
- Leather Honing Wheel: ∅ 145×26 mm (5¾"×1")
- Motor: Industrial single phase, 120 W (input)
    - 230 V, 50 Hz or 115 V, 60 Hz. Duty 30 min/hour, Maintenance free.
    - Silent running, 54 dB. 10,000 hour life
