last reviewed 2022-08-25 by KI

# Sump maintenance

* [invoice](https://plusxteam.sharepoint.com/sites/WorkShop/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FWorkShop%2FShared%20Documents%2FPurchasing%2FInvoices%20%28or%20Receipts%29%2FAmazon%2FAmazon%20%2D%20Sump%2Epng&parent=%2Fsites%2FWorkShop%2FShared%20Documents%2FPurchasing%2FInvoices%20%28or%20Receipts%29%2FAmazon)

### Firmware Updates


### General Maintenance

* Wear gloves and apron which are under the sink by the sump
* Have a plastic bag with you, check it has no wholes (all waste will go in here)
 
* Unscrew the pipe which is attached to the sink (B) hold onto it, you want to remove this and place it in the sink
* Pipe C can be left alone but the pipe tubing is moveable so you can rotate the sump box (A) towards you 
* Place the bag so it is easy to place items inside and remove the metal basket (D)
* Place it in the sink and wipe down with blue roll
* In the bottom of the sump (A) you will feel hard material all of this needs to be removed and placed in the plastic bag.
* Once complete places everything back (metal basket (D), sump (A) and pipe (B)making sure to screw back togegther tightly)
* Run the sink to check for leaks and wash down the sink
* Place the gloves and apron back under the sink, 
* Put the waste in the wood store bins outside

![sumpdiagram](assets/sumpdiagram.png)

### Replacement parts