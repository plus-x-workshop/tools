last reviewed YYYY-MM-DD by INITIALS

# DS-26P 26L Professional Vacuum Degassing System maintenance

[User Manual PDF](assets/vacuum-chamber-manual.pdf)

### Firmware Updates


### General Maintenance

Periodically check the oil level in the pump and top-up as necessary using VPO32 Vacuum Pump Oil. If oil appears contaminated (cloudy), empty it from the pump and replace it with fresh vacuum pump oil.

- Never run the pump unattended.
- Never use the system on the floor.
- Never operate the pump in dusty conditions. Dust particles will be drawn into the pump and will contaminate the oil and accelerate wear.
- Don’t use the system as a vacuum dryer. Moisture extracted from damp materials such as wood or plaster will emulsify the oil.
- Never use anything other than DVP BV32(SW40) or Easy Composites’ VPO32 Vacuum Pump Oil to top-up your vacuum pump.
- Never use solvents to clean the pump.

* [website](https://www.easycomposites.co.uk/ds26-p-professional-vacuum-degassing-system)

### Replacement parts

