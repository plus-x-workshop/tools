last reviewed 2022-08-24 by KI

# Wetlab readme

serial number: n/a

## Our docs:

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

##### Vacuum chamber

* [Vacuum Chamber Induction](https://gitlab.com/plus-x-workshop/tools/-/blob/master/Vacuum-Chamber%20/induction.md)
* [Vacuum Chamber Maintenance](https://gitlab.com/plus-x-workshop/tools/-/blob/master/Vacuum-Chamber%20/maintenance.md)
* [Vacuum Chamber Readme](https://gitlab.com/plus-x-workshop/tools/-/blob/master/Testing-Machine/readme.md)

##### testing machine

* [Testing Machine Induction](https://gitlab.com/plus-x-workshop/tools/-/blob/master/Testing-Machine/induction.md)
* [Testing Machine Maintenance](https://gitlab.com/plus-x-workshop/tools/-/blob/master/Testing-Machine/maintenance.md)
* [Testing Machine Readme](https://gitlab.com/plus-x-workshop/tools/-/blob/master/Vacuum-Chamber%20/readme.md)

## External docs

* [Vacuum Chamber user manual (PDF)](vacuum-chamber-manual.pdf)

* [Testing Machine user manual (PDF)](Z3-Z5-Z10-Z20-Z50-Z100-01-05-2017-Operation-Manual.pdf)

## Contact information

* [testing machine website](https://amlinstruments.co.uk/instruments/tensile-test-machine-universal-testing-machine/)

* [vacuum chamber website](https://www.easycomposites.co.uk/ds26-p-professional-vacuum-degassing-system)

## Issues

* [Issues tagged with Wetlab](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=Wetlab&first_page_size=20)

## Services

<!-- All machine issues should be tagged with this machine name -->

<!-- Edit this link appropriately -->

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|