last reviewed 2022-08-25 by KI

# Wetlab maintenance

* [vacuum chamber user manual PDF](assets/vacuum-chamber-manual.pdf)
* [testing machine user manual PDF](assets/Z3-Z5-Z10-Z20-Z50-Z100-01-05-2017-Operation-Manual.pdf)

### Firmware Updates

n/a

### General Maintenance

* wipe down all the surfaces and clean sink, add more blue role
* check all the wires and all are working when plugged in: hot plate, cure, proxxon, scales and toaster
* sweep behind the bin, oven, coshh cabinet and fridge
* check all glass beakers are there and clean 
* check under sink and tidy up
* check cupboard is clean and safe

#### Machine Maintenance

* check the [sump maintenance](sump_maintenance.md)
* check the [vacuum chamber maintenance](vacuum_chamber_maintenance.md)
* check the [testing machine maintenance](testing_maintenance.md)

### Replacement parts







