# Hyundai Air Compressor Induction

Last updated  2025-02-14 by AS


## Description / intended use

- For cleaning workpieces
- For powering the ATC flatbed CNC 
- Cleaning inside of plastic shredder


## Prohibited uses

- Unsafe uses as noted below
- Do not connect the compressor to any other tools
- Do not use to clean up dust, liquid waste or swarf. Use a brush or portable extractor (use the metal extractor for any metal waste).  



## Risks


| **Hazard**                                                   | **Control**                                                  |
|--------------------------------------------------------------|--------------------------------------------------------------|
| **Compressed air blown into the eyes, ears, skin or body**<br><br>**Severe** Eye, ear, skin or internal organ injury from high-pressure air. | Users instructed in appropriate use.                         |
| **Impact or inhalation of waste material**    <br><br>Compressed air used to clear up hazardous waste (e.g. dust or swarf) that could cause injury by inhalation or contact with the body at high speed | Users instructed in appropriate use.  Prohibited to use the compressor to clear up hazardous waste |
| **Impact from loose pressurised airline**  <br><br>A line could become detached, ‘whip’ and could cause injury | Ensure that air line connected to the compressor are the correct size and suitable for the working pressure. Also in good condition. Check no kinks in hose before and during use |
| Horseplay or casual use (eg to clean clothing) may result in injuries | [Users instructed in appropriate use.]()  Use is policed and inappropriate use reprimanded.[\[EC1\]](#_msocom_1)  |
| Explosion / depressurisation risk from over-pressurisation   | Receiver is fitted with a safety valve and pressure switch The receiver is fitted with a pressure gauge which indicates pressure in bar, lbf/sqin, or other suitable units. Safe max pressure is indicated on the scale and user are instructed how to read it. Receiver can be drained with airline. |
| Poor maintenance leading to failure of compressed air tank<br><br>Moisture accumulated in the tank will corrode the inside of the tank and cause weakness leading to an explosion or other failure of the tank Dirty air can cause a system to fail eg by causing fine particles of debris to agglomerate, blocking safety related valves. | Water to be regularly drained from the tank by staff Air filter to be regularly checked, cleaned (and replaced if necessary) by workshop staff |


<!-- Main section -->

## Safe Working Practices

### Before use


- Inspect the machine and make sure there is no visible damage
- Check air line is correctly fitted to compressor (collar is snapped back forward – away from the tank)
- Make sure that the air hoses are not tangled, twisted or pinched


Note: A loose, damaged or incorrectly specified airline could become detached, ‘whip’ and could cause injury. **Ensure that air line connected to the compressor are the correct size and suitable for the working pressure, and in good condition.  
Check no kinks in hose before and during use**

- Do not attempt to move the compressor by pulling the air hose.
- Do not block the engine ventilation grills or air intake
- Do not cover the compressor or restrict airflow around the machine whilst it is operating.
- Report unexpected behaviour to staff to investigate.
- If you hear continuous hissing after the initial pressurisation, the drain tap may be loose. Ask a member of staff to tighten. 
- (An initial short discharge from the glass water trap valve under the outlet pressure knob is normal when the compressor starts or when the tank is almost empty.)

![](assets/airline-collar.jpeg)
 


### In use

#### Switching on
- Perform checks as above
- Always use red power button to switch the compressor on or off (not the power switch at the wall socket)

#### Adjusting pressure
- Control outlet (working) pressure with black knob next to red power switch
- Lift up to unlock, and turn anticlockwise to reduce pressure 
- You should not need to adjust the air pressure in normal use.
 
The receiver is fitted with a pressure gauge which indicates pressure in bar, lbf/sqin, or other suitable units. Safe max pressure is indicated on the scale:
- Max Pressure (tank and working): 7 bar
- If pressure goes beyond 7 bar report to staff and discontinue use

![](assets/pressure2.jpeg)


### After use

- Turn the power switch to the OFF position.
- Unplug the compressor.
- Compressor can be left pressurised


<!-- ### Staff only: maintenance and cleaning -->

## PPE Requirements

- Safety goggles

## Sign-off

[Plus X Workshop Induction — Hyundai Air Compressor](https://forms.gle/avVqoKiemKffhnoV7)

This must be completed before any further use of the machine



