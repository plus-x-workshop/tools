last reviewed 2022-08-18 by KI

# Trotec Speedy 300 laser cutter maintenance

[Speedy 300 Manual](https://www.troteclaser.com/fileadmin/content/images/Contact_Support/Manuals/8017-Speedy-300-fiber-Manual-EN.pdf)

### Firmware Updates

* check for Ruby updates via emails 

## Clean the machine 

- Make sure the bed is low enough to remove the black bed by opening the red door on the front and being the black bed out. 
- Clean under the bed making sure there is no debris left over (this can course fires)

![Speedy Mess](assets/speedy-mess.jpeg)

- Wipe down any markings on the metal grey shelf under the bed - use anti-static foam cleaner

![Speedy Spray](assets/speedy-spray.jpeg)

- Move the bed back onto the grey shelf and move it all the way to the top of the laser cutter making sure not the hit the laser 
- clean any loose bits that may have fallen to the bottom under the grey shelf, sweep or use the hand held hoover.

## Cleaning red nozzle  

- Start by turning on the machine to see if everything lines up correctly 
- Bring the laser into the middle of the bed and unscrew the red laser head  

![nozzle](assets/nozzle.jpeg)

- Use blue roll to remove any dirt on the laser head with watered down IPA or soapy water 

![cleaning_nozzle](assets/cleaning_nozzle.jpeg)

- use the cotton buds to clean inside the nozzle as well

![cottonbuds](assets/cottonbuds.jpeg)

## Cleaning mirror and lens

[Doc for cleaning lens and mirror](assets/lens_cleaning.pdf)

- Unscrew the red ridged part under the black lens and REMOVE CAREFULLY  
- Use the cleaner liquid and lint sheets to remove dirt - dap the lens NEVER WIPE  

- Unscrew the mirror on the top by the bronze screws careful not the drop them  
- Use the cleaner liquid and lint sheets to remove dirt - dap the mirror NEVER WIPE 

## Checking extraction 

`(it is always better to change the ‘pre-filter mats’ more often than not as this will keep the ‘comfort box’ last longer)`

- Open the extraction lid by using the allen key in the Speedy 300 box in the draw 
- Remove the tube at the back and rest on the laser cutter (don’t let it drop you will struggle to get it back) 
- Lift out the ‘filter basket’ and hoover around the edge, including the lid and main box 
- Check the 2A ‘pre-filter mat’ (fluffy side facing up) 
- Hoover the filter but if this needs to be changed put the clean one in position 2B ‘pre-filter mat’ and the 2B one in position 2A  
- Remove the ‘comfort box’ - hoover the top 
- Check 2B ‘pre-filter mat’ this should be white  
- The two ‘active carbon’ bags don’t need to be changed  
- If you can tell it isn’t taking the small away, you can shake up the carbon bags which will help this.  
- You will need to attach a cable tie so they don’t go everywhere 

![filter_system](assets/filter_system.jpeg)

## Understanding extraction screen 

![speedy_screen.jpeg](assets/speedy_screen.jpeg)

**'Volume'**: 
- flow rate at which the turbines run and can be run this up to 400m3/h

`The other two values relate to the main filters pressure, you will see that as time goes on`

**'Filter'**: 
- the "filter percentage" will rise. Once it hits 100% the Atmos has determined that the turbines are having to work too hard to draw the same airflow and this is when you will get an alarm. 
- You will never see this at 0% after changing filters and a good value to see here after carrying out a filter change is around 30-40% when running the Atmos at full power.

**'ActivetedCarbo'**:
- value is simply a timer. 
- Once it hits 100% the Atmos has determined that the carbon coal needs replacing but this is only an estimate, you may find you start to notice the Atmos not extracting the smell as it once did. If this is the case I would suggest changing the coal at that point.
- If you ever need to reset the time for this, you can do so by pressing the “return” icon, moving down the coal reset option, selecting return again to put a small tick next to it and then moving down to “exit”.
- BEFORE changing the carbon bags if you place a cable tie around the carbon bags you can move the carbon around inside the bags and this will help make the % go down.

From Mat from Trotec: 'if you ever require help with that particular process then please let me know.' service-uk@troteclaser.com



 