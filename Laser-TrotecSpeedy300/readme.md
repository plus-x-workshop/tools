last reviewed 2022-08-23 by KI

# Trotec Speedy 300 Laser Cutter readme

serial number: S3-6315

## Our docs:

* [Induction document](induction.md)

* [Maintenance guide](maintenance.md)

## External docs

* [User manual (PDF)](operating-manual-Speedy-300-8066-en-us.pdf)

* [Speedy 300 Material Settings](assets/speedy300-machine-settings.pdf)
 
* Laser cutting services: [Cirrus Laser Ltd](https://cirruslaser.co.uk/) laser cutting service in Burgess Hill (recomment by another member) 

## Contact information for trotec

* Trotec Technical Support Number: 0191 4188 110 

* Andy is the best for material testing information: Campling Andrew <Andrew.Campling@troteclaser.com>
* Andy Mobile: mob +44 (0)797 056 2031
* Support: ts 1st level tluk <service-uk@troteclaser.com>

#### Add this to emails when you contacting Trotec

* Your Name: XXXX
* Your Company Name: Plus X Innovation Hub
* Your Phone Number: XXXX
* Serial Number of your machine: S3-6315

## Issues

* [Issues tagged with Speedy300](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=Speedy300&first_page_size=20)

## Services 

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| 2021-07-20 | [Speedy300 Trocare Certificate](assets/speedy_trocare_certificate.pdf)|