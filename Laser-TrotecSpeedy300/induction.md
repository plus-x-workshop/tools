last reviewed 2022-07-19 by KI

# Trotec Speedy 300 laser cutter (small) Induction
 
## Software workflow 

We support Inkscape or Illistrator with your own account for vector editing and Ruby for sending files to the laser cutter. 

## Bed size

720mm x 430mm

## Correct Files 

* illustrator (ai.)
* svg
* dxf
* vector files (eps, etc)

NOTE: often .svg files produced by illustrator import the wrong scale, this can be solved by importing a .ai file instead

## Materials 

* Places to buy from: Kitronik and Hobarts

**` --- Please ask a member of staff before cutting any plastic other than acrylic. --- `**

Many materials *`cannot`* be cut on this machine without damaging the machine or you. Under no circumstances should you attempt to cut or engrave:  

* Mirrored acrylic (with the mirror facing up) 
* Shiny metal 
* PVC 
* MDF 
* Any material for which you cannot provide the correct material data sheet 
* Any material of unknown origin (e.g. salvaged scrap) 

In general, *`acceptable`* materials are 

* Laser plywood 
* Cardboard and paper 
* Polypropylene 
* Acrylic 
* Felt
* Leather 
* Anodised aluminium can be engraved as long as it has a dull finish 
* Laser rubber 
* 2-colour laminate 

## IF CUTTING PRODUCES AND UNPLEASANT SMELL / BLACK SMOKE / SOOT, PLEASE DISABLE THE MACHINE AND CONSULT A MEMBER OF STAFF
If in any doubt about material safety, consult a member of staff first.  

## TROTEC RUBY

#### Everything written below is copied from [RubyHelp.com](https://www.rubyhelp.com/article-categories/guide/).

[Ruby Video](https://www.youtube.com/watch?v=ZHDKHiww4uY&ab_channel=TrotecLaser)

**You no longer have to do the correct colours or the correct line thickness in your vector file you can change these in ruby!**

* On the computer desktop click on the Ruby icon "Trotec Ruby" this will open up Ruby in the web browser. Make sure there is only one Ruby Tab open (check left side of browser). 
* If you need to log in this should be auto filled with 'workshop@plusx.space' and then password (please speak to a member of staff if you can't log in)
* Once into Ruby follow the steps below.

## Connecting to the machine

* Once scanned in with fabman, press the green power button on the e-stop box to turn the laser on

* Once open, the machine (if switched on) will automatically connect to **'Ruby'** 
* If it doesn't connect right click on the Ruby icon on the bottom right of the bar and hover over **'laser mode' (blue)** then make sure **'Ruby'(red)** is selected.

**` --- Notes: makesure job control is not open --- `**

![connecting+ruby](assets/connecting+ruby.jpeg)

## Manage Screen

[Ruby Help Manage Screen](https://www.rubyhelp.com/knowledge-base/management-screen/)

* The Manage Screen is the starting phase. Here you get a overview about your current Designs, Jobs and connected Laser.

* Drag your vector file onto the manage screen 

![manage_screen01](assets/manage_screen01.png)

This screen has 2 main areas:

- Management Area
- Lasers

### Management Area

![manage_screen02](assets/manage_screen02.png)
![manage_screen03](assets/manage_screen03.png)

### Lasers

![manage_screen04](assets/manage_screen04.png)

* Here you can see which laser is connected or available

## Design Screen

[Ruby Help Design Screen](https://www.rubyhelp.com/knowledge-base/design-screen/#shortcut-design)

* The Design Screen is for setting up your design before thinking about laser parameters, you can do rudimentary editing here. The toolbar has simple graphics tools. 

![design_screen01](assets/design_screen01.png)

* This screen has 3 main areas:

    * Design list
    * Toolbar
    * Right panel

### Design list

![design_screen02](assets/design_screen02.png)
![design_screen03](assets/design_screen03.png)

### Toolbar

![design_screen04](assets/design_screen04.png)
![design_screen05](assets/design_screen05.png)

### Right panel

![design_screen06](assets/design_screen06.png)

* Attributes
* Object list

### Attributes

![design_screen07](assets/design_screen07.png)
![design_screen08](assets/design_screen08.png)

### Object list

![design_screen09](assets/design_screen09.png)
![design_screen10](assets/design_screen10.png)

### Options to list your objects

![design_screen11](assets/design_screen11.png)
![design_screen12](assets/design_screen12.png)

## Prepare Screen

[Ruby Help Prepare Screen](https://www.rubyhelp.com/knowledge-base/prepare-screen/)

* The Prepare Screen is there to finalize your created job and push it to the laser. There are multiple settings to adjust like the Material or the Processing rules.

![Prepare_screen01](assets/Prepare_screen01.png)

This screen has 3 main areas:

* Job list
* Toolbar
* Right panel

### Job list

![Prepare_screen02](assets/Prepare_screen02.png)
![Prepare_screen03](assets/Prepare_screen03.png)

### Toolbar

![prepare_screen04](assets/prepare_screen04.png)
![prepare_screen05](assets/prepare_screen05.png)

### Right panel

![Prepare_screen06](assets/Prepare_screen06.png)

The Right panel is separated in 3 areas:

* Production
* Design
* Laser rules

### Production

![Prepare_screen07](assets/Prepare_screen07.png)
![Prepare_screen08](assets/Prepare_screen08.png)

### Design 

![Prepare_screen09](assets/Prepare_screen09.png)
![Prepare_screen10](assets/Prepare_screen10.png)

### Laser Rules 

![Prepare_screen11](assets/Prepare_screen11.png)
![Prepare_screen12](assets/Prepare_screen12.png)

## Produce Screen

[Ruby Help Produce Screen](https://www.rubyhelp.com/knowledge-base/produce-screen/)

* In the Production Phase you have a overview of the jobs currently queueing in your Laser. You see details about the job the laser is working on and can manage the queue.

![produce_screen01](assets/produce_screen01.png)
![produce_screen02](assets/produce_screen02.png)



## Machine – Set up and focusing  

* Turn machine on by the on and off switch in the top left of the machine on the back and log into fabman 
* Leave it to initialise – the bed will move to the bottom on the machine and the laser will move around 
* It will beep when ready 
* Place your material onto the laser bed  
* Place focusing tool onto the laser head ledge as shown in your induction 
* With the black arrows on the laser move the laser bed up toward the laser (do this slowly, it will just crash into the laser which will break the machine) 
* Now move the bed of the laser towards the focusing tool when you have reached the focusing stop – avoid bumping the focusing tool off the ledge  

***`WARNING: when moving the bed up towards the laser head, be very careful and move it slowly as you approach. If you crash the bed into the laser head, you will break the machine – this is a very costly mistake.`***

![Focusing Tool](assets/focusing_tool.jpeg)

## Printing in Job Control 

* Under the jobs list there is a USB cable head press this button - if the connection isn't made then see troubleshooting below
* By doing this it connects the computer to the laser cutter  
* The USB cable button then turns into a play button  
* You should now see the laser head on the screen in corelation to where it is on the laser bed  
* Move the laser into the correct position on the material you are cutting by using the red arrows on the laser cutter  
* Now drag your job to the laser and it should snap into place 
* You can now press the play button icon under the jobs list  
* Your job should now be printing  

## Running the job 

**`Note – ensure the sperate extractor fan is running whenever the laser is cutting. Check the extractor speed; it should read 320m^3 `**

You can start this up first to ensure good extraction as soon as the cutting starts. 

### Never leave the job unattended. You must supervise the job at all times to ensure there is no risk of fire. 

* Do not turn away from the machine to speak to someone else 

* Do not check your phone 

* Do not return to the PC to set up another job 

***`Members who are seen to not observe proper safety precautions with the laser cutter will not be allowed to use it.`***

## Finishing the job 

* Let the extractor fan run for a minute after the job has run to allow fumes to be extracted. 

* If you are cutting a material that lets off gasses after cutting (e.g. acrylic) leave it in the machine, with the lid closed for at least a minute before removing. Cut parts that are still letting off gas should be placed in the Spray Booth or Heavy Workshop by an extractor hood. 

Save your work on the PC log out and put onto sleep.  

## troubleshooting

* If the laser cutter doesn't connect via Job Control then follow these steps:

## In the event of fire 

* If you see smouldering appearing as the laser cuts, press pause on the machine. 
* Consult your settings and a member of staff; once you’re happy these are correct, try again. 
* If the fire grows, and it is safe to remove the material, you can place it on the floor and use the CO2 extinguisher to put it out. 
* If it is not safe to deal with the situation, follow the building fire procedure. 

## Transfering files to the computer

It is possible to trasfer files via the internet to the computer attached to the laser cutter. This is done through a public Google Drive folder that can be accessed via [this link](https://drive.google.com/drive/folders/18CFGHeynyKthOOkUMOfLHZcnCbxhCR_F). This folder is public and is periodically cleaned of old files. 

## Member Induction sign-off form 

This is the form please follow this link [Plus X Workshop Induction — Speedy 300](https://docs.google.com/forms/d/e/1FAIpQLScGK5o8_LjDb3yrkOvbFx9SYc0GD42iwAa1eIOTdefYFja8PQ/viewform?usp=pp_url)

#### *this must be completed before any further use of the machine*
