# Plus X Tools

## Tracking workshop tools and machines

<!-- Currently, it's all in the [wiki](https://gitlab.com/plus-x-workshop/tools/-/wikis/home) -->

- Documentation for each tool is in its directory
- For a list of all our main tools, and their capabilities, see [tools-list.md](https://gitlab.com/plus-x-workshop/tools/-/blob/master/tools-list.md)
- For current problems and fixes, see the issue tracker: <https://gitlab.com/plus-x-workshop/tools/-/issues>



# Instructions 

How to get things done with the tools repo.


## Finding information on our tools

- There is a directory for each tool (or group of tools).
- Each directory has a readme with links to original manufacturer documentation, manuals, etc.
- Where appropriate, there is also an induction doc (`induction.md`) for new users, and a standard operating procedure to use for signage (`sop.md`). 
- You could also include links to tips and tricks, refernce information or our best practice guidance.

## Weekly review

- Issues closed last week: <https://gitlab.com/plus-x-workshop/tools/-/issues?scope=all&utf8=✓&state=closed>
- Current open issues: <https://gitlab.com/plus-x-workshop/tools/-/issues?scope=all&utf8=✓&state=opened>
- Maintenance review: <https://gitlab.com/plus-x-workshop/tools/-/issues/10>


## Problems in the workshop

- Open an issue when you spot a problem that needs fixing
- Assign a user and date
- Use tags: machine name, `to-buy`, `maintenance`, etc.
- Short on time? Use the submit-by-email address
  

## Regular maintenance and recurring tasks

- Standard maintenance tasks are recorded in this issue: <https://gitlab.com/plus-x-workshop/tools/-/issues/10>
- Copy and paste the actual tasks completed into a new comment, along with any other notes (e.g. tasks *not* completed)
- Submit comments by pressing *Comment*, not *Comment and close issue*


## Access to this repository

- Ask for access
- Clone the repo, make changes locally, then push to the live repo
- Or edit directly on Gitlab
- Use meaningful commit messages, like 'add new exclusions to safe materials list' so we can see what changes have been made over time