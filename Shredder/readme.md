

# CR Clarke R25 Shredder/Granulator


## Docs

[Manual PDF](shredder-manual.pdf)

## Maintenance

After 20-30 hours and every 6 months), check the belt tension - chapter 6.2 of Shini Manual

Regular checks (before use):

- Check rubber shutter in feed box is not damaged
- Check all switches work properly
- Check star screw length and tightness (should be 85 mm)



### Cleaning

Caution - sharp blades. Gloves must be work when working in the cutting chamber.

Insert a length of wood between the blades to prvent accidental rotation.

Refer to the manual for full cleaning instructions.

