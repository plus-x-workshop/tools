# CR Clarke R25 Shredder/Granulator Induction

Laster reviewed 2024-10-31 by AS

## Hazards


| **Hazards**                                                  | **Controls**                                                 |
|--------------------------------------------------------------|--------------------------------------------------------------|
| Danger of crushing body parts between cutters, if machine is opened up | Only staff allowed to open machine.                          |
| Electricity. Risk of injury due to faulty equipment, contact with live electrical components or improper use. | Machine subject to regular inspections and maintenance. Machine to be inspected before use. |
| Risk of hearing damage due to exposure to excessive levels of noise.   | PPE: Hearing protection to be worn for longer periods of use. |
|  Ejection of debris                                          | PPE: eye protection to be worn.                              |


## PPE

When shredding, you must wear:

- Ear defenders
- Goggles

When cleaning, you must wear:

- Gloves


## Materials

### OK

Plastics including:

- HIPS
- Polypropylene
- Polyethylene
- ABS
- Acrylic
- PET
- PLA

All plastics must be clean and dry (e.g. no food waste in containers)

### Not OK

Do not attempt to shred: 

- **Any non-plastic materials**
- Composite plastics such as fibre-reinforced filament
- Soft flexible materials less than 2mm thick

Items to shred must fit into the hopper. Cut them down to size before shredding if they don't fit.

### Changing materials

Different colours of material can be granulated together to form a mottled effect. However it may be preferred to separate the colours. 

Also, it is normally wise to separate material types, as mixed materials may not blend well when processing after granulating. 

To change colour/type, it is easiest to leave the machine running when the old material has finished, and then feed some scraps of the new material through to purge the cutting chamber. When the granules run true to the new material/colour, the collection bin can be emptied so that all of the granules from that point are of the new material/colour.

## Operation

### Before use

TODO: label machine and refernce which bits to check in induction

- Check that other users in the space are happy with the noise you are about to make.
- Always perform a visual inspection before running the machine, to ensure that guards are in place and undamaged. Do not use the machine under any circumstances if there is any damage to guards or electrics.
- Check the emergency stop has not been pressed.
- Check the door and feed box are completely closed.
- **Do not start the shredder if there are unground materials in the feedbox or cutting chamber. These will overload the motor on startup.**
- Switch the machine on using the green start button. **Wait for the blades to reach a steady speed before loading material.**
- Note that an RCD plug is installed, if the machine does not start you may need to reset it. If the rcd repeatedly trips, cease use and inform a member of workshop staff

### During use

- Feed your plastics into the hoppper and listen as they are granulated.
- Do not overload the machine.
- Once it has finished chopping one load, the noise level will decrease and you can load in more.

### After use

- Let the machine run for 2 minutes after finishing granulating to ensure the blades are clear. - **You must not leave unshredded material in the machine** 
- Replace the infeed cover.

## Member Induction sign-off form 

[Plus X Workshop Induction — Shredder](https://docs.google.com/forms/d/e/1FAIpQLSeHZcOaCatSjgX6RTVErAwVBUR52hhqrguhGIxBkhXJAv4Emw/viewform?usp=sf_link)

**This must be completed before any use of the machine**

