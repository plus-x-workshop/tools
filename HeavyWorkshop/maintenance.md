Last reviewed 2022-08-22 by KI

# Heavy Workshop maintenance

### Firmware Updates

n/a

### General Maintenance

* check the exterior extraction bin [Extraction bin checking guide](donaldson-extraction-bin.md)
* hoover the floor
* check the wall of all the [handtools](handtools-maintanence.md)
* check festool extraction units 
* dust machines 
* general tidy 

### Converstion with philip mchale at donaldson

Re need for lime, and specs for install.  
philip.mchale@donaldson.com

- Filter unit sized for worst case: every point extracted at any one time
- Laser cutter: cuttin plastics - fine particulate can bond into the filter elements
- Plastics and rubbers are the worst
- Sub-micron particulate - can block up filters 
- CC will bond with particulate instead.
- Important if hammering laser cutter. Not urgent if only 1-2 hours/week
- To protect filters, not fire

https://www.diy.com/departments/blue-circle-hydrated-lime-25kg-bag/35712_BQ.prd
£17

Martin James commissioned it

- If near 100 dPascals, need to change filter 
- Delta pressure is only 289Pa. This is fine
- Something we need to do in the future. Rate of pressure build up will ramp up as it gets dirtier.
- 800Pa (80deca-pascals) is the point where it gets critical. That’s where cleaning controller (IN HEAVY WORKSHOP) kicks in 
- can look at cleaning history on controller

FRA: risk of dust build-up 
- Our velocity is 20m/s - HSE recommends above 15m/s










