last updated 22-08-2022

# Donaldson Extraction Bin

**`  High pressure warning  = change the filter normally the number is around 150/160  `** 

* To clear warning > press the enter button  


## checking/ changing the bag

* Check the bin everyday/ month depending on the amount of usage (document this in the table below)

| **Date** (DD-MM-YYYY)| **Image of bag**                                         | 
| -------------------- | -------------------------------------------------------- |
| 15-08-2022           | [insidebag_2022-08-16](assets/insidebag_2022-08-16.jpeg) |
| DD-MM-YYYY           | [image_name](assets/image_name.???) |

* get the key from the safe, either 'Extraction RH' or 'Extraction LH' this just means left hand door or right hand door. 

![doors](assets/doors.jpeg)

## Opening bin

```
PPE:
- steel toe shoes
- dust mask 
- goggles
```

* Before opening the bin you need to open the dampers by pushing the black handle in and rotating it horizontally 

![handle_horizontal](assets/handle_horizontal.jpeg)

* Then move around the bin releasing the 3 metal latches around the bin top, it is very heavy so becareful

**` --- NOTES: being careful of toes --- `**

![latches](assets/latches.jpeg)

* Check the tube as well as this keeps the bag down 

![tube](assets/tube.jpeg)

* Donalalds heavy duty bags are in the store room 

**Black Handle should always be left down!** 

![handle_down](assets/handle_down.jpeg)
