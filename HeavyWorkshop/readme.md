Last reviewed 2022-08-22 by KI

# Heavy Workshop readme

serial number: n/a

## Our docs:

* [Extraction bin checking guide](donaldson-extraction-bin.md)

* [Maintenance guide](maintenance.md)

#### Festool Extractors information

* Only use large extracotr for sanding metal!

| **Extractor**                    | **Metal Shards**             | **Metal Sanding** | **Wood Sanding** | **Wood Parts**   |
| ---------------------------------| ---------------------------- | ----------------- | ---------------- | ---------------- |
| CTM 26 E GB 240V (small)         |  no                          |        no         | yes              |       yes        |
| CTM 48 E LE EC B22 240V (large)) |  yes (make sure cooled down) |        yes        | yes              |       yes        |

## External docs

## Contact information

### Donaldson 

Donaldson Filtration (GB) Ltd, Humberstone Lane, Thurmaston, Leicester, LE4 8HP. 
Phone: +44 (0)116 256 4278  |  amy.charity@donaldson.com | www.donaldson.com
Registered Office: Citadel House, 58 High Street, Hull, HU1 1QE. Registered No: 03914641. VAT No: GB 729856971.

 
Vijay Joshi – IAF UK & Ireland Service Supervisor
Donaldson Filtration (GB) Ltd | Humberstone Lane | Thurmaston | Leicester | UK | LE4 8HP
Registered Office: Citadel House, 58 High Street, Hull, HU1 1QE. Registered No: 03914641. VAT No: GB 729856971.

Direct Dial: +44 (0) 116 2564306 - Mobile: +44 (0) 7789 967265 – Website: www.DonaldsonToritDCE.com



* Phone: 0116 269 6161

    * vijay.joshi@donaldson.com
    * Vijay Joshi Mobile number: 07789 967265

* Address: Humberstone Ln, Thurmaston, Leicester LE4 8HP

 ## Issues

* [Issues tagged with Extraction](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=Extraction&first_page_size=20)

## Services 

| Date       | Link to Service Report                                                |
| -----------| --------------------------------------------------------------------- |
| 2022-01-04 | [Dust Collection Service Report](assets/dust_collection_report.pdf)  |
| 2022-01-04 | [LEV Test Examination and Test Report](assets/lev_test.pdf)          |
