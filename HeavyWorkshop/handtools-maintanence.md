last updated 18-08-2022

# Hand tools on the wall 

### General Maintenance

- check all the blades on the saws
- check chizzles edges to see if they need to be sharped [How To Use the Tormek Sharpening Wheel](https://gitlab.com/plus-x-workshop/tools/-/blob/master/Handtools/tormek-sharpener.md)
- check all tools are in the correct space
- check all tools are working and havne't been damaged 
- all drill bits in the correct space
- check inside boxes for all tools 
- check screw drivers aren't damaged
- check rail saws aren't damaged 

### Replacement parts

- Check with Andrew for replacement parts to be purchased or the budget for previous purchasing
