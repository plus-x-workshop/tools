# ITECH CNC induction

<!--Meta-->

## ITECH CNC Router

Induction by Erin Cooper
Last reviewed date: 23-09-2024

<!--Intro-->

## Description / intended use

Mid-size ATC flat-bed CNC with vacuum fixturing bed.

## External documentation and links

* Tormek website: [Laguna IQ pro](https://lagunatools.com/cnc/iq-series/iq-pro/)

Documents:
* [Manual](assets/hb-10-en-v105.pdf)


## Risks

| **How could people be harmed?**<br>                          | **Control measures**<br>                                     |
|--------------------------------------------------------------|--------------------------------------------------------------|
| Entrapment in moving parts                                   | Machine to only be used with doors closed. |
| COSHH: Inhalation, skin or eye contact with hazardous dust | Hazardous materials are prohibited, and extraction will be on when machining. |
| Electrical shock: Water in use near electrical appliance     | Users instructed to never interact with the coolant tank. |
| Injury from sharp tools when swapping cutters.                             | Gloves to be worn when switching tools. |
| Fire from unattended job | User must be in the workshop for the duration of the job. |
<!-- Main section -->

## Safe Working Practices

### Before use

* Ensure machine is not damaged. Report any suspected damage to workshop staff.
* Ensure cutters are undamaged, and properly seated in holders. 
* Close drain valve and enable the compressor, and check the pressure is rising as expected. 
* Ensure workbed is clear of debris and screws. 
* Ensure spoilboard is in place on the vacuum table.

### PPE

* Ear defenders when extraction is enabled. 
* Steel toe boots when shifting heavy stock.
* Eye protection. 


## Materials 

### YES 

* Soft woods, Hard woods, Ply
* MDF - with auxiliary extraction (to be discussed)
* Aluminium - with air blast + retracted skirt
* Plastics
* Wax

### NO

* Materials that produce abrasive dust such as slate, marble
* Fiberglass, Carbon fiber or any glass fiber composite material
* Materials that produce hazardous dust
* Steel - not currently supported, but to be tested


## Parts of the machine

* 1. Pendant- The pendant is the primary user interface for the machine, it's how jobs are loaded and started.
* 2. Spindle - The spindle motor holds the cutting tool, and interfaces with the ATC for toolchanging. Never change tools manually. 
* 3. ATC - The ATC (auto tool-changer) is the bank of tools the machine can switch to automatically, the toolset can be changed out with staff supervision.

![](images/Overview.jpg){width=50%}


## Control box interface

* E.stop - primary emergency stop, isolates all power
* Power indicator switch - push button, glows when the machine is on
* 2. Spindle controller interface - do not touch without consulting staff
* 3. Spindle power, turn this on when preparing to start a job
* 4. Vacuum power, turn this on before fixturing a workpiece. 


![](images/controlBox.png){width=50%}

## Compressor
* 1. Power switch, pull up to enable, push down to disable
* 2. Pressure gauge, ensure this is at ~120PSI before starting a job or changing tools
* 3. Drain valve, this should be not be touched.


![](images/compressor.png){width=50%}


## Hand controller 

### Startup
* 1. Make sure "All axis home" is selected
* 2. Ensure machine bed is clear, and press "REF / OK"

![](images/Start.png){width=50%}

### Main interface

#### Movement
* 2. X / Y / Z jog buttons, use these to align the machine for zeroing - quick taps will move 0.1mm 
* 5. X/Y zero, Z/C zero - these set the current zero points for x/y and z.
* 9. The current position of the toolhead will be shown here as a cross. if outside of the file area it will not be visible.
* 10. This section shows the current position, and current coordinate system (machine coordinates or workpiece coordinates)

#### Tool-switch
* 6. Pressing tool switch, then a number in the 2. button array will swap to that tool. 

#### Job management
* 1. The file button opens up the disk selection menu
* 3. The okay button, for general UI navigation.
* 7. The run / pause / Delete button will start the current job, be cautious. 
* 8. The Stop / Cancel button will pause the current job and ask if you want to cancel it. IT IS NOT AN E-STOP. 

![](images/Main.png){width=50%}

### File selection menu
* The menu will show you multiple disk options, select "UDISK file" and press okay. 
* You will now see a list of files on the USB stick, select any .mmg (gcode) file and press okay to load it. 

![](images/FileDisk.png){width=30%}
![](images/FileList.png){width=30%}


## Software workflows 

The induction will cover using Vcarve, but we do have a post-processor for fusion 360. this is currently experimental.
Please ask if you'd like to know more about using fusion 360 with the cnc. 

### Vcarve 

We have a vcarve pro license installed on the workshop laptop.
Induction content is from the official Vcarve 9.0 Usage guide 
([PDF Manual](VCarvePro.pdf))   ([Website Manual](https://docs.vectric.com/docs/V9.0/VCarvePro/ENU/Help/VCarvePro.html))

Vcarve supports 2 general file workflows, 2D and 3D, this induction will cover the 2D workflow. 


```mermaid
flowchart TD
    A[DXF / SVG] -->|Import| B(Stock setup)
        C[STL 3D MODEL] -->|import| B(Stock setup)

    B --> E(Toolpath setup) --> F(GCODE Output) --> |save to usb| G(CNC MACHINE)
        
```

- DXF and SVG files should be simple shapes with a cleanly defined outline. 

- When in stock setup, always set your Z origin point to the material top

![](images/Z-Zero.png){width=50%}

- Choose your X/Y origin point (where you'll align the cnc tool at the start of the job)
- and set the exact thickness of your material

![](images/Stock.png){width=50%}

#### File import and manipulation

- press "create new file" 
- File -> import -> design
- Your design can be almost any vector format, but DXF/SVG is suggested. 

- Uncheck "use offset"

- press "okay" 

```
images needed here for scaling / positioning
```


#### Toolpath planning

- shift your attention to the toolpath panel on the right side.
- click a profile in your design
- click on either "pocket" or "profile" icon, depending on your intentions. 
- other operations such as drilling are not covered in this induction, but are well documented in the vcarve docs. (or ask a member of staff)

 


- check the preset tool, and click on the tool select button if it's not what you need
- 3. select the tool you'll be using from the library, make sure the tool number matches the position in the machine 
- 1. if the tool you need is not currently installed in the machine, check with a member of staff before attempting to load custom tooling. 
- 2. the spindle speed set in the tool menu does not affect the actual spindle speed of the machine (sadly.)

![](images/toolchoice.png){width=50%}


- type in your cut depth, remembering to slightly overshoot (~0.2mm) if you're cutting all the way through.  
- press "add tabs" if you're cutting all the way through, the auto-generated positions are often fine. 

- check the generated passes number (just under the tool selection)
- for wood, 3mm / pass is generally reasonable - you can edit the pass number by pressing "Edit passes"
- remember to press "set passes" after editing the number

- scroll down and press "calculate" 
#### Gcode export

- Once your toolpaths are generated, you'll see them listed at the bottom of the right panel. (image needed)
- press "save as toolpath" with all the toolpaths selected in this panel
- Save your mmg file with a simple name, containing only letters and numbers, to the CNC usb stick. 

- Keep in mind that multiple toolpaths saved to the same .mmg file will run all at once 
- Saving toolpaths as individual mmg files makes issues easier to correct. 

## Machine workflow

### Machine startup
- fabman login
- verify water pump is on (whirring)
- check the bed is clear, then confirm the machines request to home all axes as detailed in the pendant overview.
- close compressor drain valve and turn it on.
- wait for the compressor to reach 60 psi before starting a job, or attempting to change tools. (image needed)

### Workpiece setup

- ensure your down-facing stock side is clean and flat so the vacuum can grip it (no tape, screw-holes, splinters)
- ensure your stock is greater than 25x25cm (as a general rule) for sufficienct vacuum holding force
- measure your stock thickness precisely and note it down. 
- for small stock, configure the vacuum table to have suction in only the area you need (see vacuum bed section)
- position stock, and enable the vacuum table (see control box overview)

- jog the tool roughly to the center of your cutting area, and slowly lower until touching the surface (photo needed)
- press z-zero
- raise the tool, and jog to your x/y start point (photo needed)
- press x/y-zero



#### if your job uses more than one tool, your origin needs to be set with tool 1

### Running jobs

- Do not enable main extraction if working with metal stock. 
- enable main room extraction at the control panel (image needed)
- put on ear-defenders
- close machine doors
- wait for main room extraction to fully spool up. 
- Check that the compressor is on, and fully pressurised. 
- Check the vacuum bed is enabled, and that the workpiece / spoilboard is secure (gently tap it)
- ensure the water pump is running (whirring)
- press "run / pause / delete", and then "OK" 
- have your finger on the Stop button, in case toolpath error causes a machine crash. 

- The machine will switch to the start tool specified in the Gcode, and begin machining. 

#### Keep an eye on smaller surface-area jobs as the vacuum table can fail under heavy cutting load.

- Once completed, jog the cutting head out of the way and vacuum up loose chips.
- Disable the air compressor and open the drain valve. This is very important.

## Vacuum table configuration 

```
pictures of the vacuum table foam setup here

vacuum table setup goes here, detailing the foam strip arrangement for different stock sizes and. 
```
## Common issues

- Cancelling the job immediately after start can cause a crash - turn the machine on and off via fabman
- The restore job from power off function sometimes doesn't trigger, re-running the full job is a potential workaround. 



## Member Induction sign-off form 

Please review and sign this form: [Plus X Workshop Induction — Flatbed CNC](https://docs.google.com/forms/d/e/1FAIpQLSeJ7_cL61Pk9-T1DEhL11NwcdQj_WQlcOvA79jNK47ye2LEXQ/viewform?usp=sf_link)

**This must be completed before any use of the machine**
