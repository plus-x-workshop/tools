last reviewed YYYY-MM-DD by INITIALS

# Machine Name 

## Top Tips

[User Manual PDF](assets/EN-OriginManual.pdf)

#### Designing for Origin page 3

* interior cut - black stroke and white fill
* exterior cut - black stroke and black fill
* on-line cut - gray stroke
* pocketing cut - gray fill
* guide - blue stroke or blue fill

## Best Practices 