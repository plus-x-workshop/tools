last reviewed 2021-10-09 by KI

# Shaper Origin	Portable CNC machine Induction

Last reviewed 2022-01-21

## Manufacturer Documentation:

- [Shaper Workstation FAQs](https://support.shapertools.com/hc/en-gb/articles/360044785953-Shaper-Workstation-FAQs)
- [Manual download](https://support.shapertools.com/hc/en-gb/articles/4402543084315-Product-manual-of-Shaper-Workstation)
- [Manual PDF](assets/manual_workstation.pdf)


## Safety and proper use

- Workstation must be properly secured to the benchtop, not just left to rest.
- With improper use, it is possible to cut into the aluminium body of Workstation. This will damage Workstation, Origin, your workpiece and possibly you.
- Only use Workstation with Shaper Origin.
- Do not use the Shaper tape surface for gluing, cutting or anything else that will damage the surface.  
- Ensure you use a sacrificial spoilboard if you are cutting all the way through your workpiece - do not cut into the clamping face or shelf. If the spoilboard needs replacing, please ask a member of the workshop team.
- When moving Origin, ensure it is well-supported. You can fix the support bar in front of your workpiece for extra support on small pieces.

- **When using Workstation, you will be operating the router very close to your body. Always be aware of where the router is cutting, and ensure that no part of your body or clothes could enter the cutting area**

## Setup

Secure Workstation to the benchtop with screws (you must use washers) or clamps (if the benchtop has holes for clamping from underneath)

Attach the clamping face at the correct height (depending on your need for spoilboard). Use the 4 mm hex wrench to turn the lock screws but do not over-tighten them.

Add both support arms and gently tighten the 4 mm locking nuts

(You can also ask the workshop team for help installing the Workstation)

## Securing your workpiece

Your workpiece should be:

1. Top face flush with the shaper tape surface
2. Left or right reference edge flush with the alignment pins (assuming you're making 90° cuts, otherwise, use the angle fence)
3. Clamped to the clamping face or taped to the shelf





Extend one set of the vertical alignment pins to give yourself a 90° reference edge. (1)
Use the support bar to provide a reference edge for your top face (2)
Clamp your workpiece so that the vertical edge is flush to the alignment pins (3)
Replace the support bar if needed (4)

![](assets/workstation-vertical.png)



For smaller pieces, fix your piece to the shelf (on the sacrificial board), and adjust the height of the shelf using the Support Bar as a vertical end stop (3)

![](assets/workstation-shelf.png)


## Using Workstation without a spoilboard, or with different height spoilboards

If you are only making internal cuts (e.g. mortises), the clamping face can be installed at the highest position. 

Or it can be dropped down to either of the lower positions to make deeper cuts that extend beyond the material (e.g finger joints). The two positions allow for 25 mm or 50 mm external cuts against the correctly sized spoilboard.

![](assets/workstation-horizontal.png)




## Member Induction sign-off form 

Please review and sign  this form: [Plus X Workshop Induction — Shaper Workstation](https://forms.gle/W4Wf1HxKsdN4rkJc9)

#### *this must be completed before any further use of the machine*





