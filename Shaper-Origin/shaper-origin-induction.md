last reviewed ;2024-02-27by AS

# Shaper Origin	Portable CNC machine Induction

See also separate [Origin Workstation induction](shaper-workstation-induction.md)

## Documentation

- Shaper provides excellent tutorials: <https://www.shapertools.com/en-gb/tutorials>
- They have many walkthrough videos on their YouTube channel: <https://youtube.com/shapertools/>

## Basic concepts

Origin is a CNC router where you move the tool on the workpiece instead of an XY gantry system. The router head is controlled by Origin and corrects for the inaccuracies of your hand movement.

It works most simply on large flat sheet materials like plywood, but you can also use the Workstation clamping table to work on smaller pieces or to do more complex joinery. (separate induction TBC)

Origin knows where it is (in XY space), and forms an image of your workpiece as it is initially by positioning itself against the special tape.

It works out the position of the end of the milling bit (Z space) by `z-touching`, i.e moving down the bit to the work surface, and remembering that position.

CNC designs are loaded onto the Origin over wifi from Shaper Hub, or on a USB stick. You can also do some basic design work on the tool itself. (e.g. to cut a series of circles on a grid)

There are three modes:

- Scan: help Origin form a picture of the workspace and locate itself
- Design: download and place designs on the workpiece where you want to cut them
- Cut: perform the cutting operations

## Materials

Origin can cut materials such as wood, soft metals, plastics, foam and composites.

See Shaper\'s [list of tested materials here](https://support.shapertools.com/hc/en-gb/articles/115002740813-What-materials-can-I-cut-with-Origin-)

## Power

Plug Origin into a regular power supply. (It is tricky to use the Origin with the power socket on the Festool extractor as the Origin always needs power, even when not cutting.)

## Fixturing

- Fix your workpiece down with double-sided tape.
- Apply firm pressure to the tape.
- Ensure the piece to be removed is fixed as well as any remaining material, otherwise it could move during the cut.
  
## Positioning

- Apply shaper tape to your working area - the camera looks at the area in front of the tool:
  - Apply the tape in roughly horizontal strips about 10 cm apart
  - Shaper only recognises complete rectangles on the tape
  - Tape does not have to be precisely placed

- Press `Start scan` and move Origin around to capture all the positioning tape
- Each rectangle will turn blue when captured
- Press  `Finish` when you have swept the whole area

This will produce a composite image of the working area which will be displayed on the Origin screen.

This is saved as a `Workspace`. You can recall previous workspaces, and create new ones. If Origin struggles to locate itself (perhaps becuase yo uhave cut through some of the tape in previous cutting operations, you can add new tape an update your workspace in the scanning mode).

Once your scan is complete, the screen will show a preview of the working area under the router head. **But note this is not live - it will not show any objects (e.g fingers, clothing, tools) introduced after the scan**.


## Extraction

**This is essential for all cuts. You could damage the tool if cuts are not properly cleared of waste.**

Connect to the CTM 26 extractor. Set the extractor to Auto mode, and use the bluetooth button on the hose to turn the extraction on manually during (or after) cutting.

If the extractor is **beeping** please get a member of staff for help and stop using it.

Before using the tool, empty the chip tray at the back.

## Key hazards and safe operation

### Dust and debris

- Use the Festool extraction - don't forget to switch it on. It's easy not to notice when the spindle is running and your ear defenders are on.
- Wear safety glasses.

### Noise

- Wear ear defenders.

### Cutting endmills

- Ensure the clear router guard is installed when cutting – it self-locates with magnets.
- Always detach the router power cable when changing bits
- Note spindle **does not switch off** after cutting – it's easy not to notice if you also have the extraction running

### Trapped clothing, hair, jewellery or fingers

- Tie back loose hair
- Secure or remove loose clothing
- Remove any loose jewellery
- Ensure that you switch off the spindle after cutting
- Always use the spindle guard
- Disconnect the router from power if changin the cutter


### Dropping Origin

- Ensure Origin is supported across the cutting path - use the Workstation, or additional sheets of material around the edges if necessary
- Wear steel toe shoes

### Unexpected router movement

Like all routers, the cutting action of the endmill can pull the router violently and without warning. This can easily damage your workpiece, the tool or yourself, if you are not careful.

- Cut in conservative passes (e.g. 3mm deep)

### Shattered endmills

Aggressive cutting, or use of a blunt or damaged endmill can cause it to break, which may result in sharp fragments being expelled from the router at high speed.

- Use the guard
- Check the endmill before use
- Cut conservatively (e.g. 3 mm passes)

### Unexpected cutting

Like all CNC tools, it is possible to make mistakes in your planning, resulting in the tool cutting where you don't expect. This might mean going too deep through a workpiece, cutting too agressively by mistake, or cutting in an unexpected position

- Always make an aircut first (use the `Aircut` preset on the tool: depth -0.5 mm)
- Do not cut too aggressively. Try a conservative cut depth first (e.g. 3 mm) and check that you can control the tool, before increasing the cut depth.
- If making multiple cuts in multiple passes, ensure you reset the cut depth to a small amount for each new cut line
- Use `conventional` cutting direction. Move the router against the direction of the cutting edge of the endmill.  The endmill turns clockwise as seen from above, so for an interiaor cut you shoul dmove the tool counter-clockwise, and for an exterior cut, move the tool clockwise.
![conventional cutting](assets/shaper-conventional-cut.gif)

- See [Climb cutting and conventional cutting](https://support.shapertools.com/hc/en-gb/articles/115002986094-Climb-cutting-and-conventional-cutting) for a detailed explanation.


### Moving workpiece

- Ensure the piece to be removed is fixed as well as any remaining material, otherwise it could move during the cut
- On your final pass, leave small tabs between each plunge and retraction point. These can be easily cut with a chisel

### Tiredness and environment

You are doing the work of moving the tool around. It can be tiring, and the noise makes it worse. Hazards are also more difficult to notice when the machine is running.

- Be aware of your posture and energy. Take regular breaks, especially when cutting large pieces
- If cutting a piece that you need to move around, be aware of the location of power leads and extraction hoses, and also dust on the floor. 
- Wear ear defenders.


## Designing for Origin

Origin takes SVG files. It is important that the file is saved as an SVG and not exported as an SVG. If defining a cut line, make it 2pt wide.

[Learn more about designing SVGs for Origin](https://support.shapertools.com/hc/en-gb/categories/115000256393-Designing-for-Origin)




## Fitting bits

See Shaper's guide on cutters: <https://support.shapertools.com/hc/en-gb/articles/115002985634-Cutters>

**Note that you cannot use following cutters (fitted with bearings).** These can only be used with a traditional hand or table router.

- Always detach the router power cable when changing bits
- Use the 4 mm hex wrench to loosen the router housing machine screw
- Slide out the router
- Hold down the bit release button, and use the 19 mm spanner to loosen the collet
- Check the old bit for damage, and if safe, return to the correct box. If you suspect damage, inform a member of the workshop team
- Check the bit you wish to use for damage
- Insert the bit so the majority of the shank is in the collet
- Hold down the bit release button, and use the 19 mm spanner to tighten the collet – you do not need to overtighten it
- Slide the router back into the sleeve, using the positioning groove on the back. Let it slide all the way in.
- Re-tighten the router housing machine screw - do not over-tighten
- Re-attach the router power cord

You will always need to z-touch after changing bits.

Don't forget to retighten the router after replacing it after swapping bits - it's easy to forget this step.

## Loading files

You can store files on Shaperhub (Origin can connect over wifi) or a USB key

## When cutting

Set your cutting parameters. Check:

- Cut depth (be conservative in your step downs, perform an aircut (-0.5mm) first)
- On line/inside/outside/pocket
- Offset (useful for quick roughing followed by more controlled clean-up of the edge)
- Bit diameter (make sure it is correct!)
- Z-touch, if you need to initiate manually
- Speeds, if you need to adjust (e.g. for plastics). Spindle speed is set on the router itself
- 
Ensure you move Origin in a `conventional cutting` direction. See [Climb cutting and conventional cutting](https://support.shapertools.com/hc/en-gb/articles/115002986094-Climb-cutting-and-conventional-cutting) for a detailed explanation.

### Cut depths

- Remove material in passes (e.g 3-5 mm per pass)
- Measure your material so you know its thickness accurately, and leave a thin skin at the bottom to hold the part securely before making the final cut to free it from the workpiece
- On your final pass, leave small tabs between each plunge and retraction point. These can be easily cut with a chisel

![tabs](assets/shaper-tabs.png)

## Pocketing

Note pockets are automatically offset from the edge. Use an inside cut after pocketing to finish cleanly.

## After cutting

Press `Retract` to raise the router
Swith off the router spindle using the router power switch
Switch off the extractor
You may nbeed to clean the cut path with some sandpaper

## Suggested tasks

- Fix a thin piece (e.g. 6 mm) of scrap sheet material to a sacrifical work surface
- Scan the work area
- Cut a piece of geometry using the on-tool design function (e.g. a circle) leaving tabs for safe cutting
- Change the router bit to large diameter bit (6 mm)
- Pocket out a shape (e.g. a rectangle)
- Finish the pocket with an inside pass

## Member Induction sign-off form 

PLease review and sign the form here: [Plus X Workshop Induction — Shaper Origin](https://forms.gle/RLNWUZ6Prxbv86TZ6)

#### *this must be completed before any further use of the machine*
