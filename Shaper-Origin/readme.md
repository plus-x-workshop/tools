last reviewed 2022-08-23 by KI

# Shaper Origin	Portable CNC machine readme

serial number: 40058399

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

## External docs

* [Orign user manual (PDF)](assets/singer4432-manual.pdf)

* [Workstation user manual (PDF)](assets/EN-OriginManual.pdf)

## Contact information

* [website](https://www.shapertools.com/en-gb/origin/overview) 

## Issues

* [Issues tagged with shaper-origin](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=shaper-origin&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|