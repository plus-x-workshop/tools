last reviewed 2022-08-24 by KI

# Shaper Origin	Portable CNC machine maintenance

[User Manual PDF](assets/EN-OriginManual.pdf)
[User Manual table PDF](assets/manual_workstation.pdf)

### Firmware Updates

n/a 

### Manual breakdown

[User Manual PDF](assets/EN-OriginManual.pdf)

* Quick Start page ii

    * [support website](support.shapertools.com)
    * [tutorials on origin](tutorials.shapertools.com)

* Designing for Origin page iii

    * interior cut - black stroke and white fill
    * exterior cut - black stroke and black fill
    * on-line cut - gray stroke
    * pocketing cut - gray fill
    * guide - blue stroke or blue fill

* What's Included: *page 2-3*
* Overview: *page 4-5*
* Basics: *pages 6-9*
* Using ShaperTape: *pages 10-12*
* Safety & Care: *pages 13-20*

### General Maintenance

* [Regular Maintenance of Origin](https://support.shapertools.com/hc/en-gb/articles/360005494733-Regular-Maintenance-of-Origin)

### Spoilboard Dimensions

Shop-made Spoilboards can be cut from MDF using the listed dimensions. 
Use the Spoilboard Tensioning Cams to adjust for variations in material thickness.

* Small: H: 25mm, L:  423mm
* Large: H: 50mm L: 421mm

Thickness: 18.1mm – 20.75mm

Shelf Top Dimensions

Shop-made ShelfTops can be cut using the listed dimensions. Shelf Tops can also be customized to suit the needs of your project.

![workstation-shelf-dimensions](assets/workstation-shelf-dimensions.png)

pages from manual
videos from online 
top tips 
day to day things

### Replacement parts


