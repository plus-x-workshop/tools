Last reviewed 2021-10-09 by KI

# Artec Eva Lite 3D scanner Induction

**`Always place scanner in middle of table`**

**`Never leave cables in scanner when put away`**

**`Be aware of the leads when moving around the scanner`**

## Setting up Scanner 

* Machine should always be placed in the box as shown in photo no calbe should be left in machine when put away.
    + Placing the cables into the correct ports of the machine:

### USB cable: 
* Has to placed correctly into the whole in the scanner, it helps to go from above and wiggle it in. Do not force it, if it isn’t going in ask for help!
* Place the wire through the slot as shown in image
* Wiggle cable to check it is secure 

### Power supple cable: 
* Push this into the correct threaded whole
* Screw cap over threaded part until it stops (do not over tighten!)
* Wiggle cable to check it is secure 

![3dscanner](assets/3dscanner.png)
![ports](assets/ports.png)
![wires](assets/wires.png)

### Scanner parts 
* There are two cameras at the top and bottom of the scanner that detect the object you are scanning – do not cover these when using 

![front](assets/front.png)

## Scanning
* Place object in the correct position so that you can get around it as easily as possible. 
* Keep wires out of the way when scanning 
* Watch the screen not the object – 
* Correct distance detector – the object will change colour to let you know your distance: 

|             |                   |
| ----------- | ----------------- |
| `Blue`      | Too far away      |
| `Green`     | Perfect disctance |
| `Red     `  | Too close         |


## Artec Studio 
* More information on how to get the best scan by following going to [basic tutorial PDF](https://www.dropbox.com/s/l3vcn50hvpwcrae/Artec%20Studio%20PT%20head%20tutorial%20v15%202020.pdf?dl=0) 
* Open up and press scan this should then show up as saying `‘preview’` 
* If there is no `‘preview’` check your scanner is plugged in correctly 

## Tidying Up
 
* Unplug the scanner from the computer and the power
* Place all leads and scanner correctly in the box all disconnected
* Give back the scanner and laptop to the technician 

## Member Induction sign-off form 

This is the form please follow this link [Plus X Workshop Induction — 3D Scanner](https://forms.gle/tKZvDYDhzYNteWtH7)

#### *this must be completed before any further use of the machine*




