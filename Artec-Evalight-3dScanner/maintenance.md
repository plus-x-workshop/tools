last reviewed 2022-08-18 by KI

# Artec Eva Lite 3D Scanner maintenance

[User Manual PDF](assets/artec-scanner.pdf)

### Firmware Updates

n/a

### General Maintenance

- check all cables are there

![cables](assets/3dscanner.png)

- check no damage to the machine
- check the computer and remove any files from members that have been saved

### Replacement parts

- if damaged report and call someone to repair do not attempted to repair yourself
