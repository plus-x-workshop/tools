Last reviewed 2022-08-24 by KI

# Artec Eva Lite 3D Scanner readme

serial number: EV.30.39626336

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

## External docs

* [User manual (PDF)](assets/artec-scanner.pdf)

## Contact Information

[Artec 3d Support](https://artecgroup.zendesk.com/hc/en-us)

## Issues

* [Issues tagged with Artec-Scanner](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=Artec-Scanner&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|
