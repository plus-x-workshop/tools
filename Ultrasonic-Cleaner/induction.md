Last reviewed 2024-11-01 by AS


# RS Pro Ultrasonic Cleaner Induction

## Documentation

-   [Manufacturer website](hhttps://uk.rs-online.com/web/p/ultrasonic-cleaners/1249722)
-   [Manual (PDF)](assets/A700000007374775.pdf)

## Intended use
- For cleaning small objects in non-hazardous solvents

## Restrictions
- Only inducted staff and members permitted to use machine
- If you're unsure of anything, ask for asssistance

- Most objects can be processed in an ultrasonic tank but there are some exceptions including some plastics, precious stones and electronic components. We advise checking with the manufacturer of the objects to be cleaned whether they are suitable for ultrasonic cleaning. 


| Hazard                                                       | Control                                                      |
|--------------------------------------------------------------|--------------------------------------------------------------|
| **Tissue damage**<br>Contact exposure to ultrasonic cavitation is suspected to cause living tissue and bone damage. | Do not place hands in machine when running                   |
| **COSHH**<br>Dirt or other material on items to be cleaned could be a COSHH hazard | Handle used solvents and dirty/cleaned parts with care, using appropriate PPE for the contaminant and materials you’re working with. |
| **Electrical**<br>Electrical shock from water spilling out of bath when moving, loading, emptying or when running cleaner   | Ensure machine is placed away from and below the height of electrical sockets when plugged in. <br>Ensure power is disconnected before attempting to add or remove fluid. If pouring fluid away, pour away from the electrical socket on the rear and front control panel. <br>Use drain and hose to empty majority of tank into sink. |
| **Fire**<br>Volatile solvents vaporising and creating flammable or toxic atmospheres.  Machine could cause sparks or static build up which could be an ignition source. | Volatile solvents with low boiling temperatures must not be used. |
| **Machine damage: electrical/fire**<br>An underfilled tank could damage the machine | Do not operate if the tank is less than 2/3 full.            |
| **Machine damage: electrical/fire**<br>Some solvents could damage the housing (e.g. acids)  | Do not use any fluids which could potentially damage the stainless steel bath e.g acids.    |
| **Fire/explosion**<br>A solvent inside a sealed jar in the tank could expand under temperature increase (caused by ultrasonic action, or by intentional heating) and then cause an explosion | Do not fully seal jars                                       |

## General Safety

- Do not run the machine unattended
- Do not use solvents to clean the system, use the dampened blue-roll as provided
- Do not allow any metal object to directly contact the walls of the basin
• Do not let items rest on the base of the bath, always use a glass beaker (with a beaker
rack) or a standard basket.



## Suitable Cleaning agents

- **No volatile, flammable solvents (inc IPA and petroleum-based solvents)**
- Use water, degreaser or specialist ultrasonic cleaning fluid
- Do not use any fluids which could potentially damage the stainless steel bath e.g acids.
- Only use water in the tank, and then a solvent inside a container in the tank
- Aqueous solvent/detergent solutions should be made up with deionised, demineralised or distilled water as calcium carbonate and other impurities in tap water can effect/ reduce the cleaning properties of the solutions and produce undesirable side effects such as deposits and staining.




## Operation
- Check that the tank is clean and empty, and that the basket is present.
- Fill the basin with *WATER ONLY* up to the top of the basket, *NEVER LESS THAN 2/3 FULL*
- Fill your part container with a suitable cleaning agent, place your part in and screw the lid on tight
- Place the container in the basin. 
- Set the operation time using the two time buttons on the right side of the interface
- Press the "Degas" button and allow the machine to run for a few minutes, this is removing bubbles from the water
- Press either the delicate or the full cycle buttons on the machine to initiate your cleaning cycle
- Allow the cycle to run, but keep an eye on the container to make sure cleaning is working, and no leakage is occuring. 
• Maximum running time for a single cycle is 30 minutes.
• Always allow a rest time of 15 minutes after 30 minutes of use between cleaning cycles.
- Remove your part and drain the basin using the valve lever on the right side of the machine. 
- Wipe down the machine and leave it as you found it. 


## Cleaning Modes 

- Degas Mode: Degas mode will start intermittent operation of the ultrasonic power. This ensures rapid removal of air from liquids. This can be started via the degas button. This button will also stop the cleaning prior to timed ending if needed. 
Delicate Mode: The ultrasonic cleaner delivers half power to provide a less aggressive clean for delicate items. To start the delicate mode press the delicate button. This button will also stop the cleaning prior to the timed ending if needed. 

- Full Mode: The ultrasonic cleaner delivers its maximum ultrasonic power to provide an aggressive clean for heavily soiled items. The full button will start Full mode on your ultrasonic tank. This button will also stop the cleaning prior to the timed ending if needed. 

## Different Methods of Cleaning 

- General Cleaning: For lightly soiled objects we suggest only using warm water. This should be paired with a temperature around 40°C 

- Enhanced Cleaning: If the objects in question need a deeper clean then we advise the use of an ultrasonic cleaning solution mixed weakly and heated to the mid range of the fluids operating temperature. Again a warm temperature between 40-60°C will help you achieved your desired results. 
 
- Extensive Cleaning: For the removal of tarnish, fuel and hard carbon deposits, and rust from non-plated metals, etc, we recommend a pre-soaking in the ultrasonic bath of detergent/solvent mixed to its strongest concentration ratio to soften unwanted deposits whilst heated to the high end of the fluids operating temperature range. These steps coupled with the Full mode will ensure the best cleaning possible. 



## Member Induction sign-off form
[Plus X Workshop Induction — Ultrasonic cleaner](https://forms.gle/GusUo7RNBqRLhL7W6)

#### *this must be completed before any further use of the machine*
