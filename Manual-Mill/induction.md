# Engineer Series SX3 Manual Mill Induction

<!--Meta-->

Author: AS  
Last reviewed date: 2025-03-07

<!--Intro-->

## Description / intended use

Manual milling machine for metals and plastics.

### Materials 
+  OK: Aluminium, Brass, Copper, Steel, Plastics
+  Not suitable: Titanium, Magnesium, or any other metal without consulting workshop staff. 


## External documentation and links

- [Manufacturer website](https://www.axminstertools.com/axminster-engineer-series-sx3-mill-drill-digi-505106?gad_source=1&gbraid=0AAAAAD-oZrNCArmrp8h0CwfUhxTIuP9Cs&gclid=EAIaIQobChMInfS57vD3iwMVhJNQBh3qwAMXEAAYASAAEgJUpfD_BwE)
- [Manual (PDF)](assets/SX3-mill-manual.pdf)


## Risks


| **Hazards**                                                  | **Controls**                                                 |
|--------------------------------------------------------------|--------------------------------------------------------------|
| **Powered feed moves table automatically**<br><br>Operator could be trapped between moving table and other static equipment (e.g. lathe or workbench)<br>  | Space given around machine to reduce trap risk               |
| **Electrical shock**<br><br>Damaged machine casing or cables could be live   | Check for damage before use                                  |
| **Rotating spindle near to operator**<br><br>Loose hair, jewellery or clothing cold be entangled in spindle or cutter and operator drawn in resulting in scalping, other skin loss, crushing or other injuries | No loose clothing or jewellery to be worn.Hair tied back    |
| **Heat**<br><br>Could be burned when changing cutters, checking or removing workpiece, or removing swarf from working area | Care taken when changing cutters                             |
| **Chips, swarf, broken cutters**<br><br>Could be hit by small fine particles/chips or larger pieces of material moving unpredictably at high speed | Operator wears PPE: eye protection<br>All material to be securely clamped when milling.<br>Care taken when other workshop users are in vicinity.<br> <br>  |
| **Harmful lubricants used when cutting metal**<br><br>Skin irritation, long-term sensitization or more serious conditions <br>  | Operator wears PPE when using lubricants: eye protection and nitrile gloves<br>No unauthorised lubricants to be used in workshop |



<!-- Main section -->

## Safe Working Practices

### Parts of the machine

![](https://gitlab.com/plus-x-workshop/tools/-/raw/master/Manual-Mill/assets/mill-parts.png)

### Specification
Max. drilling capacity: 25 mm
Max. tapping capacity: 12 mm
End mill capacity 16 mm
Face mill capacity 50 mm
Spindle stroke 70 mm
Throat 230 mm
Max. distance spindle to table 350 mm
Spindle taper MT#3 or R8
Spindle speed 100-1750 rpm f10%
Table effective size 550x160 mm
T-slot size 12 mm
Table cross travel 160 mm
Table longitudinal travel 300 mm
Motor output power 1000 W

### Before use

- Check for any damage to the machine and especially cutters.
- Check the floor area is clear and free from spilt lubricant or swarf
- Familiarise yourself with the emergency stop buttons
- Secure clothing and hair
- Use PPE



#### Securing stock

Stock can be mounted in the vice, or directly to the bed with clamps. 
Ensure the vice, or clamping area are free from swarf before securing. 
**Safety:** Proper securing of stock will prevent the risk of parts being ejected - Use the vise, or at least two clamps. 

Use the Dial test indicator to ensure the vice is parallel to the table (see Milling book, p. 33)


#### Cutters

The minimum amount of cutter should be protruding, without any of the flutes being held in the collet
Be careful when unmounting cutters that the cutter does not fall onto the bed. 

We have some cutters suitable for aluminium or steel. 
Please purchase your own cutters for general usage - we get ours from https://www.shop-apt.co.uk/

For aluminium we would recommmend 3 flute unequal DLC coated cutters [here](https://www.shop-apt.co.uk/3-flute-veriable-unequal-helix-carbide-end-mills-for-aluminium-dlc-coated.html)

#### Changing collet chucks

- Unscrew the cylindrical cover on the top of the spindle assembly, it should not be tight. 
- Using either a spanner, or adjustable spanner, hold the flats above the collet on the spindle. 
- Use an 8mm allen key to undo the drawbar exactly 3 turns. 
- With one hand holding onto the collet chuck, tap the draw bar with a soft-blow hammer. 
- Gently unscrew the drawbar by hand, while holding onto the collet chuck, do not let the collet chuck drop onto the table. 
- Don't forget to remove the allen key and replace the cover before milling

We have a standard ER32 collet chuck with a variety of collets.  
As well as a keyless drill chuck for more flexible, but less secure tool holding.
<!-- 
#### Edge finder usage

Find the collet chuck with the edge finder installed in the tool-rack. 
 -->



### In use


#### Using the axes

L-R
- Move the worktable left-right using the cross feed handle on the front of the mill
- Ensure the handle is engaged with the drive

F-B
Move the worktable front-back using the cross feed handwheel on the front of the mill

U-D (spindle box)
- Lock and unlock the vertical position of the spindle box with the lock handle on the right of the fuselage
- Adjust the starting height of the cutter with the lifting handwheel on the front-right of the mill

U-D (cutter)
- Move the cutter up and down using the longitudinal handwheel on the right of the mill
- Tighten the fine feeding lock handle inside this handwheel and then use the fine feeding handwheel to move the cuttter up and down in fine increements wiothout it springing back

Angled and horizontal milling

- Loosen socket screw on right side
- Then loosen two nuts to tilt the headstock

![](https://gitlab.com/plus-x-workshop/tools/-/raw/master/Manual-Mill/assets/angled-milling.jpg)

Automatic feed

- turn the speed dial to zero (beyond the click point)
- engage the drive by turning the drive switch on the front left of the worktable
- choose your drive direction
- slowly increase the speed 

Take care that 
- nothing unexpected is in the cutters path
- nothing will be trapped by the moving worktable
- keep attention on the machine while using the automatic feed


#### Basic Operations

- Too much load on a tool can cause breakage, listen to your cut, if you hear squealing or chattering, slow down or increase your spindle rpm. 
- Aluminium, brass, and plastics don't necessarily need cutting fluid, but it can help. Use cutting fluid when machining harder metals. 

**Facing a top surface**  

Use a 4 flute endmill or fly cutter. 
P. 34, "Milling for beginners"

**Facing an end surface** 

Use a 4 flute endmill.  
Use the whole length of the tool and move the workpice F-B to avoid using only a short part of the cutting edge (ie if bringing the cutter down into the workpiece)  
P. 34, "Milling for beginners"

**Cutting a rebate** 

P. 35, "Milling for beginners"  
Cut using as much of the length of the tool as possible. ie make a series of narrow cuts at full depth, rather than cutting the full width in a series of cuts of increasing depth.

**Cutting stopped slots**  

4-flute endmills cannot be plunged down into a workpiece - the cutting edges do not cross the centre of the tool. They can only be used to cut across a workpiece.
To cut a stopped slot, you must use a 2 flute cutter. 
(A 2 flute cutter will also cut all slots more accurately, with less deflection)

**Drilling holes**

Use the DRO for accurate placement of holes (p. 47)  
Depending on the drill bit you're using (if it doesn't have a split point with cutting edges that cross the centre point), you may need to make a centre drill or spotting drill to make a dimple first.  
For holes of a diameter up to 6 mm you can drill to the diameter in one go. For larger sizes, step up gradually. 

<!-- We have a deburrer to clean up entry and exit holes

##### Squaring a block
p. 38

##### Facing with a fly cutter

p. 41


UP to p. 53 in book .... -->


**Tapping a hole**

- Press the “Tapping” button, the light above the ‘Tapping’ button lights. The mill is now in ‘Tapping mode’
- ‘Forward’ and ‘Reverse’ buttons are unavailable
- The highest speed under 'Tapping mode' is 500rpm.
- Press the button at the end of the handle to go forward, and again to reverse.

Take extreme caution when tapping as the motoer can get very hot, and the workpiece is under high force.
In particular, be careful when tapping blind holes that you don't bottom out the tap



#### Measuring

We have a varety of measuring tools
See page 27 of the book "Milling for beginners" for instructions

- Calipers
- Micrometer
- Dial test indicator and dial gauge
- Edge finder
- DRO

#### Using the DRO (Digital Read Out)

- Ensure the DRO is plugged into power
- Zero axes using the X/Y/Z0 buttons




### After use

- Turn off the motor and be aware that the workpiece may be hot before touching it
- Remove all swarf and waste material from the machine and put in the appropriate bins
- If you have used cutting fluid, ensure the machine and work area is clean from oil  
- If you have spotted any issues or breakages – even if not caused by you – please report these to a member of staff
- Make sure the area is clean before leaving the machine. 

<!-- ### Staff only: maintenance and cleaning -->

## PPE Requirements

- Eye protection: at all times
- Nitrile gloves, if using harmful solvents

## Sign-off


[Plus X Workshop Induction — Manual Mill](https://forms.gle/7sCzBtGRFmyVjnnt8)

*this must be completed before any further use of the machine*
