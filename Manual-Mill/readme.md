last reviewed 2022-08-D23D by KI

# Engineer Series SX3 Manual Mill Drill Digi readme

serial number: 21-2051-A-114

## Our docs:

* [Induction document](induction.md)

* [Standard Operating Procedure](sop.md)

* [Maintenance guide](maintenance.md)

## External docs

* [User Manual (PDF)](assets/beltsander_manual.pdf)

## Contact information

* Email: b2beast@axminstertools.com
* Number: (Alex) 07976 968733

* [Website](https://www.axminstertools.com/customer-services)

## Issuess

* [Issues tagged with manual-mill](https://gitlab.com/plus-x-workshop/tools/-/issues/?sort=created_date&state=all&label_name%5B%5D=manual-mill&first_page_size=20)

## Services

| Date (YYYY-MM-DD)| Link to  Reports  |
| -----------| ----------------- |
| YYYY-MM-DD | [ADD CERTIFICATE](assets/)|