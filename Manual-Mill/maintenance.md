last reviewed YYYY-MM-DD by INITIALS

# Engineer Series SX3 Manual Mill Drill Digi maintenance

[User Manual PDF](assets/)

### Firmware Updates

n/a

### General Maintenance

pages from manual
videos from online 
top tips 
day to day things

### Replacement parts


## Maintenance

### Contact information for Workbee?


## Troubleshooting

Set static IP (or permanent address): http://workbee.local

How to:
https://learn.ooznest.co.uk/Answers/View/113/The+IP+Address+on+my+WorkBee+keeps+changing


- Connect in YAT and send M552 to get the new IP Address.
- Connect under this new IP Address.
- Once you are inside WorkBee Control go to system settings and edit config.g.
- Copy the following line:
- M550 PWorkBee CNC Machine ; Set machine name
- Paste it in customconfig.g and change it to:
- M550 PWorkBee; Set machine name
- You can now connect under the URL: http://workbee.local/
- This will work regardless if the IP Address changes. 