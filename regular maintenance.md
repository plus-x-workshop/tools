# Regular Maintenance

# FABLAB 

**Speedy 300**

* [Speedy 300 Maintenance Doc](https://gitlab.com/plus-x-workshop/tools/-/blob/master/Laser-TrotecSpeedy300/maintanence.md)

- clean mirror

- clean laser head
- clean sense
- check extraction levels
- clean bed
- check filers in extraction

**Electronics**  
- check the accessory box (soldering, tweezers etc)
- check wires are not melted 
- check soldering tips aren't bent  

**Prusa**

* [Prusa Maintenance Doc](https://gitlab.com/plus-x-workshop/tools/-/blob/master/3DPrinters-PrusaI3/maintanence.md)

- clean nozzles
    - make sure the nozzle temperature is at 230 and use the metal pointy stick to push up and check for clogging. 
    - then if this works fine manually push the filament through the nozzle checking it comes out evenly.
    - clean the nozzle with the que tips and check it is screwed in enough.
    - use the brass brush to clean the nozzle and heater so it is clean
- check movements on axis
    - go into settings and press 'axis home' (CHECK THIS)
- clean axis 
    - check there are bits of filament or rubbish around the printer
    - use blue roll to wipedown any residue on the metal dowels 
    - check the cables are clean and in good condition 
- check oiling 
- check for upgrade
    - [Prusa Upgrading guide](https://help.prusa3d.com/en/article/firmware-updating-mk3s-mk3s-mk3_2227)
- calibrate beds
- go through SD cards and remove other work

**Formlabs**

* [Formlabs Maintenance Doc]()

- check wificonnection
- check for upgrade
- dust machine 

**SRM**

* [SRM-20 Maintenance Doc]()

- clean
- dust
- turn on 
- surface out bed
- check on accessories (milling bits etc)
- do we need to buy more tools 
- check tools for the machine

**VINYL CUTTER**

* [Vinyl Cutter Maintenance Doc]()

- dust
- turn on
- check if grey roller is there
- check rubber feet are not damaged and are holding down the vinyl correctly

# HEAVY WORKSHOP

**Q500** 

* [Q500 Maintenance Doc](https://gitlab.com/plus-x-workshop/tools/-/blob/master/Laser-TrotecQ500/maintanence.md)

- clean mirror
- clean laser head
- clean sense
- check extraction levels
- clean bed
- dust machine 
- turn on extraction fans
- check water level in cooler

**MDX**

* [MDX Maintenance Doc]()

- clean
- dust
- turn on 
- surface out bed
- check on accessories (milling bits etc)
- do we need to buy more tools 
- check tools for the machine 

**Workbee**

* [Workbee Maintenance Doc]()

- ANDREW

**Line Bender**

* [Line Bender Maintenance Doc]()

- check accessories 
- check check cables

**Vacuum former**

* [Vacuum former Maintenance Doc]()

- check accessories 
- check got enough material
- check bed is clear
- KATIE ?? <-- anything else?

# WOODWORKING AREA 

**Band Saw**

* [Band Saw Maintenance Doc]()

- check blade (how long has it been in use)
- clean 
- remove scraps
- are all accessories there?
- bed level
- turn on (how does it sound)
- need oiling?

**Scroll saw**

* [Scroll saw Maintenance Doc]()

- check blade (how long has it been in use)
- clean 
- remove scraps
- bed level
- turn on (how does it sound)
- need oiling?

**Pillar Drill**

* [Pillar Drill Maintenance Doc]()

- clean 
- remove scraps
- are all accessories there?
- bed level
- turn on (how does it sound)
- need oiling?

**Belt Sander**

* [Belt Sander Maintenance Doc]()

- check belt (how long has it been in use)
- clean 
- remove scraps
- are all accessories there?
- bed level
- turn on (how does it sound)
- check extraction is open 

**Festools** 

* [Festools Maintenance Doc]()

- all in correct boxes
- check blades, sanders
- check wires to check they are not curled up

**Festool Hoovers**

* [Festool Hoovers Maintenance Doc]()

- check bags
- all accessories there
- check suction 
- check hose and wire to check they are not curled up

**Hand tools**
- check all hand tools have been placed back into the correct slot
- check non are broken 
- check battery life on drill

# GENERAL 

- check wood store for excess wood and safety
- check wood scrap bin for excess wood
- check Extraction isn’t too full
- check extraction levels (nothing more than 150)
- check spray booth is clean and working 

# ASSEMBLY SPACE

- dust material library 
- check sink supplies (washing up, hand soap, blue roll, sponge, spray)
- sharpening tool
- oil vacuum chamber
- check testing equipment 
- defrost fridge
- [clean sump](https://gitlab.com/plus-x-workshop/tools/-/blob/master/WetArea/readme.md#sump)